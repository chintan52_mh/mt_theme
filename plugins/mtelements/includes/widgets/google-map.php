<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Group_Control_Css_Filter;

/**
 * Mtelements Google Map Class
 *
 * The main class that initiates and runs google map element.
 *
 * @since 1.0.0
 */
class Mtelements_Google_Map_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-maps';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Google Map', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-map-marked';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget keywords.
	 */
	public function get_keywords() {
		return [ 'google', 'map', 'embed', 'location' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'section_map',
			[
				'label' => __( 'Map', 'mtelements' ),
			]
		);

		$mttheme_google_map_api_key = get_theme_mod( 'mttheme_google_map_api_key', '' ); 
		if ( ! $mttheme_google_map_api_key ) {
			$query['autofocus[control]'] = 'mttheme_google_map_divider';
			$control_link = add_query_arg( $query, admin_url( 'customize.php' ) );
			$this->add_control(
				'set_key',
				[
					'type' => Controls_Manager::RAW_HTML,
					'raw'  => sprintf(
						esc_html__( 'Please set Google maps API key before using this widget. You can create own API key  %1$s. Paste created key on %2$s', 'mtelements' ),
						'<a target="_blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key">' . esc_html__( 'here', 'mtelements' ) . '</a>',
						'<a target="_blank" href="' . esc_url( $control_link ) . '">' . esc_html__( 'settings page', 'mtelements' ) . '</a>'
					)
				]
			);
		}

		$this->add_control(
			'latitude',
			[
				'label' => __( 'Location Latitude', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$this->add_control(
			'longitude',
			[
				'label' => __( 'Location Longitude', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'description' => sprintf(
						esc_html__( 'You can get your location Latitude and Longitude from %1$s.', 'mtelements' ),
						'<a target="_blank" href="https://www.latlong.net/">' . esc_html__( 'here', 'mtelements' ) . '</a>'
					)
			]
		);

		$this->add_control(
			'zoom',
			[
				'label' => __( 'Zoom', 'mtelements' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 10,
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 20,
					],
				],
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'height',
			[
				'label' => __( 'Height', 'mtelements' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 300,
				],
				'range' => [
					'px' => [
						'min' => 40,
						'max' => 1440,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .contact-map' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'mtelements' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_logo',
			[
				'label' => __( 'Pointer Image & Get Directions', 'mtelements' ),
			]
		);
			$this->add_control(
				'map_logo',
				[
					'label' => __( 'Pointer Image', 'mtelements' ),
					'type' => Controls_Manager::MEDIA,
					'show_label' => true,
				]
			);

			$this->add_control(
				'get_direction_separator',
				[
					'label' => __( 'Get Directions', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'get_directions_text',
				array(
					'label'       => esc_html__( 'Text', 'mtelements' ),
					'type'        => Controls_Manager::TEXT,
					'default' 	  => 'Get Directions',
				)
			);

			$this->add_control(
				'get_directions_link',
				array(
					'label'       => esc_html__( 'Link', 'mtelements' ),
					'type'        => Controls_Manager::TEXT,
					'default' 	  => '#',
				)
			);

			$this->add_control(
				'get_directions_link_target',
				array(
					'label'        => esc_html__( 'Open link in new window', 'mtelements' ),
					'type'         => Controls_Manager::SWITCHER,
					'return_value' => '_blank',
				)
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			$this->add_control(
				'image_background_color',
				[
					'label' => __( 'Image Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-map .htmlmarker' => 'background: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'direction_background_color',
				[
					'label' => __( 'Direction Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-map .direction' => 'background: {{VALUE}};',
					],
				]
			);
		$this->end_controls_section();

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		$options = $this->get_map_options();
		?>
			<div class="contact-map mtelements-contact-map" data-map_options="<?php echo htmlspecialchars( json_encode( $options ) ); ?>">
			</div>
		<?php
	}

	/**
	 * Returns Google map options
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_map_options() {

		$settings = $this->get_settings();

		$get_directions_link_target = ( ! empty( $settings['get_directions_link_target'] ) ) ? ' target="' . $settings['get_directions_link_target'] . '"' : '';

		$options  = array(
			'latitude'        	 	=> $settings['latitude'],
			'longitude'        	 	=> $settings['longitude'],
			'zoom'			 	 	=> $settings['zoom']['size'],
			'mapLogo'		 		=> $settings['map_logo']['url'],
			'getDirectionLink' 		=> $settings['get_directions_link'],
			'getDirectionText' 		=> $settings['get_directions_text'],
			'getDirectionLinkTarget' => $get_directions_link_target,
		);

		return $options;
	}
}