<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Core\Schemes;
use Elementor\Group_Control_Typography;
use Elementor\Control_Media;
use Elementor\Icons_Manager;
use Elementor\Repeater;
use Elementor\Utils;

/**
 * Mtelements Testimonial Slider Widget Class
 *
 * The main class that initiates and runs testimonial slider element.
 *
 * @since 1.0.0
 */
class Mtelements_Testimonial_Slider_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-testimonial-slider';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Testimonial Slider', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-quote-left';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'sliders',
			[
				'label' => __( 'Style and Data', 'mtelements' ),
			]
		);

		$this->add_control(
			'testimonial_slider_style',
			array(
				'label'   => esc_html__( 'Style', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'testimonial-slider-style-1',
				'options' => [
					'testimonial-slider-style-1' => __( 'Style 1', 'mtelements' ),
				]
			)
		);

		$this->add_control(
			'testimonial_main_title',
			[
				'label' => __( 'Testimonial Main Title', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Testimonial', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
			]
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'slide_title',
			[
				'label' => __( 'Title', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Slide title', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
			]
		);
        
        $repeater->add_control(
			'slide_content',
			[
				'label' => __( 'Content', 'mtelements' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => __( 'Slide Content', 'mtelements' ),
			]
		);

        $repeater->add_control(
			'testimonial_slide_image',
			[
				'label' => __( 'Testimonial Image', 'mtelements' ),
				'type' => Controls_Manager::MEDIA,
				'show_label' => true,
			]
		);

		 $repeater->add_control(
			'testimonial_slide_name',
			[
				'label' => __( 'Testimonial Name', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Nikky Engelen', 'mtelements' ),
			]
		);

		$repeater->add_control(
			'testimonial_slide_location',
			[
				'label' => __( 'Testimonial Location', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Goodworth Clatford, Andover', 'mtelements' ),
			]
		);

		$repeater->add_control(
			'testimonial_gallery',
			[
				'label' => __( 'Testimonial Gallery', 'mtelements' ),
				'type' => Controls_Manager::GALLERY,
				'dynamic' => [
					'active' => true,
				],
			]
		);

		$repeater->add_control(
			'testimonial_sold_image',
			[
				'label' => __( 'Testimonial Sold Image', 'mtelements' ),
				'type' => Controls_Manager::MEDIA,
				'show_label' => true,
			]
		);

		$this->add_control(
			'slides',
			[
				'label' => __( 'Slider Slides', 'mtelements' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[	
						'slide_title' 					=> __( 'Testimonial #1 Title', 'mtelements' ),
						'slide_content' 				=> __( 'Testimonial #1 Content', 'mtelements' ),
						'testimonial_slide_name'		=> __( 'Testimonial #1 Name', 'mtelements' ),
						'testimonial_slide_location'	=> __( 'Testimonial #1 Location', 'mtelements' ),
					],
					[
						'slide_title' 					=> __( 'Testimonial #2 Title', 'mtelements' ),
						'slide_content' 				=> __( 'Testimonial #2 Content', 'mtelements' ),
						'testimonial_slide_name'		=> __( 'Testimonial #2 Name', 'mtelements' ),
						'testimonial_slide_location'	=> __( 'Testimonial #2 Location', 'mtelements' ),
					],
				],
				'title_field' => '{{{ slide_title }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'slider_configuration',
			[
				'label' => __( 'Slider Configuration', 'mtelements' ),
			]
		);

		$this->add_control(
			'arrows',
			array(
				'label'        => esc_html__( 'Navigation', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'On', 'mtelements' ),
				'label_off'    => esc_html__( 'Off', 'mtelements' ),
				'return_value' => 'true',
				'default'      => 'true',
			)
		);

		$this->add_control(
			'autoplay',
			array(
				'label'        => esc_html__( 'Autoplay', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'On', 'mtelements' ),
				'label_off'    => esc_html__( 'Off', 'mtelements' ),
				'return_value' => 'true',
				'default'      => 'true',
			)
		);

		$this->add_control(
			'autoplay_speed',
			array(
				'label'     => esc_html__( 'Autoplay Speed', 'mtelements' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 5000,
				'condition' => array(
					'autoplay' => 'true',
				),
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_main_title_style',
			[
				'label' => __( 'Main Title', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'main_title_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .slider-detail .testimonial-title' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'main_title_typography',
					'selector' => '{{WRAPPER}} .slider-detail .testimonial-title',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_title_content_style',
			[
				'label' => __( 'Title & Content', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_title',
				[
					'label' => __( 'Title', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'title_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .slider-detail .slide-title' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .slider-detail .slide-title',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_content_title',
				[
					'label' => __( 'Content', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'content_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .slider-detail .slide-content' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'content_typography',
					'selector' => '{{WRAPPER}} .slider-detail .slide-content',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_name_location_style',
			[
				'label' => __( 'Name & Location', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_name',
				[
					'label' => __( 'Name', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'name_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .slider-detail .slide-testimonial-wrap .slide-testimonial-name' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'name_typography',
					'selector' => '{{WRAPPER}} .slider-detail .slide-testimonial-wrap .slide-testimonial-name',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_content_location',
				[
					'label' => __( 'Location', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'location_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .slider-detail .slide-testimonial-wrap .slide-testimonial-location' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'location_typography',
					'selector' => '{{WRAPPER}} .slider-detail .slide-testimonial-wrap .slide-testimonial-location',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		if ( empty( $settings['slides'] ) ) {
			return;
		}

		$slides = [];
		$slide_html = '';

		switch ( $settings['testimonial_slider_style'] ) {
			case 'testimonial-slider-style-1':
				foreach ( $settings['slides'] as $index => $slide ) {
					$slide_html = '<div class="swiper-slide">';
						$slide_html .= '<div class="row">';
							if ( ! empty( $slide['testimonial_gallery'] ) ) {
								$slide_html .= '<div class="col-lg-5 col-md-8 offset-lg-0 offset-md-2">';
									$slide_html .= '<div class="gallery-slider">';
										$slide_html .= '<span class="before-pattern"></span>';
										$slide_html .= '<div class="swiper-container gallery-slider-container">';
											$slide_html .= '<div class="swiper-wrapper">';
												foreach ( $slide['testimonial_gallery'] as $index => $attachment ) {
													$slide_html .= '<div class="swiper-slide">';
														$slide_html .= '<img class="swiper-slide-image" src="' . esc_attr( $attachment['url'] ) . '" alt="' . esc_attr( Control_Media::get_image_alt( $attachment ) ) . '" />';
													$slide_html .= '</div>';
												}
											$slide_html .= '</div>';
											$slide_html .= '<div class="swiper-pagination mtelements-gallery-pagination"></div>';
										$slide_html .= '</div>';
										$slide_html .= '<span class="after-pattern"></span>';
										if ( ! empty( $slide['testimonial_sold_image']['url'] ) ) {
											$slide_html .= '<div class="slide-sold-image">';
												$slide_html .= '<img class="sold-image" src="' . esc_attr( $slide['testimonial_sold_image']['url'] ) . '" alt="' . esc_attr( Control_Media::get_image_alt( $slide['testimonial_sold_image'] ) ) . '" />';
											$slide_html .= '</div>';
										}
									$slide_html .= '</div>';
								$slide_html .= '</div>';
							}
							$slide_html .= '<div class="col-lg-6">';
								$slide_html .= '<div class="slider-detail">';
									$slide_html .= '<div class="testimonial-title">';
										$slide_html .=  $settings['testimonial_main_title'];
									$slide_html .= '</div>';
									if ( ! empty( $slide['slide_title'] ) ) {
										$slide_html .= '<div class="slide-title">';
											$slide_html .= $slide['slide_title'];
										$slide_html .= '</div>';
									}
									if ( ! empty( $slide['slide_content'] ) ) {
										$slide_html .= '<div class="slide-content">';
											$slide_html .= $slide['slide_content'];
										$slide_html .= '</div>';
									}
									if ( ! empty( $slide['testimonial_slide_image'] ) || ! empty( $slide['testimonial_slide_name'] ) || ! empty( $slide['testimonial_slide_location'] ) ) {
										$slide_html .= '<div class="slide-testimonial-wrap">';
											$slide_html .= '<div class="slide-testimonial-left">';
												if ( ! empty( $slide['testimonial_slide_image']['url'] ) ) {
													$slide_html .= '<div class="slide-testimonial-image">';
														$slide_html .= '<img src="'.esc_url( $slide['testimonial_slide_image']['url'] ).'" alt="' . esc_attr( Control_Media::get_image_alt( $slide['testimonial_slide_image'] ) ) . '" />';
													$slide_html .= '</div>';
												}
											$slide_html .= '</div>';
											$slide_html .= '<div class="slide-testimonial-right">';
												if ( ! empty( $slide['testimonial_slide_name'] ) ) {
													$slide_html .= '<div class="slide-testimonial-name">';
														$slide_html .= $slide['testimonial_slide_name'];
													$slide_html .= '</div>';
												}
												if ( ! empty( $slide['testimonial_slide_location'] ) ) {
													$slide_html .= '<div class="slide-testimonial-location">';
														$slide_html .= $slide['testimonial_slide_location'];
													$slide_html .= '</div>';
												}
											$slide_html .= '</div>';
										$slide_html .= '</div>';
									}
								$slide_html .= '</div>';
							$slide_html .= '</div>';
						$slide_html .= '</div>';
					$slide_html .= '</div>';

					$slides[] = $slide_html;
				}
			break;
		}

		if ( empty( $slides ) ) {
			return;
		}

		$this->add_render_attribute( [
			'slider' => [
				'class' => 'mtelements-testimonial-slider swiper-wrapper '. $settings['testimonial_slider_style'],
			],
			'slider-wrapper' => [
				'class' => 'mtelements-testimonial-slider-wrapper swiper-container',
			],
		] );

		$slides_count = count( $settings['slides'] );

		$options = $this->get_slider_options();
		?>
			<div <?php echo $this->get_render_attribute_string( 'slider-wrapper' ); ?> data-slider_options="<?php echo htmlspecialchars( json_encode( $options ) ); ?>">
				<div <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
					<?php echo implode( '', $slides ); ?>
				</div>
				<?php if ( 1 < $slides_count && $settings['arrows'] ) : ?>
	    			<div class="navigation-wrapper">
	    				<div class="mtelements-swiper-button mtelements-prev-arrow<?php echo $this->get_id(); ?>">
	    				    <img src="<?php echo plugins_url(). '/mtelements/assets/images/svg/left-arrow-blue.svg'; ?>" alt="prev-image" />
	    				</div>
	    				<div class="mtelements-swiper-button mtelements-next-arrow<?php echo $this->get_id(); ?>">
	    				    <img src="<?php echo plugins_url(). '/mtelements/assets/images/svg/right-arrow-blue.svg'; ?>" alt="next-image" />
	    				</div>
	    			</div>  
				<?php endif; ?>
			</div>
		<?php
	}

	/**
	 * Returns array with slider options
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_slider_options() {

		$settings = $this->get_settings();
		$widget_id = $this->get_id();

		$options  = array(
			'autoplaySpeed'  => absint( $settings['autoplay_speed'] ),
			'autoplay'       => filter_var( $settings['autoplay'], FILTER_VALIDATE_BOOLEAN ),
			'arrows'         => filter_var( $settings['arrows'], FILTER_VALIDATE_BOOLEAN ),
			'prevArrow'      => '.mtelements-prev-arrow' . $widget_id,
			'nextArrow'      => '.mtelements-next-arrow' . $widget_id,
		);

		return $options;
	}

}