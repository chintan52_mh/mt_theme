<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Icons_Manager;
use Elementor\Core\Schemes;
use Elementor\Utils;

/**
 * Mtelements Team Widget Class
 *
 * The main class that initiates and runs team carousel element.
 *
 * @since 1.0.0
 */
class Mtelements_Team_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-team';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Team', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-user';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'team',
			[
				'label' => __( 'Style and Data', 'mtelements' ),
			]
		);

		$this->add_control(
			'team_style',
			array(
				'label'   => esc_html__( 'Style', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'team-style-1',
				'options' => [
					'team-style-1' => __( 'Style 1', 'mtelements' ),
				]
			)
		);

		$this->add_control(
			'team_image',
			[
				'label' => __( 'Image', 'mtelements' ),
				'type' => Controls_Manager::MEDIA,
				'default' => array(
					'url' => Utils::get_placeholder_image_src(),
				),
			]
		);

		$this->add_control(
			'team_title',
			[
				'label' => __( 'Title', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Loren Spears', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
			]
		);
        
        $this->add_control(
			'team_designatation',
			[
				'label' => __( 'Designation', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Marketing manager', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
			]
		);

		 $this->add_control(
			'team_contact',
			[
				'label' => __( 'Contact Text', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'default' => __( 'Call: 000-111-2222', 'mtelements' ),
			]
		);
		
		$this->add_control(
			'team_button_text',
			[
				'label'       => esc_html__( 'Button Text', 'mtelements' ),
				'type'        => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'default' => __( 'Contact now', 'mtelements' ),
			]
		);

		$this->add_control(
			'team_button_link',
			[
				'label'       => esc_html__( 'Button Link', 'mtelements' ),
				'type'        => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'default' => '#',
			]
		);

		$this->add_control(
			'team_button_link_target',
			[
				'label'        => esc_html__( 'Open link in new window', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'return_value' => '_blank',
			]
		);

		$this->add_control(
			'team_button_link_rel',
			[
				'label'        => esc_html__( 'Add button nofollow', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'return_value' => 'nofollow',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);	

			$this->add_control(
				'box_background_color',
				[
					'label' => __( 'Box Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mtelements-team' => 'background-color: {{VALUE}};',
					],	
				]
			);

			$this->add_group_control(
				Group_Control_Box_Shadow::get_type(),
				[
					'name' => 'box_shadow',
					'selector' => '{{WRAPPER}} .mtelements-team',
				]
			);

			$this->add_responsive_control(
				'team_image_size',
				[
					'label' => __( 'Image Size', 'mtelements' ),
					'type' => Controls_Manager::SLIDER,
					'default' => [
						'size' => 115,
					],
					'range' => [
						'px' => [
							'min' => 50,
							'max' => 150,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .team-image img' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$this->add_control(
				'separator_color',
				[
					'label' => __( 'Separator Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .team-button' => 'border-top-color: {{VALUE}};',
					],	
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_title_designatation_style',
			[
				'label' => __( 'Title & Designatation', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_title',
				[
					'label' => __( 'Title', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'title_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .team-title' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .team-title',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_heading_designatation',
				[
					'label' => __( 'Designatation', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'designatation_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .team-designation' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'designatation_typography',
					'selector' => '{{WRAPPER}} .team-designation',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_contact_button_style',
			[
				'label' => __( 'Contact Text & Button', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_contact',
				[
					'label' => __( 'Contact Text', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'contact_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .team-contact' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'contact_typography',
					'selector' => '{{WRAPPER}} .team-contact',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_heading_button',
				[
					'label' => __( 'Button', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'button_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .team-button .btn.btn-link' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_hover_text_color',
				[
					'label' => __( 'Hover Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .team-button .btn.btn-link:hover, {{WRAPPER}} .team-button .btn.btn-link:focus' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_hover_bg_color',
				[
					'label' => __( 'Background Hover Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .team-button .btn.btn-link:hover, {{WRAPPER}} .team-button .btn.btn-link:focus' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'button_typography',
					'selector' => '{{WRAPPER}} .team-button .btn.btn-link',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		$this->add_render_attribute( [
			'main-wrapper' => [
				'class' => 'mtelements-team '. $settings['team_style'],
			],
		] );

		if( $settings['team_button_link_target'] ) {
			$this->add_render_attribute( [
				'team-button-wrapper' => [
					'target' => $settings['team_button_link_target']
				],
			] );
		}
		if( $settings['team_button_link_rel'] ) {
			$this->add_render_attribute( [
				'team-button-wrapper' => [
					'rel'   => $settings['team_button_link_rel'],
				],
			] );
		}

		switch ( $settings['team_style'] ) {
			case 'team-style-1':
			    echo '<div ' . $this->get_render_attribute_string( 'main-wrapper' ) . '>';
			    	if ( ! empty( $settings['team_image'] ) ) {
    					echo '<div class="team-image">';
    						echo '<img src="'.esc_url( $settings['team_image']['url'] ).'" alt="" />';
    					echo '</div>';
    				}
    				if ( ! empty( $settings['team_title'] ) ) {
    					echo '<div class="team-title">';
    						echo $settings['team_title'];
    					echo '</div>';
    				}
    				if ( ! empty( $settings['team_designatation'] ) ) {
    					echo '<div class="team-designation">';
    						echo $settings['team_designatation'];
    					echo '</div>';
    				}
    				if ( ! empty( $settings['team_contact'] ) ) {
    					echo '<div class="team-contact">';
    						echo $settings['team_contact'];
    					echo '</div>';
    				}
    				if ( ! empty( $settings['team_button_text'] ) && ! empty( $settings['team_button_link'] ) ) {
    					echo '<div class="team-button">';
    						echo '<a href="' . esc_url( $settings['team_button_link'] ) . '" class="btn btn-link" '. $this->get_render_attribute_string( 'team-button-wrapper' ) .' >' . $settings['team_button_text'] . '</a>';
    					echo '</div>';
    				}
				echo '</div>';
			break;
		}
	}
}