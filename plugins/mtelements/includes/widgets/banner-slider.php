<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Utils;
use Elementor\Core\Schemes;
use Elementor\Group_Control_Typography;

/**
 * Mtelements Accrodion Widget Class
 *
 * The main class that initiates and runs banner slider element.
 *
 * @since 1.0.0
 */
class Mtelements_Banner_Slider_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-banner-slider';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Banner Slider', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-sliders-h';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'sliders',
			[
				'label' => __( 'Style and Data', 'mtelements' ),
			]
		);

		$repeater = new Repeater();

		$this->add_control(
			'banner_slider_style',
			array(
				'label'   => esc_html__( 'Style', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'banner-slider-style-1',
				'options' => [
					'banner-slider-style-1' => __( 'Style 1', 'mtelements' ),
				]
			)
		);

		$repeater->add_control(
			'slide_image',
			[
				'label' => __( 'Image', 'mtelements' ),
				'type' => Controls_Manager::MEDIA,
				'default' => array(
					'url' => Utils::get_placeholder_image_src(),
				),
				'show_label' => false,
			]
		);

		$repeater->add_control(
			'slide_title',
			[
				'label' => __( 'Title', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Slide title', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
			]
		);
        
        $repeater->add_control(
			'slide_email_address',
			[
				'label' => __( 'Email Address', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'help@propery.com', 'mtelements' ),
			]
		);
		
		$repeater->add_control(
			'slide_button_text',
			array(
				'label'       => esc_html__( 'Button Text', 'mtelements' ),
				'type'        => Controls_Manager::TEXT,
			)
		);

		$repeater->add_control(
			'slide_button_link',
			array(
				'label'       => esc_html__( 'Button Link', 'mtelements' ),
				'type'        => Controls_Manager::TEXT,
			)
		);

		$repeater->add_control(
			'slide_button_link_target',
			array(
				'label'        => esc_html__( 'Open link in new window', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'return_value' => '_blank',
			)
		);

		$repeater->add_control(
			'slide_button_link_rel',
			array(
				'label'        => esc_html__( 'Add button nofollow', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'return_value' => 'nofollow',
			)
		);

		$this->add_control(
			'slides',
			[
				'label' => __( 'Slider Slides', 'mtelements' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[	
						'slide_image' => [
							'url' => Utils::get_placeholder_image_src() 
						],
						'slide_title' => __( 'Slide #1 Title', 'mtelements' ),
						'slide_email_address' => __( 'help@propery.com', 'mtelements' ),
						'slide_button_text' => __( 'Button #1 text.', 'mtelements' ),
						'slide_button_link' => '#'
					],
					[	
						'slide_image' => [ 
							'url' => Utils::get_placeholder_image_src() 
						],
						'slide_title' => __( 'Slide #2 Title', 'mtelements' ),
						'slide_email_address' => __( 'help@propery.com', 'mtelements' ),
						'slide_button_text' => __( 'Button #2 text.', 'mtelements' ),
						'slide_button_link' => '#'
					],
				],
				'title_field' => '{{{ slide_title }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'slider_configuration',
			[
				'label' => __( 'Slider Configuration', 'mtelements' ),
			]
		);
		$this->add_responsive_control(
			'slides_to_show',
			array(
				'label'   => esc_html__( 'Slides to Show', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'desktop_default' => [
					'1'
				],
				'tablet_default' => [
					'2'
				],
				'mobile_default' => [
					'3'
				],
				'options' => $this->get_select_range( 10 ),
			)
		);

		$this->add_control(
			'arrows',
			array(
				'label'        => esc_html__( 'Navigation', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'On', 'mtelements' ),
				'label_off'    => esc_html__( 'Off', 'mtelements' ),
				'return_value' => 'true',
				'default'      => 'true',
			)
		);

		$this->add_control(
			'autoplay',
			array(
				'label'        => esc_html__( 'Autoplay', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'On', 'mtelements' ),
				'label_off'    => esc_html__( 'Off', 'mtelements' ),
				'return_value' => 'true',
				'default'      => 'true',
			)
		);

		$this->add_control(
			'autoplay_speed',
			array(
				'label'     => esc_html__( 'Autoplay Speed', 'mtelements' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 5000,
				'condition' => [
					'autoplay' => 'true',
				],
			)
		);

		$this->add_control(
			'loop',
			array(
				'label'        => esc_html__( 'Loop', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'On', 'mtelements' ),
				'label_off'    => esc_html__( 'Off', 'mtelements' ),
				'return_value' => 'true',
				'default'      => 'true',
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'border_color',
				[
					'label' => __( 'Left Border Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'default' => '',
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slider-detail .slide-title' => 'border-left-color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'title_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slider-detail .slide-title' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .swiper-slide .slider-detail .slide-title',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_button_style',
			[
				'label' => __( 'Button', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'button_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .btn.btn-3.btn-warning' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_hover_color',
				[
					'label' => __( 'Hover Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .btn.btn-3:hover span' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_bg_color',
				[
					'label' => __( 'Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .btn.btn-3.btn-warning' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_hover_bg_color',
				[
					'label' => __( 'Hover Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .btn.btn-3.btn-warning::after' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_border_color',
				[
					'label' => __( 'Border Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .btn.btn-3.btn-warning' => 'border-color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'button_typography',
					'selector' => '{{WRAPPER}} .btn.btn-3.btn-warning',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_email_style',
			[
				'label' => __( 'Email', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'email_icon_color',
				[
					'label' => __( 'Icon Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slider-detail .slide-email-info i' => 'color: {{VALUE}} !important;',
					],
				]
			);

			$this->add_control(
				'email_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slider-detail .slide-email-info a' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'email_hover_color',
				[
					'label' => __( 'Hover Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slider-detail .slide-email-info a:hover' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'email_typography',
					'selector' => '{{WRAPPER}} .swiper-slide .slider-detail .slide-email-info a',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		if ( empty( $settings['slides'] ) ) {
			return;
		}

		$slides = [];

		switch ( $settings['banner_slider_style'] ) {
			case 'banner-slider-style-1':
				foreach ( $settings['slides'] as $index => $slide ) {
					$target = ( $slide['slide_button_link_target'] ) ? ' target="'.$slide['slide_button_link_target'].'"' : '';
					$rel = ( $slide['slide_button_link_rel'] ) ? ' rel="'.$slide['slide_button_link_rel'].'"' : '';
					$slide_html = '<div class="swiper-slide" style="background-image:url('.esc_url( $slide['slide_image']['url'] ).')">';
	    				$slide_html .= '<div class="container">';
	        				$slide_html .= '<div class="row">';
	        					$slide_html .= '<div class="col-lg-9">';
	        					    $slide_html .= '<div class="slider-detail">';
	                    				if ( ! empty( $slide['slide_title'] ) ) {
	                    					$slide_html .= '<div class="slide-title">';
	                    						$slide_html .= $slide['slide_title'];
	                    					$slide_html .= '</div>';
	                    				}
	                    				if ( ! empty( $slide['slide_button_text'] ) && ! empty( $slide['slide_button_link'] ) ) {
	                    					$slide_html .= '<div class="slide-button mt-3">';
	                    						$slide_html .= '<a href="' . esc_url( $slide['slide_button_link'] ) . '" class="btn btn-warning btn-3" ' . $target . $rel . '><span>' . $slide['slide_button_text'] . '</span></a>';
	                    					$slide_html .= '</div>';
	                    				}
	                    				if ( ! empty( $slide['slide_email_address'] ) ) {
	                    					$slide_html .= '<div class="slide-email-info">';
	                    						$slide_html .= '<i class="icon-mail text-warning" aria-hidden="true"></i>';
	                    						$slide_html .= '<a href="mailto:' . $slide['slide_email_address'] . '">';
	                    						    $slide_html .= $slide['slide_email_address'];
	                    						$slide_html .= '</a>';
	                    					$slide_html .= '</div>';
	                    				}
	                				$slide_html .= '</div>';
	            				$slide_html .= '</div>';
	            			$slide_html .= '</div>';
	    				$slide_html .= '</div>';
					$slide_html .= '</div>';

					$slides[] = $slide_html;
				}
			break;
			
			default:
			
			break;
		}

		if ( empty( $slides ) ) {
			return;
		}

		$this->add_render_attribute( [
			'slider' => [
				'class' => 'mtelements-banner-slider swiper-wrapper banner-slider-style-1',
			],
			'slider-wrapper' => [
				'class' => 'mtelements-banner-slider-wrapper swiper-container',
			],
		] );

		$slides_count = count( $settings['slides'] );

		$options = $this->get_slider_options();
		?>
		<div <?php echo $this->get_render_attribute_string( 'slider-wrapper' ); ?> data-slider_options="<?php echo htmlspecialchars( json_encode( $options ) ); ?>">
			<div <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
				<?php echo implode( '', $slides ); ?>
			</div>
			<?php if ( 1 < $slides_count && $settings['arrows'] ) : ?>
    			<div class="navigation-wrapper">
    				<div class="mtelements-swiper-button mtelements-prev-arrow<?php echo $this->get_id(); ?>">
    				    <img src="<?php echo plugins_url(). '/mtelements/assets/images/prev.png'; ?>" alt="prev-image" />
    				</div>
    				<div class="mtelements-swiper-button mtelements-next-arrow<?php echo $this->get_id(); ?>">
    				    <img src="<?php echo plugins_url(). '/mtelements/assets/images/next.png'; ?>" alt="next-image" />
    				</div>
    			</div>  
			<?php endif; ?>
		</div>

		<?php

	}

	/**
	 * Returns array with numbers in $index => $name format for numeric selects
	 *
	 * @since 1.0.0
	 *
	 * @param  integer $to Max numbers
	 * @return array
	 */
	public function get_select_range( $to = 10 ) {
		$range = range( 1, $to );
		return array_combine( $range, $range );
	}

	/**
	 * Returns array with slider options
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_slider_options() {

		$settings = $this->get_settings();
		$widget_id = $this->get_id();

		$options  = array(
			'slidesToShow'   => array(
				'desktop' => absint( $settings['slides_to_show']['0'] ),
				'tablet'  => absint( $settings['slides_to_show_tablet'] ),
				'mobile'  => absint( $settings['slides_to_show_mobile'] ),
			),
			'autoplaySpeed'  => absint( $settings['autoplay_speed'] ),
			'autoplay'       => filter_var( $settings['autoplay'], FILTER_VALIDATE_BOOLEAN ),
			'loop'       	 => filter_var( $settings['loop'], FILTER_VALIDATE_BOOLEAN ),
			'arrows'         => filter_var( $settings['arrows'], FILTER_VALIDATE_BOOLEAN ),
			'prevArrow'      => '.mtelements-prev-arrow' . $widget_id,
			'nextArrow'      => '.mtelements-next-arrow' . $widget_id,
		);

		return $options;
	}

}