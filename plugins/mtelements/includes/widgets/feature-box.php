<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Control_Media;
use Elementor\Utils;
use Elementor\Core\Schemes;
use Elementor\Group_Control_Typography;

/**
 * Mtelements Feature Box Widget Class
 *
 * The main class that initiates and runs feature box element.
 *
 * @since 1.0.0
 */
class Mtelements_Feature_Box_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-feature-box';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Feature Box', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-box';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'feature_box',
			[
				'label' => __( 'Style and Data Settings', 'mtelements' ),
			]
		);

		$this->add_control(
			'feature_box_style',
			array(
				'label'   => esc_html__( 'Style', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'feature-box-style-1',
				'options' => [
					'feature-box-style-1' => __( 'Style 1', 'mtelements' ),
				]
			)
		);

		$this->add_control(
			'feature_box_column',
			array(
				'label'   => esc_html__( 'No. of Columns', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '3',
				'options' => [ 
					'1'	=> __( '1 column', 'mtelements' ),
					'2'	=> __( '2 column', 'mtelements' ),
					'3'	=> __( '3 column', 'mtelements' ),
					'4'	=> __( '4 column', 'mtelements' ),
					'6'	=> __( '6 column', 'mtelements' ),
				]
			)
		);

		$repeater = new Repeater();

		$repeater->add_control(
			'slide_image',
			[
				'label' => __( 'Image', 'mtelements' ),
				'type' => Controls_Manager::MEDIA,
				'default' => array(
					'url' => Utils::get_placeholder_image_src(),
				),
				'show_label' => false,
			]
		);

		$repeater->add_control(
			'slide_title',
			[
				'label' => __( 'Title', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Slide title', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'slide_subtitle',
			[
				'label' => __( 'Subtitle', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Slide Subtitle', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
				'label_block' => false,
			]
		);

		$this->add_control(
			'slides',
			[
				'label' => __( 'Features', 'mtelements' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[	
						'slide_image' => [
							'url' => Utils::get_placeholder_image_src()
						],
						'slide_title' => __( 'Title #1', 'mtelements' ),
						'slide_subtitle' => __( 'Subtitle #1', 'mtelements' ),
					],
					[	
						'slide_image' => [ 
							'url' => Utils::get_placeholder_image_src() 
						],
						'slide_title' => __( 'Title #2', 'mtelements' ),
						'slide_subtitle' => __( 'Subtitle #2', 'mtelements' ),
					],
					[	
						'slide_image' => [ 
							'url' => Utils::get_placeholder_image_src() 
						],
						'slide_title' => __( 'Title #3', 'mtelements' ),
						'slide_subtitle' => __( 'Subtitle #3', 'mtelements' ),
					],
				],
				'title_field' => '{{{ slide_title }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			$this->add_control(
				'box_background_color',
				[
					'label' => __( 'Box Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .feature-outer .feature-text' => 'background: {{VALUE}};',
					],
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Title & Subtitle', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_title',
				[
					'label' => __( 'Title', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'title_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .feature-outer .feature-text .feature-title' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .feature-outer .feature-text .feature-title',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_heading_subtitle',
				[
					'label' => __( 'Subtitle', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'subtitle_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .feature-outer .feature-text .feature-subtitle' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'subtitle_typography',
					'selector' => '{{WRAPPER}} .feature-outer .feature-text .feature-subtitle',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		if ( empty( $settings['slides'] ) ) {
			return;
		}

		$slides = [];

		$colmn_class = $this->column_class( $settings['feature_box_column'] );

		$slide_html = '';
		switch ( $settings['feature_box_style'] ) {
			case 'feature-box-style-1':
				foreach ( $settings['slides'] as $index => $slide ) {
					$slide_html = '<div class="' . esc_attr( $colmn_class ) . '">';
						$slide_html .= '<div class="feature-outer">';
							if ( ! empty( $slide['slide_image']['url'] ) ) {
								$slide_html .= '<div class="feature-image-wrap">';
									$slide_html .= '<img class="feature-image" src="' . esc_url( $slide['slide_image']['url'] ) . '" alt="' . esc_attr( Control_Media::get_image_alt( $slide['slide_image'] ) ) . '" />';
								$slide_html .= '</div>';
							}
							if ( ! empty( $slide['slide_title'] ) || ! empty( $slide['slide_subtitle'] )  ) {
								$slide_html .= '<div class="feature-text">';
									if ( ! empty( $slide['slide_title'] ) ) {
										$slide_html .= '<div class="feature-title">';
											$slide_html .= $slide['slide_title'];
										$slide_html .= '</div>';
									}
									if ( ! empty( $slide['slide_subtitle'] ) ) {
										$slide_html .= '<div class="feature-subtitle">';
											$slide_html .= $slide['slide_subtitle'];
										$slide_html .= '</div>';
									}
								$slide_html .= '</div>';
							}
						$slide_html .= '</div>';
					$slide_html .= '</div>';
					$slides[] = $slide_html;
				}
			break;
		}

		$this->add_render_attribute( [
			'main-wrapper' => [
				'class' => 'mtelements-feature-box '. $settings['feature_box_style'],
			],
		] );
		?>
			<div <?php echo $this->get_render_attribute_string( 'main-wrapper' ); ?> >
				<div class="row">
					<?php echo implode( '', $slides ); ?>
				</div>
			</div>
		<?php
	}

	/**
	 * Column class from column
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function column_class( $column ) {
		$colmn_class = '';
		switch( $column ) {
            case '1':
                $colmn_class = ' col-md-12 col-sm-12 col-xs-12';
            break;
            case '2':
                $colmn_class = ' col-lg-6 col-md-6 col-sm-6 col-xs-12';
            break;
            case '3':
            default:
                $colmn_class = ' col-lg-4 col-md-6 col-sm-6 col-xs-12';
            break;
            case '4':
                $colmn_class = 'col-xl-3 col-md-6';
                break;
            case '6':
                $colmn_class = ' col-lg-2 col-md-4 col-sm-6 col-xs-12';
            break;
        }
        return $colmn_class;
	}
}