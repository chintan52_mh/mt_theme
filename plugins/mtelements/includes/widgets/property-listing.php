<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Core\Schemes;
use Elementor\Group_Control_Typography;

/**
 * Mtelements Property Listing Widget Class
 *
 * The main class that initiates and runs property listing element.
 *
 * @since 1.0.0
 */
class Mtelements_Property_Listing_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-property-listing';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Property Listing', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-home';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'properties',
			[
				'label' => __( 'Style and Data Settings', 'mtelements' ),
			]
		);

			$this->add_control(
				'property_listing_style',
				array(
					'label'   => esc_html__( 'Style', 'mtelements' ),
					'type'    => Controls_Manager::SELECT,
					'default' => 'property-listing-style-1',
					'options' => [
						'property-listing-style-1' => __( 'Style 1', 'mtelements' ),
						'property-listing-style-2' => __( 'Style 2', 'mtelements' ),
					]
				)
			);

			$this->add_control(
				'property_listing_column',
				array(
					'label'   => esc_html__( 'No. of Columns', 'mtelements' ),
					'type'    => Controls_Manager::SELECT,
					'default' => '4',
					'options' => [ 
						'1'	=> __( '1 column', 'mtelements' ),
						'2'	=> __( '2 column', 'mtelements' ),
						'3'	=> __( '3 column', 'mtelements' ),
						'4'	=> __( '4 column', 'mtelements' ),
						'6'	=> __( '6 column', 'mtelements' ),
					]
				)
			);

			$this->add_control(
				'property_orderby',
				array(
					'label'   => esc_html__( 'Orderby', 'mtelements' ),
					'type'    => Controls_Manager::SELECT,
					'default' => 'date',
					'options' => [ 
						'date'			=> __( 'Date', 'mtelements' ),
						'ID'			=> __( 'ID', 'mtelements' ),
						'author'		=> __( 'Author', 'mtelements' ),
						'title'			=> __( 'Title', 'mtelements' ),
						'name'			=> __( 'Post name', 'mtelements' ),
						'modified'		=> __( 'Modified', 'mtelements' ),
						'rand'			=> __( 'Random', 'mtelements' ),
						'comment_count'	=> __( 'Comment count', 'mtelements' ),
						'menu_order'	=> __( 'Menu order', 'mtelements' ),
					]
				)
			);

			$this->add_control(
				'property_order',
				array(
					'label'   => esc_html__( 'Order', 'mtelements' ),
					'type'    => Controls_Manager::SELECT,
					'default' => 'DESC',
					'options' => [ 
						'DESC'			=> __( 'Descending', 'mtelements' ),
						'ASC'			=> __( 'Ascending', 'mtelements' ),
					]
				)
			);

			$this->add_control(
				'property_post_per_page',
				array(
					'label'   => esc_html__( 'No. of posts per page', 'mtelements' ),
					'type'    => Controls_Manager::TEXT,
					'default' => '5',
					'description' => __( 'Default 5 posts per page', 'mtelements' ),
				)
			);

			$this->add_control(
				'property_pagination',
				array(
					'label'        => esc_html__( 'Pagination', 'mtelements' ),
					'type'         => Controls_Manager::SWITCHER,
					'label_on'     => esc_html__( 'On', 'mtelements' ),
					'label_off'    => esc_html__( 'Off', 'mtelements' ),
					'return_value' => 'true',
					'default'      => '',
				)
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			$this->add_control(
				'box_background_color',
				[
					'label' => __( 'Box Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .property .property-detail' => 'background-color: {{VALUE}};',
					],
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Title & Price', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_title',
				[
					'label' => __( 'Title', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'title_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .property .property-detail .property-title' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .property .property-detail .property-title',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_heading_price',
				[
					'label' => __( 'Price', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'price_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .property .property-detail .property-price' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'price_typography',
					'selector' => '{{WRAPPER}} .property .property-detail .property-price',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'price_symbol_color',
				[
					'label' => __( 'Symbol Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .property .property-detail .price-symbol' => 'color: {{VALUE}} !important;',
					],
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_pagination_style',
			[
				'label' => __( 'Pagination', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'property_pagination' => [ 'true' ],
				],
			]
		);
			$this->add_control(
				'pagination_color',
				[
					'label' => __( 'Pagination Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .pagination .page-numbers' => 'color: {{VALUE}};',
					],
					'condition' => [
						'property_pagination' => [ 'true' ],
					],
				]
			);

			$this->add_control(
				'pagination_active_color',
				[
					'label' => __( 'Active Pagination Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .pagination .page-numbers.current' => 'color: {{VALUE}};',
					],
					'condition' => [
						'property_pagination' => [ 'true' ],
					],
				]
			);

			$this->add_control(
				'pagination_border_color',
				[
					'label' => __( 'Border Hover Pagination Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .pagination .page-numbers.prev::before, {{WRAPPER}} .pagination .page-numbers.next::before' => 'border-color: {{VALUE}};',
					],
					'condition' => [
						'property_pagination' => [ 'true' ],
					],
				]
			);

		$this->end_controls_section();

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		if ( get_query_var('paged') ) {
            $paged = get_query_var('paged'); 
        } elseif ( get_query_var('page') ) {
            $paged = get_query_var('page'); 
        } else {
            $paged = 1;
        }

        $post_per_page = ! empty( $settings['property_post_per_page'] ) ? $settings['property_post_per_page'] : '5';

		$property_query_args = [
			'post_type' 	=> 'property',
			'post_status'   => 'publish',
			'paged'         => $paged,
			'posts_per_page'=> $post_per_page,
		];

		if ( ! empty( $settings['property_orderby'] ) ) {
            $property_query_args['orderby'] = $settings['property_orderby'];
        }
        if ( ! empty( $settings['property_order'] ) ) {
            $property_query_args['order'] =  $settings['property_order'];
        }

		$properties = new WP_Query( $property_query_args );

		$this->add_render_attribute( [
			'property-listing-wrapper' => [
				'class' => 'mtelements-property-wrapper '.$settings['property_listing_style'],
			],
		] );

		switch ( $settings['property_listing_style'] ) {
			case 'property-listing-style-1':
				if ( $properties->have_posts() ) {
					?>
					<div <?php echo $this->get_render_attribute_string( 'property-listing-wrapper' ); ?> >
						<div class="row">
							<?php
								$property_listing_count = '1';
								while( $properties->have_posts() ) : 
									$properties->the_post();
									$property_price = get_post_meta( get_the_ID(), '_mttheme_propery_price', true );

					                if ( $property_listing_count == '1' || $property_listing_count == '2' || $property_listing_count == '5' ) {
					                	$column_class = 'col-lg-6 col-md-6';
					                } else {
					                	$column_class = 'col-lg-3 col-md-6';
					                }
					                if ( $property_listing_count == '5' ) {
					                	$property_listing_count = '0';
					                }
									?>
										
										<div class="<?php echo esc_attr( $column_class ); ?>">
											<div <?php post_class( 'property' ); ?>>
												<?php if ( has_post_thumbnail() ) { ?>
													<a href="<?php echo esc_url( get_permalink() ); ?>" >
														<?php
															$property_image = '';
															if ( $property_listing_count == '1' || $property_listing_count == '2' || $property_listing_count == '5' ) {
																$property_image = get_the_post_thumbnail_url( get_the_ID(), 'mttheme-extra-medium-image' );
															} else {
																$property_image = get_the_post_thumbnail_url( get_the_ID(), 'mttheme-medium-image' );
															}
														?>
														<div class="property-image" style="background-image: url(<?php echo esc_url( $property_image ); ?>);">
														</div>
														<div class="property-detail">
															<div class="property-title">
																<?php echo get_the_title(); ?>
															</div>
															<?php if ( ! empty( $property_price ) ) { ?>
																<div class="property-price">
																	<?php echo mttheme_get_price_wrap( $property_price ); ?>
																</div>
															<?php } ?>
														</div>
													</a>
												<?php } ?>
											</div>
										</div>
										
									<?php
									$property_listing_count++;
								endwhile;
								wp_reset_postdata();
							?>
						</div>
					</div>
					<?php if ( ! empty( $settings['property_pagination'] ) && $properties->max_num_pages > 1 ) { ?>
						<div class="col-xl-12">
							<div class="pagination">
								<?php
									$current = ( $properties->query_vars['paged'] > 1 ) ? $properties->query_vars['paged'] : 1; 
									$big = 999999999; // need an unlikely integer
									echo paginate_links( array(
										'base'		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
										'format'	=> '',
										'add_args'	=> '',
										'current'	=> $current,
										'total'		=> $properties->max_num_pages,
										'prev_text'	=> '<span>' . __( 'Prev', 'mtelements' ) . '</span>',
										'next_text'	=> '<span>' . __( 'Next', 'mtelements' ) . '</span>',
										'type'		=> 'plain',
									) );
								?>
							</div>
						</div>
					<?php } ?>
					<?php
				} else{
					?>
						<div class="">
							<?php esc_html_e( 'No Property Found...', 'mtelements' ); ?>
						</div>
					<?php
				}
			break;
			
			case 'property-listing-style-2':
				if ( $properties->have_posts() ) {
					?>
					<div <?php echo $this->get_render_attribute_string( 'property-listing-wrapper' ); ?> >
						<div class="row">
							<?php
								while( $properties->have_posts() ) : 
									$properties->the_post();
									$property_price = get_post_meta( get_the_ID(), '_mttheme_propery_price', true );

									$property_images_ids = get_post_meta( get_the_ID(), '_mttheme_propery_images', true );
									$property_images_array = ! empty( $property_images_ids ) ? explode( ',', $property_images_ids ) : [];
									$property_image_count = 0;
									?>
										<div class="col-lg-12">
											<div <?php post_class( 'property' ); ?>>
												<?php if ( has_post_thumbnail() ) { ?>
													<div class="property-large-img">
														<?php
															$property_image = get_the_post_thumbnail_url( get_the_ID(), 'mttheme-extra-medium-image' ); 
														?>
														<div class="property-image" style="background-image: url(<?php echo esc_url( $property_image ); ?>);">
														</div>
														<div class="property-detail">
															<div class="property-title">
																<?php echo get_the_title(); ?>
															</div>
															<?php if ( ! empty( $property_price ) ) { ?>
																<div class="property-price">
																	<?php echo mttheme_get_price_wrap( $property_price ); ?>
																</div>
															<?php } ?>
														</div>
													</div>
													<?php if ( $property_images_array ) { ?>
														<div class="property-gallery">
															<?php
																foreach ( $property_images_array as $property_image ) {
																	if ( $property_image_count == 2 ) {
																		break;
																	}
																	if ( $property_image_count == 1 ) {
																		?>
																			<div class="property-gallery-image">
																				<?php echo wp_get_attachment_image( $property_image, 'mttheme-medium-image' ); ?>
																			</div>
																			<div class="property-link">
																				<a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-1 btn-white view-property-btn">
																					<span><?php esc_html_e( 'View all', 'mtelements' ); ?></span>
																				</a>
																			</div>
																		<?php
																	} else {
																		?>
																			<div class="property-gallery-image">
																				<?php echo wp_get_attachment_image( $property_image, 'mttheme-medium-image' ); ?>
																			</div>
																		<?php
																	}
																	$property_image_count++;
																}
															?>
														</div>
													<?php } ?>													
												<?php } ?>
											</div>
										</div>
									<?php
								endwhile;
								wp_reset_postdata();
							?>
						</div>
					</div>
					<?php if ( ! empty( $settings['property_pagination'] ) && $properties->max_num_pages > 1 ) { ?>
						<div class="col-xl-12">
							<div class="pagination">
								<?php
									$current = ( $properties->query_vars['paged'] > 1 ) ? $properties->query_vars['paged'] : 1; 
									$big = 999999999; // need an unlikely integer
									echo paginate_links( array(
										'base'		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
										'format'	=> '',
										'add_args'	=> '',
										'current'	=> $current,
										'total'		=> $properties->max_num_pages,
										'prev_text'	=> '<span>' . __( 'Prev', 'mtelements' ) . '</span>',
										'next_text'	=> '<span>' . __( 'Next', 'mtelements' ) . '</span>',
										'type'		=> 'plain',
									) );
								?>
							</div>
						</div>
					<?php } ?>
					<?php
				} else{
					?>
						<div class="">
							<?php esc_html_e( 'No Property Found...', 'mtelements' ); ?>
						</div>
					<?php
				}
			break;
		}
	}

}