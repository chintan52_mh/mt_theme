<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Core\Schemes;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Repeater;
use Elementor\Utils;

/**
 * Mtelements Team Carousel Widget Class
 *
 * The main class that initiates and runs team carousel element.
 *
 * @since 1.0.0
 */
class Mtelements_Team_Carousel_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-team-carousel';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Team Carousel', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-users';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'sliders',
			[
				'label' => __( 'Style and Data', 'mtelements' ),
			]
		);

		$repeater = new Repeater();

		$this->add_control(
			'team_carousel_style',
			array(
				'label'   => esc_html__( 'Style', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'team-carousel-style-1',
				'options' => [
					'team-carousel-style-1' => __( 'Style 1', 'mtelements' ),
				]
			)
		);

		$repeater->add_control(
			'slide_image',
			[
				'label' => __( 'Image', 'mtelements' ),
				'type' => Controls_Manager::MEDIA,
				'default' => array(
					'url' => Utils::get_placeholder_image_src(),
				),
				'show_label' => false,
			]
		);

		$repeater->add_control(
			'slide_title',
			[
				'label' => __( 'Title', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Slide title', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
				'label_block' => true,
			]
		);
        
        $repeater->add_control(
			'slide_designatation',
			[
				'label' => __( 'Designation', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Marketing manager', 'mtelements' ),
				'dynamic' => [
					'active' => true,
				],
			]
		);

		 $repeater->add_control(
			'slide_contact',
			[
				'label' => __( 'Contact Text', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
				'default' => __( 'Marketing manager', 'mtelements' ),
			]
		);
		
		$repeater->add_control(
			'slide_button_text',
			array(
				'label'       => esc_html__( 'Button Text', 'mtelements' ),
				'type'        => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
			)
		);

		$repeater->add_control(
			'slide_button_link',
			array(
				'label'       => esc_html__( 'Button Link', 'mtelements' ),
				'type'        => Controls_Manager::TEXT,
				'dynamic' => [
					'active' => true,
				],
			)
		);

		$repeater->add_control(
			'slide_button_link_target',
			array(
				'label'        => esc_html__( 'Open link in new window', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'return_value' => '_blank',
			)
		);

		$repeater->add_control(
			'slide_button_link_rel',
			array(
				'label'        => esc_html__( 'Add button nofollow', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'return_value' => 'nofollow',
			)
		);

		$this->add_control(
			'slides',
			[
				'label' => __( 'Slider Slides', 'mtelements' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[	
						'slide_title' 			=> __( 'Team #1 Title', 'mtelements' ),
						'slide_designatation' 	=> __( 'Designation #1', 'mtelements' ),
						'slide_contact' 		=> __( 'Call: 000-111-2222', 'mtelements' ),
						'slide_button_text' 	=> __( 'Button #1 text.', 'mtelements' ),
						'slide_button_link' 	=> '#'
					],
					[
						'slide_title' 			=> __( 'Team #2 Title', 'mtelements' ),
						'slide_designatation'	=> __( 'Designation #2', 'mtelements' ),
						'slide_contact' 		=> __( 'Call: 000-111-2222', 'mtelements' ),
						'slide_button_text' 	=> __( 'Button #2 text.', 'mtelements' ),
						'slide_button_link' 	=> '#'
					],
				],
				'title_field' => '{{{ slide_title }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'slider_configuration',
			[
				'label' => __( 'Slider Configuration', 'mtelements' ),
			]
		);
		$this->add_responsive_control(
			'slides_to_show',
			array(
				'label'   => esc_html__( 'Slides to Show', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'desktop_default' => [
					'4'
				],
				'tablet_default' => [
					'2'
				],
				'mobile_default' => [
					'1'
				],
				'options' => $this->get_select_range( 5 ),
			)
		);

		$this->add_control(
			'pagination',
			array(
				'label'        => esc_html__( 'Pagination', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'On', 'mtelements' ),
				'label_off'    => esc_html__( 'Off', 'mtelements' ),
				'return_value' => 'true',
				'default'      => 'true',
			)
		);

		$this->add_control(
			'autoplay',
			array(
				'label'        => esc_html__( 'Autoplay', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'On', 'mtelements' ),
				'label_off'    => esc_html__( 'Off', 'mtelements' ),
				'return_value' => 'true',
				'default'      => 'true',
			)
		);

		$this->add_control(
			'autoplay_speed',
			array(
				'label'     => esc_html__( 'Autoplay Speed', 'mtelements' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 5000,
				'condition' => array(
					'autoplay' => 'true',
				),
				'description' => __( 'Default 5000', 'mtelements' ),
			)
		);

		$this->add_control(
			'space_between',
			array(
				'label'     => esc_html__( 'Space Between Slides', 'mtelements' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 20,
				'description' => __( 'Default 20', 'mtelements' ),
			)
		);

		$this->add_control(
			'loop',
			array(
				'label'        => esc_html__( 'Loop', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'On', 'mtelements' ),
				'label_off'    => esc_html__( 'Off', 'mtelements' ),
				'return_value' => 'true',
				'default'      => 'true',
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			$this->add_control(
				'box_background_color',
				[
					'label' => __( 'Box Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Box_Shadow::get_type(),
				[
					'name' => 'button_box_shadow',
					'selector' => '{{WRAPPER}} .swiper-slide',
				]
			);

			$this->add_responsive_control(
				'team_image_size',
				[
					'label' => __( 'Image Size', 'mtelements' ),
					'type' => Controls_Manager::SLIDER,
					'default' => [
						'size' => 115,
					],
					'range' => [
						'px' => [
							'min' => 50,
							'max' => 150,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slide-image img' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$this->add_control(
				'separator_color',
				[
					'label' => __( 'Separator Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slide-button' => 'border-top-color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'pagination_color',
				[
					'label' => __( 'Pagination Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-pagination .swiper-pagination-bullet.swiper-pagination-bullet-active' => 'background-color: {{VALUE}};',
						'{{WRAPPER}} .swiper-pagination .swiper-pagination-bullet' => 'border-color: {{VALUE}};'
					],
					'condition' => [
						'pagination' => 'true',
					],
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Title & Designation', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_title',
				[
					'label' => __( 'Title', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'title_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slide-title' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .swiper-slide .slide-title',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_heading_designation',
				[
					'label' => __( 'Designation', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'designation_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slide-designation' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'designation_typography',
					'selector' => '{{WRAPPER}} .swiper-slide .slide-designation',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_contact_button_style',
			[
				'label' => __( 'Contact Text & Button', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_contact',
				[
					'label' => __( 'Contact Text', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'contact_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slide-contact' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'contact_typography',
					'selector' => '{{WRAPPER}} .swiper-slide .slide-contact',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_heading_button',
				[
					'label' => __( 'Button', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'button_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slide-button .btn.btn-link' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_hover_text_color',
				[
					'label' => __( 'Hover Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .swiper-slide .slide-button .btn.btn-link:hover, {{WRAPPER}} .swiper-slide .slide-button .btn.btn-link:focus' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'button_typography',
					'selector' => '{{WRAPPER}} .swiper-slide .slide-button .btn.btn-link',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		if ( empty( $settings['slides'] ) ) {
			return;
		}

		$slides = [];

		switch ( $settings['team_carousel_style'] ) {
			case 'team-carousel-style-1':
				foreach ( $settings['slides'] as $index => $slide ) {
					$target = ( $slide['slide_button_link_target'] ) ? ' target="'.$slide['slide_button_link_target'].'"' : '';
					$rel = ( $slide['slide_button_link_rel'] ) ? ' rel="'.$slide['slide_button_link_rel'].'"' : '';
					$slide_html = '<div class="swiper-slide">';
					    $slide_html .= '<div class="slider-detail">';
					    	if ( ! empty( $slide['slide_image'] ) ) {
	        					$slide_html .= '<div class="slide-image">';
	        						$slide_html .= '<img src="'.esc_url( $slide['slide_image']['url'] ).'" alt="" />';
	        					$slide_html .= '</div>';
	        				}
	        				if ( ! empty( $slide['slide_title'] ) ) {
	        					$slide_html .= '<div class="slide-title">';
	        						$slide_html .= $slide['slide_title'];
	        					$slide_html .= '</div>';
	        				}
	        				if ( ! empty( $slide['slide_designatation'] ) ) {
	        					$slide_html .= '<div class="slide-designation">';
	        						$slide_html .= $slide['slide_designatation'];
	        					$slide_html .= '</div>';
	        				}
	        				if ( ! empty( $slide['slide_contact'] ) ) {
	        					$slide_html .= '<div class="slide-contact">';
	        						$slide_html .= $slide['slide_contact'];
	        					$slide_html .= '</div>';
	        				}
	        				if ( ! empty( $slide['slide_button_text'] ) && ! empty( $slide['slide_button_link'] ) ) {
	        					$slide_html .= '<div class="slide-button">';
	        						$slide_html .= '<a href="' . esc_url( $slide['slide_button_link'] ) . '" class="btn btn-link" ' . $target . $rel . '>' . $slide['slide_button_text'] . '</a>';
	        					$slide_html .= '</div>';
	        				}
	    				$slide_html .= '</div>';
					$slide_html .= '</div>';

					$slides[] = $slide_html;
				}
			break;
		}

		if ( empty( $slides ) ) {
			return;
		}

		$this->add_render_attribute( [
			'slider' => [
				'class' => 'mtelements-team-carousel swiper-wrapper '. $settings['team_carousel_style'],
			],
			'slider-wrapper' => [
				'class' => 'mtelements-team-carousel-wrapper swiper-container',
			],
		] );

		$slides_count = count( $settings['slides'] );

		$options = $this->get_slider_options();
		?>
		<div <?php echo $this->get_render_attribute_string( 'slider-wrapper' ); ?> data-slider_options="<?php echo htmlspecialchars( json_encode( $options ) ); ?>">
			<div <?php echo $this->get_render_attribute_string( 'slider' ); ?>>
				<?php echo implode( '', $slides ); ?>
			</div>
			<?php if ( $settings['pagination'] ) : ?>
    			<div class="swiper-pagination mtelements-pagination<?php echo $this->get_id(); ?>"></div>
			<?php endif; ?>
		</div>

		<?php

	}

	/**
	 * Returns array with numbers in $index => $name format for numeric selects
	 *
	 * @since 1.0.0
	 *
	 * @param  integer $to Max numbers
	 * @return array
	 */
	public function get_select_range( $to = 5 ) {
		$range = range( 1, $to );
		return array_combine( $range, $range );
	}

	/**
	 * Returns array with slider options
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_slider_options() {

		$settings = $this->get_settings();
		$widget_id = $this->get_id();

		$options  = array(
			'slidesToShow'   => array(
				'desktop' => absint( $settings['slides_to_show']['0'] ),
				'tablet'  => absint( $settings['slides_to_show_tablet'] ),
				'mobile'  => absint( $settings['slides_to_show_mobile'] ),
			),
			'autoplaySpeed'  => absint( $settings['autoplay_speed'] ),
			'autoplay'       => filter_var( $settings['autoplay'], FILTER_VALIDATE_BOOLEAN ),
			'loop'       	 => filter_var( $settings['loop'], FILTER_VALIDATE_BOOLEAN ),
			'pagination'     => filter_var( $settings['pagination'], FILTER_VALIDATE_BOOLEAN ),
			'paginationel'   => '.mtelements-pagination' . $widget_id,
			'spaceBetween'	 => absint( $settings['space_between'] ),
		);

		return $options;
	}

}