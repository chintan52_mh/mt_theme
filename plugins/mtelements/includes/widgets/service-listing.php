<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Core\Schemes;
use Elementor\Group_Control_Typography;

/**
 * Mtelements Services Listing Widget Class
 *
 * The main class that initiates and runs services listing element.
 *
 * @since 1.0.0
 */
class Mtelements_Services_Listing_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-service-listing';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Services Listing', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-list';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'sliders',
			[
				'label' => __( 'Style and Data Settings', 'mtelements' ),
			]
		);

			$this->add_control(
				'service_listing_style',
				array(
					'label'   => esc_html__( 'Style', 'mtelements' ),
					'type'    => Controls_Manager::SELECT,
					'default' => 'service-listing-style-1',
					'options' => [
						'service-listing-style-1' => __( 'Style 1', 'mtelements' ),
					]
				)
			);

			$this->add_control(
				'service_listing_column',
				array(
					'label'   => esc_html__( 'No. of Columns', 'mtelements' ),
					'type'    => Controls_Manager::SELECT,
					'default' => '4',
					'options' => [ 
						'1'	=> __( '1 column', 'mtelements' ),
						'2'	=> __( '2 column', 'mtelements' ),
						'3'	=> __( '3 column', 'mtelements' ),
						'4'	=> __( '4 column', 'mtelements' ),
						'6'	=> __( '6 column', 'mtelements' ),
					]
				)
			);

			$this->add_control(
				'service_orderby',
				array(
					'label'   => esc_html__( 'Orderby', 'mtelements' ),
					'type'    => Controls_Manager::SELECT,
					'default' => 'date',
					'options' => [ 
						'date'			=> __( 'Date', 'mtelements' ),
						'ID'			=> __( 'ID', 'mtelements' ),
						'author'		=> __( 'Author', 'mtelements' ),
						'title'			=> __( 'Title', 'mtelements' ),
						'name'			=> __( 'Post name', 'mtelements' ),
						'modified'		=> __( 'Modified', 'mtelements' ),
						'rand'			=> __( 'Random', 'mtelements' ),
						'comment_count'	=> __( 'Comment count', 'mtelements' ),
						'menu_order'	=> __( 'Menu order', 'mtelements' ),
					]
				)
			);

			$this->add_control(
				'service_order',
				array(
					'label'   => esc_html__( 'Order', 'mtelements' ),
					'type'    => Controls_Manager::SELECT,
					'default' => 'DESC',
					'options' => [ 
						'DESC'			=> __( 'Descending', 'mtelements' ),
						'ASC'			=> __( 'Ascending', 'mtelements' ),
					]
				)
			);

			$this->add_control(
				'service_post_per_page',
				array(
					'label'   => esc_html__( 'No. of posts per page', 'mtelements' ),
					'type'    => Controls_Manager::TEXT,
					'default' => '8',
					'description' => __( 'Default 8 posts per page', 'mtelements' ),
				)
			);

			$this->add_control(
				'service_pagination',
				array(
					'label'        => esc_html__( 'Pagination', 'mtelements' ),
					'type'         => Controls_Manager::SWITCHER,
					'label_on'     => esc_html__( 'On', 'mtelements' ),
					'label_off'    => esc_html__( 'Off', 'mtelements' ),
					'return_value' => 'true',
					'default'      => '',
				)
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
			$this->add_control(
				'box_background_color',
				[
					'label' => __( 'Box Background Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .services a .service-text' => 'background-color: {{VALUE}};',
					],
				]
			);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Title & Highlight Title', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'separator_heading_title',
				[
					'label' => __( 'Title', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'title_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .services a .service-text' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .services a .service-text',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

			$this->add_control(
				'separator_heading_highlight',
				[
					'label' => __( 'Highlight Title', 'mtelements' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'highlight_text_color',
				[
					'label' => __( 'Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .services a .service-text span' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'highlight_typography',
					'selector' => '{{WRAPPER}} .services a .service-text span',
					'scheme' => Schemes\Typography::TYPOGRAPHY_2,
				]
			);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_pagination_style',
			[
				'label' => __( 'Pagination', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'service_pagination' => [ 'true' ],
				],
			]
		);
			$this->add_control(
				'pagination_color',
				[
					'label' => __( 'Pagination Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .pagination .page-numbers' => 'color: {{VALUE}};',
					],
					'condition' => [
						'service_pagination' => [ 'true' ],
					],
				]
			);

			$this->add_control(
				'pagination_active_color',
				[
					'label' => __( 'Active Pagination Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .pagination .page-numbers.current' => 'color: {{VALUE}};',
					],
					'condition' => [
						'service_pagination' => [ 'true' ],
					],
				]
			);

			$this->add_control(
				'pagination_border_color',
				[
					'label' => __( 'Border Hover Pagination Color', 'mtelements' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .pagination .page-numbers.prev::before, {{WRAPPER}} .pagination .page-numbers.next::before' => 'border-color: {{VALUE}};',
					],
					'condition' => [
						'service_pagination' => [ 'true' ],
					],
				]
			);

		$this->end_controls_section();

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();

		if ( get_query_var('paged') ) {
            $paged = get_query_var('paged'); 
        } elseif ( get_query_var('page') ) {
            $paged = get_query_var('page'); 
        } else {
            $paged = 1;
        }

        $post_per_page = ! empty( $settings['service_post_per_page'] ) ? $settings['service_post_per_page'] : '8';

		$service_query_args = [
			'post_type' 	=> 'services',
			'post_status'   => 'publish',
			'paged'         => $paged,
			'posts_per_page'=> $post_per_page,
		];

		if ( ! empty( $settings['service_orderby'] ) ) {
            $service_query_args['orderby'] = $settings['service_orderby'];
        }
        if ( ! empty( $settings['service_order'] ) ) {
            $service_query_args['order'] =  $settings['service_order'];
        }

		$services = new WP_Query( $service_query_args );

		$this->add_render_attribute( [
			'service-listing-wrapper' => [
				'class' => 'mtelements-service-wrapper '.$settings['service_listing_style'],
			],
		] );

		$colmn_class = $this->column_class( $settings['service_listing_column'] );

		switch ( $settings['service_listing_style'] ) {
			case 'service-listing-style-1':
				if ( $services->have_posts() ) {
					?>
					<div <?php echo $this->get_render_attribute_string( 'service-listing-wrapper' ); ?> >
						<div class="row">
							<?php
								while ( $services->have_posts() ) {
									$services->the_post();
									?>
										<div class="<?php echo esc_attr( $colmn_class ); ?>">
											<div <?php post_class( 'services' ); ?>>
												<a href="<?php echo esc_url( get_permalink() ); ?>" >
													<?php if ( has_post_thumbnail() ) { ?>
														<div class="service-image">
															 <?php echo get_the_post_thumbnail(); ?>
														</div>
													<?php } ?>
													<div class="service-text">
														<?php 	
															$post_title = get_the_title();
															$title_as_array = explode( ' ', $post_title );
															$last_word = array_pop( $title_as_array );
															$last_word_with_span = '<span>' . $last_word . '</span>';
															array_push( $title_as_array, $last_word_with_span );
															$modified_title = implode( ' ', $title_as_array );
															echo $modified_title;
														?>
													</div>
												</a>
											</div>
										</div>
									<?php
								}
								wp_reset_postdata();
							?>
						</div>
					</div>
					<?php if ( ! empty( $settings['service_pagination'] ) && $services->max_num_pages > 1 ) { ?>
						<div class="col-xl-12">
							<div class="pagination">
								<?php
									$current = ( $services->query_vars['paged'] > 1 ) ? $services->query_vars['paged'] : 1;
									$big = 999999999; // need an unlikely integer
									echo paginate_links( array(
										'base'		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
										'format'	=> '',
										'add_args'	=> '',
										'current'	=> $current,
										'total'		=> $services->max_num_pages,
										'prev_text'	=> '<span>' . __( 'Prev', 'mtelements' ) . '</span>',
										'next_text'	=> '<span>' . __( 'Next', 'mtelements' ) . '</span>',
										'type'		=> 'plain',
									) );
								?>
							</div>
						</div>
					<?php } ?>
					<?php
				}
			break;
		}
	}

	/**
	 * Column class from column
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function column_class( $column ) {
		$colmn_class = '';
		switch( $column ) {
            case '1':
                $colmn_class = ' col-md-12 col-sm-12 col-xs-12';
            break;
            case '2':
                $colmn_class = ' col-lg-6 col-md-6 col-sm-6 col-xs-12';
            break;
            case '3':
            default:
                $colmn_class = ' col-lg-4 col-md-6 col-sm-6 col-xs-12';
            break;
            case '4':
                $colmn_class = 'col-xl-3 col-md-6';
                break;
            case '6':
                $colmn_class = ' col-lg-2 col-md-4 col-sm-6 col-xs-12';
            break;
        }
        return $colmn_class;
	}
}