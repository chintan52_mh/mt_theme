<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Core\Schemes;
use Elementor\Group_Control_Typography;

/**
 * Mtelements Button Widget Class
 *
 * The main class that initiates and runs button element.
 *
 * @since 1.0.0
 */
class Mtelements_Button_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-button';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Button', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-hand-pointer';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'button_data',
			[
				'label' => __( 'Style and Data', 'mtelements' ),
			]
		);

		$this->add_control(
			'button_style',
			array(
				'label'   => esc_html__( 'Style', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'btn-1',
				'options' => [
					'btn-1' 		=> __( 'Style 1', 'mtelements' ),
					'btn-2' 		=> __( 'Style 2', 'mtelements' ),
				]
			)
		);

		$this->add_control(
			'button_text',
			array(
				'label'       => esc_html__( 'Button Text', 'mtelements' ),
				'type'        => Controls_Manager::TEXT,
			)
		);

		$this->add_control(
			'button_link',
			array(
				'label'       => esc_html__( 'Button Link', 'mtelements' ),
				'type'        => Controls_Manager::TEXT,
			)
		);

		$this->add_control(
			'button_link_target',
			array(
				'label'        => esc_html__( 'Open link in new window', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'return_value' => '_blank',
			)
		);

		$this->add_control(
			'button_link_rel',
			array(
				'label'        => esc_html__( 'Add button nofollow', 'mtelements' ),
				'type'         => Controls_Manager::SWITCHER,
				'return_value' => 'nofollow',
			)
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'button_settings',
			[
				'label' => __( 'Button Settings', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'button_text_color',
			[
				'label' => __( 'Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_hover_text_color',
			[
				'label' => __( 'Hover Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_border_color',
			[
				'label' => __( 'Border Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .btn::before' => 'border-color: {{VALUE}};',
				],
				'condition' => [
					'button_style' => 'btn-1',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'selector' => '{{WRAPPER}} .btn',
				'scheme' => Schemes\Typography::TYPOGRAPHY_2,
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();


		$this->add_render_attribute( [
			'button-wrapper' => [
				'class' => 'btn '.$settings['button_style'],
			],
		] );

		if( $settings['button_link_target'] ) {
			$this->add_render_attribute( [
				'button-wrapper' => [
					'target' => $settings['button_link_target']
				],
			] );
		}
		if( $settings['button_link_rel'] ) {
			$this->add_render_attribute( [
				'button-wrapper' => [
					'rel'   => $settings['button_link_rel'],
				],
			] );
		}

		switch ( $settings['button_style'] ) {
			case 'btn-1':
				if ( $settings['button_link'] || $settings['button_text'] ) {
					echo '<a href="' . esc_url( $settings['button_link'] ) . '" '. $this->get_render_attribute_string( 'button-wrapper' ) .' ><span>' . $settings['button_text'] . '</span></a>';
				}
			break;
			case 'btn-2':
				if ( $settings['button_link'] || $settings['button_text'] ) {
					echo '<a href="' . esc_url( $settings['button_link'] ) . '" '. $this->get_render_attribute_string( 'button-wrapper' ) .' >' . $settings['button_text'] . '</a>';
				}
			break;
		}
	}

}