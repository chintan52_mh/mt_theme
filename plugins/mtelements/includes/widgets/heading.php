<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

use Elementor\Controls_Manager;
use Elementor\Core\Schemes;
use Elementor\Group_Control_Typography;

/**
 * Mtelements Heading Widget Class
 *
 * The main class that initiates and runs accordion element.
 *
 * @since 1.0.0
 */
class Mtelements_Heading_Widget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'mttheme-heading';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'MT Heading', 'mtelements' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'fas fa-heading';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'mttheme' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function _register_controls() {

		$this->start_controls_section(
			'heading_data',
			[
				'label' => __( 'Style and Data', 'mtelements' ),
			]
		);

		$this->add_control(
			'heading_style',
			array(
				'label'   => esc_html__( 'Style', 'mtelements' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'heading-style-1',
				'options' => [
					'heading-style-1'       => __( 'Style 1', 'mtelements' ),
				]
			)
		);

		$this->add_control(
			'heading_title',
			[
				'label' => __( 'Title', 'mtelements' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Heading title', 'mtelements' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'heading_title_tag',
			[
				'label' => __( 'Title HTML Tag', 'mtelements' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => 'H1',
					'h2' => 'H2',
					'h3' => 'H3',
					'h4' => 'H4',
					'h5' => 'H5',
					'h6' => 'H6',
					'div' => 'div',
					'span' => 'span',
					'p' => 'p',
				],
				'default' => 'h2',
			]
		);

		$this->add_control(
			'heading_subtitle',
			[
				'label' => __( 'Subtitle', 'mtelements' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => __( 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'mtelements' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'heading_subtitle_tag',
			[
				'label' => __( 'Subtitle HTML Tag', 'mtelements' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => 'H1',
					'h2' => 'H2',
					'h3' => 'H3',
					'h4' => 'H4',
					'h5' => 'H5',
					'h6' => 'H6',
					'div' => 'div',
					'span' => 'span',
					'p' => 'p',
				],
				'default' => 'h4',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Title & Subtitle', 'mtelements' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'leftspacing',
			[
				'label' => __( 'Left Spacing', 'mtelements' ),
				'type' => Controls_Manager::NUMBER,
				'selectors' => [
					'{{WRAPPER}} .heading' => 'padding-left: {{VALUE}}px;',
				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => __( 'Left Border Color', 'mtelements' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .heading' => 'border-left-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'separator_heading_title',
			[
				'label' => __( 'Title', 'mtelements' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'title_text_color',
			[
				'label' => __( 'Color', 'mtelements' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .heading .heading-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .heading .heading-title',
				'scheme' => Schemes\Typography::TYPOGRAPHY_2,
			]
		);

		$this->add_control(
			'separator_heading_subtitle',
			[
				'label' => __( 'Subtitle', 'mtelements' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'subtitle_text_color',
			[
				'label' => __( 'Color', 'mtelements' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .heading .heading-subtitle' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'subtitle_typography',
				'selector' => '{{WRAPPER}} .heading .heading-subtitle',
				'scheme' => Schemes\Typography::TYPOGRAPHY_2,
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {

		$settings = $this->get_settings_for_display();


		$this->add_render_attribute( [
			'heading-wrapper' => [
				'class' => 'heading '.$settings['heading_style'],
			],
		] );

		switch ( $settings['heading_style'] ) {
			case 'heading-style-1':
			?>
				<div <?php echo $this->get_render_attribute_string( 'heading-wrapper' ); ?> >
					<?php
						if ( ! empty( $settings['heading_title'] ) ) {
							echo sprintf( '<%1$s class="heading-title">%2$s</%1$s>', $settings['heading_title_tag'], $settings['heading_title'] );
						}
					?>
					<?php 
						if ( ! empty( $settings['heading_subtitle'] ) ) {
							echo sprintf( '<%1$s class="heading-subtitle">%2$s</%1$s>', $settings['heading_subtitle_tag'], $settings['heading_subtitle'] );
						}
					?>
				</div>
			<?php
			break;
		}
	}

}