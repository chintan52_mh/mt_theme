( function( $, elementor ) {

	"use strict";
	var Mtelements = {

		init: function() {
	
			var widgets = {
				'mttheme-banner-slider.default'			: Mtelements.bannerSlider,
				'mttheme-team-carousel.default'			: Mtelements.teamCarousel,
				'mttheme-testimonial-slider.default'	: Mtelements.testimonialSlider,
				'mttheme-maps.default'					: Mtelements.googleMap,
			};

			$.each( widgets, function( widget, callback ) {
				elementor.hooks.addAction( 'frontend/element_ready/' + widget, callback );
			});

		},

		bannerSlider: function( $scope ) { 
			var $target = $scope.find( '.mtelements-banner-slider-wrapper' ),
				slideroption = $target.data('slider_options');

			var swiperOptions = {
		        slidesPerView: slideroption.slidesToShow.desktop,
		        loop: slideroption.loop,
		    };
			
			swiperOptions.breakpoints = {
				1025: {
					slidesPerView: slideroption.slidesToShow.desktop,
			    },
				767: {
					slidesPerView: slideroption.slidesToShow.tablet,
			    },
			    300: {
					slidesPerView: slideroption.slidesToShow.mobile,
			    },
			};

			if ( slideroption.autoplay ) {

				swiperOptions.autoplay = {
					delay: slideroption.autoplaySpeed
				};
			}	

			if ( slideroption.arrows ) {
				swiperOptions.navigation = {
					prevEl: slideroption.prevArrow,
					nextEl: slideroption.nextArrow
			    };
			}

			new Swiper( $target, swiperOptions );
		},

		teamCarousel: function( $scope ) { 
			var $target = $scope.find( '.mtelements-team-carousel-wrapper' ),
				slideroption = $target.data('slider_options');

			var swiperOptions = {
		        slidesPerView: slideroption.slidesToShow.desktop,
		        loop: slideroption.loop,
		        spaceBetween : slideroption.spaceBetween,
		    };
			
			swiperOptions.breakpoints = {
				1025: {
					slidesPerView: slideroption.slidesToShow.desktop,
			    },
				767: {
					slidesPerView: slideroption.slidesToShow.tablet,
			    },
			    300: {
					slidesPerView: slideroption.slidesToShow.mobile,
			    },
			};

			if ( slideroption.autoplay ) {

				swiperOptions.autoplay = {
					delay: slideroption.autoplaySpeed
				};
			}

			if ( slideroption.pagination ) {
				swiperOptions.pagination = {
					el: slideroption.paginationel,
					clickable: true,
			    };
			}
			new Swiper( $target, swiperOptions );
		},

		testimonialSlider: function( $scope ) {
			var $target = $scope.find( '.mtelements-testimonial-slider-wrapper' ),
				slideroption = $target.data('slider_options'),
				gallerySlider = $target.find('.gallery-slider-container');

			var swiperOptions = {};

			if ( slideroption.autoplay ) {

				swiperOptions.autoplay = {
					delay: slideroption.autoplaySpeed
				};
			}

			if ( slideroption.arrows ) {
				swiperOptions.navigation = {
					prevEl: slideroption.prevArrow,
					nextEl: slideroption.nextArrow
			    };
			}
			new Swiper( $target, swiperOptions );
			new Swiper( gallerySlider, { nested: true, pagination: { el: '.mtelements-gallery-pagination', clickable: true, } } );
		},

		googleMap: function( $scope ) {
			var $target = $scope.find( '.mtelements-contact-map' ),
				mapoption = $target.data('map_options');

			var myLatLng = new google.maps.LatLng(parseFloat( mapoption.latitude ),parseFloat( mapoption.longitude ));
			var mapOptions = {
			  	zoom: parseInt( mapoption.zoom ),
			  	center: myLatLng,
			  	disableDefaultUI: true,
	           	draggable: false,
			  	mapTypeId: google.maps.MapTypeId.READMAP
			};

			var gmap = new google.maps.Map( $target[0], mapOptions );

			function HTMLMarker(lat,lng){
				this.lat = lat;
				this.lng = lng;
				this.pos = new google.maps.LatLng(lat,lng);
			}

			HTMLMarker.prototype = new google.maps.OverlayView();
			HTMLMarker.prototype.onRemove= function(){}

			var div, getDirectiondiv;
			//init your html element here
			HTMLMarker.prototype.onAdd= function(){
			    div = document.createElement('DIV');
			    div.className = 'htmlmarker';
			    if ( mapoption.mapLogo && mapoption.getDirectionLink ) {

				    div.innerHTML = '<img src="'+ mapoption.mapLogo +'">';
				    getDirectiondiv = document.createElement('DIV');
				    getDirectiondiv.className = 'direction';
				    getDirectiondiv.innerHTML += '<a href="'+ mapoption.getDirectionLink +'" class="get-direction-link" '+ mapoption.getDirectionLinkTarget +'>'+ mapoption.getDirectionText +'</a>';
				    var panes = this.getPanes();
				    panes.overlayImage.appendChild(div);
				    panes.overlayImage.appendChild(getDirectiondiv);
				}
			}

			HTMLMarker.prototype.draw = function(){
			  var overlayProjection = this.getProjection();
			  var position = overlayProjection.fromLatLngToDivPixel(this.pos);
			  var panes = this.getPanes();
			  panes.overlayImage.style.left = position.x + 'px';
			  panes.overlayImage.style.top = position.y - 30 + 'px';
			}

			//to use it
			var htmlMarker = new HTMLMarker(mapoption.latlong);
			htmlMarker.setMap(gmap);
		},
	};

	$( window ).on( 'elementor/frontend/init', Mtelements.init );

}( jQuery, window.elementorFrontend ) );

