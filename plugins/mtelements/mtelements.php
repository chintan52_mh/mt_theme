<?php
/**
 * Plugin Name: MtElements
 * Description: MtElements collection of elementor elements.
 * Plugin URI:  https://www.manektech.com/
 * Version:     1.0
 * Author:      Manektech
 * Author URI:  https://www.manektech.com/
 * Text Domain: mtelements
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Main Mtelements Main Extension Class
 *
 * The main class that initiates and runs the plugin.
 *
 * @since 1.0.0
 */
final class Mtelements_Extension {

	/**
	 * Plugin Version
	 *
	 * @since 1.0.0
	 *
	 * @var string The plugin version.
	 */
	const VERSION = '1.0.0';

	/**
	 * Minimum Elementor Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum Elementor version required to run the plugin.
	 */
	const MINIMUM_ELEMENTOR_VERSION = '2.0.0';

	/**
	 * Minimum PHP Version
	 *
	 * @since 1.0.0
	 *
	 * @var string Minimum PHP version required to run the plugin.
	 */
	const MINIMUM_PHP_VERSION = '7.0';

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @static
	 *
	 * @var Mtelements_Extension The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 * @static
	 *
	 * @return Mtelements_Extension An instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {

		add_action( 'init', [ $this, 'i18n' ] );
		add_action( 'plugins_loaded', [ $this, 'init' ] );

		// Load widgets assets files.
		add_action( 'init', [ $this, 'assets_init' ], -999 );

	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 *
	 * Fired by `init` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function i18n() {

		load_plugin_textdomain( 'mtelements' );
	}

	/**
	 * Initialize the plugin
	 *
	 * Load the plugin only after Elementor (and other plugins) are loaded.
	 * Checks for basic plugin requirements, if one check fail don't continue,
	 * if all check have passed load the files required to run the plugin.
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Check if Elementor installed and activated
		if ( ! did_action( 'elementor/loaded' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ] );
			return;
		}

		// Check for required Elementor version
		if ( ! version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ] );
			return;
		}

		// Check for required PHP version
		if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
			add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ] );
			return;
		}

		// Add functions files
		require_once( __DIR__ . '/includes/property-functions.php' );

		// Add Elements category
		add_action( 'elementor/elements/categories_registered', [ $this, 'widget_category' ] );

		// Add Plugin actions
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );
	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have Elementor installed or activated.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_missing_main_plugin() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor */
			esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'mtelements' ),
			'<strong>' . esc_html__( 'Mtelements Main Extension', 'mtelements' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'mtelements' ) . '</strong>'
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required Elementor version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_elementor_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: Elementor 3: Required Elementor version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'mtelements' ),
			'<strong>' . esc_html__( 'Mtelements Main Extension', 'mtelements' ) . '</strong>',
			'<strong>' . esc_html__( 'Elementor', 'mtelements' ) . '</strong>',
			 self::MINIMUM_ELEMENTOR_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Admin notice
	 *
	 * Warning when the site doesn't have a minimum required PHP version.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function admin_notice_minimum_php_version() {

		if ( isset( $_GET['activate'] ) ) unset( $_GET['activate'] );

		$message = sprintf(
			/* translators: 1: Plugin name 2: PHP 3: Required PHP version */
			esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'mtelements' ),
			'<strong>' . esc_html__( 'Mtelements Main Extension', 'mtelements' ) . '</strong>',
			'<strong>' . esc_html__( 'PHP', 'mtelements' ) . '</strong>',
			 self::MINIMUM_PHP_VERSION
		);

		printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message );

	}

	/**
	 * Add Mttheme widgets category for elements
	 *
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function widget_category( $elements_manager ) {

		$elements_manager->add_category(
			'mttheme',
			[
				'title' => __( 'Mttheme Elements', 'mtelements' ),
				'icon' => 'fa fa-plug',
			]
		);

	}

	/**
	 * Init Widgets
	 *
	 * Include widgets files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init_widgets() {

		// Include Widget files
		require_once( __DIR__ . '/includes/widgets/banner-slider.php' );
		require_once( __DIR__ . '/includes/widgets/heading.php' );
		require_once( __DIR__ . '/includes/widgets/button.php' );
		require_once( __DIR__ . '/includes/widgets/service-listing.php' );
		require_once( __DIR__ . '/includes/widgets/team-carousel.php' );
		require_once( __DIR__ . '/includes/widgets/team.php' );
		require_once( __DIR__ . '/includes/widgets/testimonial-slider.php' );
		require_once( __DIR__ . '/includes/widgets/property-listing.php' );
		require_once( __DIR__ . '/includes/widgets/feature-box.php' );
		require_once( __DIR__ . '/includes/widgets/blog-listing.php' );
		require_once( __DIR__ . '/includes/widgets/google-map.php' );

		// Register widget
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Banner_Slider_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Heading_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Button_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Services_Listing_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Team_Carousel_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Team_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Testimonial_Slider_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Property_Listing_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Feature_Box_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Blog_Listing_Widget() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Mtelements_Google_Map_Widget() );

	}

	/**
	 * Init Widgets Assets
	 *
	 * Include widgets assets files and register them
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function assets_init() {

		require_once( __DIR__ . '/includes/class-mtelements-assets.php' );

		mtelements_assets()->init();

	}
}

Mtelements_Extension::instance();