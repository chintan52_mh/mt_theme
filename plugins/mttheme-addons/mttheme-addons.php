<?php
/**
 * Plugin Name: Mttheme Addons
 * Description: Mttheme Addons part of Mttheme theme.
 * Plugin URI:  https://www.manektech.com/
 * Version:     1.0
 * Author:      Manektech
 * Author URI:  https://www.manektech.com/
 * Text Domain: mttheme-addons
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

defined( 'MT_ADDONS_ROOT' ) or define( 'Mt_Addons_ROOT', dirname(__FILE__) );
defined( 'MT_ADDONS_ROOT_DIR' ) or define( 'MT_ADDONS_ROOT_DIR', plugins_url() . '/mttheme-addons' );
defined( 'MT_ADDONS_ROOT_PATH' ) or define( 'MT_ADDONS_ROOT_PATH', plugin_dir_path( __FILE__ ) );

defined( 'MT_ADDONS_INCLUDES' ) or define( 'MT_ADDONS_INCLUDES', MT_ADDONS_ROOT_PATH . 'includes' );
defined( 'MT_ADDONS_IMPORT' ) or define( 'MT_ADDONS_IMPORT', MT_ADDONS_INCLUDES . '/importer' );
defined( 'MT_ADDONS_WIDGET' ) or define( 'MT_ADDONS_WIDGET', MT_ADDONS_INCLUDES . '/widgets' );
defined( 'MT_ADDONS_METABOX' ) or define( 'MT_ADDONS_METABOX', MT_ADDONS_INCLUDES . '/metabox' );

defined( 'MT_ADDONS_ASSETS' ) or define( 'MT_ADDONS_ASSETS', MT_ADDONS_ROOT_DIR . '/assets' );
defined( 'MT_ADDONS_CSS_DIR' ) or define( 'MT_ADDONS_CSS_DIR', MT_ADDONS_ASSETS . '/css' );
defined( 'MT_ADDONS_JS_DIR' ) or define( 'MT_ADDONS_JS_DIR', MT_ADDONS_ASSETS . '/js' );
defined( 'MT_ADDONS_IMAGES_DIR' ) or define( 'MT_ADDONS_IMAGES_DIR', MT_ADDONS_ASSETS . '/images' );
defined( 'MT_ADDONS_IMPORT_DIR' ) or define( 'MT_ADDONS_IMPORT_DIR', MT_ADDONS_ROOT_DIR . '/includes/importer' );

/**
 * Main Mtelements Main Extension Class
 *
 * The main class that initiates and runs the plugin.
 *
 * @since 1.0.0
 */
final class Mt_Addons_Extension {

	/**
	 * Plugin Version
	 *
	 * @since 1.0.0
	 *
	 * @var string The plugin version.
	 */
	const VERSION = '1.0.0';

	/**
	 * Instance
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 * @static
	 *
	 * @var Mtelements_Extension The single instance of the class.
	 */
	private static $_instance = null;

	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 * @static
	 *
	 * @return Mtelements_Extension An instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;

	}

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {

		add_action( 'init', [ $this, 'i18n' ] );
		add_action( 'init', [ $this, 'custom_post_type' ] );
		add_action( 'plugins_loaded', [ $this, 'init' ] );

	}

	/**
	 * Load Textdomain
	 *
	 * Load plugin localization files.
	 *
	 * Fired by `init` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function i18n() {

		load_plugin_textdomain( 'mttheme-addons' );
	}

	/**
	 * Custom post type
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function custom_post_type() {
		require_once( MT_ADDONS_INCLUDES. '/custom-post-type.php' );
	}

	/**
	 * Initialize the plugin
	 *
	 * Fired by `plugins_loaded` action hook.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function init() {

		// Property function file
	    require_once( MT_ADDONS_INCLUDES.'/property-functions.php' );

		// Meta box required files
		require_once( MT_ADDONS_METABOX. '/metabox-functions.php' );
		require_once( MT_ADDONS_METABOX. '/class-metabox.php' );

		// Importer file
		require_once( MT_ADDONS_IMPORT. '/mttheme-demo-importer.php' );

		// Widgets file
	    require_once( MT_ADDONS_WIDGET.'/custom-menu-widget.php' );
	}
}

Mt_Addons_Extension::instance();