( function( $ ) {

    "use strict";
    
    $( document ).ready(function() {

        // Accordion and sorting
        $( '.mttheme-meta-accordion' ).accordion({
            collapsible : true,
            active      : true,
            heightStyle: 'content',
            header      : 'h3'
        }).sortable({
            axis        : 'y',
            handle      : 'h3',
            items       : '.mttheme_separator_box',
            update : function () {
                var arr = [];
                var i = 0;
                $(this).find( ".mttheme_separator_box" ).each(function( index ) {
                    arr.push(jQuery( this ).attr('data-id'));
                    i++;
                });
                $( this ).parent().find( '.ordering' ).val( arr ).trigger( 'change' );
            }
        });

        // Add new tab
        $( document ).on( 'click', '.mttheme-add-tab', function() {
            var t_id, tab_id;
            var mttheme_global_content =  tinyMCEPreInit.mceInit.content;
            var children        = $( '.mttheme-add-more-box .mttheme_separator_box .mttheme-textarea' );
            var children_length = $( '.mttheme-add-more-box .mttheme_separator_box .mttheme-textarea' ).length;
            if( children_length > 0 ) {

                var element = [];
                children.each( function () {
                    tab_id = $( this ).attr( 'id' );
                    tab_id = tab_id.replace( 'edit_post', '' );
                    element.push( tab_id );
                });
                var max= Math.max.apply( Math, element );
                t_id = max + 1;
            } else {
                t_id = 1;
            } 
            $.ajax({
                type : 'POST',
                url  : ajaxurl,
                data : {
                    'action':'mtaddons_custom_tab_add',
                    'tabid' : t_id
                },
                success:function( response ) {                  
                    $( '.mttheme-add-more-box' ).append( response );          
                    var newtab    = $( '.newtabid' ).val();
                    var editor_id = 'edit_post' + t_id;
                    $( '.mttheme-meta-accordion.mttheme-add-more-box' ).accordion( 'refresh' );
                    tinymce.init( mttheme_global_content );
                    tinyMCE.execCommand( 'mceAddEditor', true, editor_id );                 
                    quicktags({
                        id : editor_id
                    });
                    
                },
            });
        });

        // Remove tab
        $( document ).on( 'click', '.mttheme-remove-tab', function() {
            if ( confirm( mtthememeta.alertRemoveTab ) ) {
                $( this ).parents( 'div.mttheme_separator_box' ).remove();
                return false;
            }
        });

        // Single image upload in metabox
        $( document ).on( 'click', '.mtaddons_upload_button', function(event) {
            var file_frame;
            var button = $(this);

            var button_parent = $(this).parent();
            var id = button.attr('id').replace('_button', '');
            event.preventDefault();
            

            // If the media frame already exists, reopen it.
            if ( file_frame ) {
              file_frame.open();
              return;
            }

            // Create the media frame.
            file_frame = wp.media.frames.file_frame = wp.media({
              title: $( this ).data( 'uploader_title' ),
              button: {
                text: $( this ).data( 'uploader_button_text' ),
              },
              multiple: false  // Set to true to allow multiple files to be selected
            });

            // When an image is selected, run a callback.
            file_frame.on( 'select', function() {
                // We set multiple to false so only get one image from the uploader
                var full_attachment = file_frame.state().get('selection').first().toJSON();

                var attachment = file_frame.state().get('selection').first();

                var thumburl = attachment.attributes.sizes.thumbnail;
                var thumb_hidden = button_parent.find('.upload_field').data('id');

                if ( thumburl || full_attachment ) {
                    button_parent.find("#"+id).val(full_attachment.url);
                    button_parent.find("."+thumb_hidden+"_thumb").val(full_attachment.url);

                    button_parent.find( '.upload_image_screenshort' ).attr( 'src', full_attachment.url );
                    button_parent.find( '.upload_image_screenshort' ).slideDown();
                }
            });

            // Finally, open the modal
            file_frame.open();
        });
        
        // Single image remove button in metabox
        $( document ).on( 'click', '.mtaddons_remove_button', function(event) {
            var remove_parent = $(this).parent();
            remove_parent.find('.upload_field').val('');
            remove_parent.find('input[type="hidden"]').val('');
            remove_parent.find('.upload_image_screenshort').slideUp();
        });

        // Single image on page load add all image url to show in screenshort.
        $('.upload_field').each(function(){
          if( $(this).val() ) {
            $(this).parent().find('.upload_image_screenshort').attr("src", $(this).parent().find('input[type="hidden"]').val());
          } else {
            $(this).parent().find('.upload_image_screenshort').hide();
          }
        });

        // Multiple image upload in metabox
        $( document ).on( 'click', '.mtaddons_upload_button_multiple', function(event) {
            var file_frame;
            var button = $(this);

            var button_parent = $(this).parent();
            var id = button.attr('id').replace('_button', '');
            var app = [];
            event.preventDefault();
            

            // If the media frame already exists, reopen it.
            if ( file_frame ) {
              file_frame.open();
              return;
            }

            // Create the media frame.
            file_frame = wp.media.frames.file_frame = wp.media({
              title: $( this ).data( 'uploader_title' ),
              button: {
                text: $( this ).data( 'uploader_button_text' ),
              },
              multiple: true  // Set to true to allow multiple files to be selected
            });

            // When an image is selected, run a callback.
            file_frame.on( 'select', function() {

              var thumb_hidden = button_parent.find('.upload_field').attr('name');
             
                var selection = file_frame.state().get('selection');
                var app = [];
                    selection.map( function( attachment ) {
                    var attachment = attachment.toJSON();

                    button_parent.find('.multiple_images').append( '<div id="'+attachment.id+'"><img src="'+attachment.url+'" class="upload_image_screenshort_multiple" alt="" style="width:100px;"/><a href="javascript:void(0)" class="remove">Remove</a></div>' );
                    $('.multiple_images').each(function(){
                        if($(this).children().length > 0){
                            var attach_id = [];
                            var pr_div = $(this).parent();
                            $(this).children('div').each(function(){
                                    attach_id.push($(this).attr('id'));                     
                            });

                            pr_div.find('.upload_field').val(attach_id);
                        }else{
                            $(this).parent().find('.upload_field').val('');
                        }
                    });
                });
            });
            // Finally, open the modal
            file_frame.open();
        });

        // Multiple image remove in metabox
        $('.multiple_images').on( 'click', '.remove', function() {
            var remove_Item = $(this).parent().attr('id');
            $('.multiple_images').each(function(){
                if($(this).children().length > 0){
                    var attach_id = [];
                    var pr_div = $(this).parent();
                    $(this).children('div').each(function(){
                            attach_id.push($(this).attr('id'));                     
                    });
                    attach_id = $.grep(attach_id, function(value) {
                      return value != remove_Item;
                    });
                    pr_div.find('.upload_field').val(attach_id);
                }else{
                    $(this).parent().find('.upload_field').val('');
                }
            });

            $(this).parent().slideUp();
            $(this).parent().remove();
        });

        // Add more button for load offerd
        $('.add_field_button').on( 'click', function() {
            var target = $(this).siblings('.mt-meta-right-part'),
                dataID = $(this).data('id');
            $(target).append('<div class="add-more-wrap"><input type="text" id="'+ dataID +'" name="'+ dataID +'[]" class="" placeholder="'+ mtthememeta.placeholderText +'" ><input name="'+ dataID +'[]" class="upload_field" id="mtaddons_upload" type="text" placeholder="'+ mtthememeta.placeholderImage +'"><img class="upload_image_screenshort" style="display:none;" /><input class="mtaddons_upload_button button" id="mtaddons_upload_button" type="button" value="'+ mtthememeta.browseText +'"><span class="mtaddons_remove_button button">'+ mtthememeta.removeText +'</span><a href="javascript:void(0)" class="remove_field_button">'+ mtthememeta.removeText +'</a></div>');
        });
        
        // Remove button for load offerd
        $(document).on( 'click', '.remove_field_button', function() {
            $(this).parents('.add-more-wrap').remove();
        });
    });
        
})( jQuery );