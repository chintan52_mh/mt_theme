<?php
/**
 * Theme extra function file
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Get bedrooms array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_bedrooms_array' ) ) {
    function mttheme_get_bedrooms_array() {
        
        $bedrooms_options = [ 
                                ''  => __( 'Select', 'mttheme-addons' ),
                                '1' => '1', 
                                '2' => '2',
                                '3' => '3',
                                '4' => '4',
                                '5' => '5',
                            ];
        return apply_filters( 'mttheme_bedrooms_options', $bedrooms_options );
    }
}