<?php
/**
 * Custom Post tye for service and property
 *
 * @package mttheme-addons
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$service_labels = array(
	'name'               => _x( 'Services', 'Service', 'mttheme-addons' ),
	'singular_name'      => _x( 'Services', 'Service', 'mttheme-addons' ),
	'add_new'            => _x( 'Add New Service', 'Service', 'mttheme-addons' ),
	'add_new_item'       => __( 'Add New Service', 'mttheme-addons' ),
	'edit_item'          => __( 'Edit Service', 'mttheme-addons' ),
	'new_item'           => __( 'New Service', 'mttheme-addons' ),
	'all_items'          => __( 'All Services', 'mttheme-addons' ),
	'view_item'          => __( 'View Service', 'mttheme-addons' ),
	'search_items'       => __( 'Search Services', 'mttheme-addons' ),
	'not_found'          => __( 'No Services found', 'mttheme-addons' ),
	'not_found_in_trash' => __( 'No Services found in the Trash', 'mttheme-addons' ),
	'menu_name'          => __( 'Services', 'mttheme-addons' )
);
$services_args = array(
    'labels'             => $service_labels,
	'public'             => true,
	'capability_type'    => 'post',
	'hierarchical'  	 => true,
	'menu_position'      => 40,
	'has_archive'		 => true,
	'menu_icon'     	 => 'dashicons-screenoptions',
	'supports'           => array( 'title', 'editor','thumbnail', 'revisions', 'author', 'excerpt', 'trackpacks', 'custom-fields' ), //editor, thumbnail, title, author, excerpt, trackpacks, custom-fields, comments, revisions, page-attributes, post-formats
);

register_post_type( 'services', $services_args );

$property_labels = array(
	'name'               => _x( 'Property', 'Property', 'mttheme-addons' ),
	'singular_name'      => _x( 'Property', 'Property', 'mttheme-addons' ),
	'add_new'            => _x( 'Add New Property', 'Property', 'mttheme-addons' ),
	'add_new_item'       => __( 'Add New Property', 'mttheme-addons' ),
	'edit_item'          => __( 'Edit Property', 'mttheme-addons' ),
	'new_item'           => __( 'New Property', 'mttheme-addons' ),
	'all_items'          => __( 'All Properties', 'mttheme-addons' ),
	'view_item'          => __( 'View Property', 'mttheme-addons' ),
	'search_items'       => __( 'Search Properties', 'mttheme-addons' ),
	'not_found'          => __( 'No Properties found', 'mttheme-addons' ),
	'not_found_in_trash' => __( 'No Properties found in the Trash', 'mttheme-addons' ),
	'menu_name'          => __( 'Properties', 'mttheme-addons' )
);
$property_args = array(
    'labels'             => $property_labels,
	'public'             => true,
	'capability_type'    => 'post',
	'hierarchical'       => true,
	'has_archive'		 => true,
	'menu_position'      => 40,
	'menu_icon'     	 => 'dashicons-admin-home',
	'supports'           => array( 'title', 'editor','thumbnail', 'revisions', 'author', 'excerpt', 'trackpacks', 'custom-fields' ), //editor, thumbnail, title, author, excerpt, trackpacks, custom-fields, comments, revisions, page-attributes, post-formats
);

register_post_type( 'property', $property_args );


$labels = array(
	'name'              => _x( 'Property Types', 'Property Types', 'mttheme-addons' ),
	'singular_name'     => _x( 'Property Type', 'Property Type', 'mttheme-addons' ),
	'search_items'      => __( 'Search Property Types', 'mttheme-addons' ),
	'all_items'         => __( 'All Property Types', 'mttheme-addons' ),
	'parent_item'       => __( 'Parent Property Type', 'mttheme-addons' ),
	'parent_item_colon' => __( 'Parent Property Type:', 'mttheme-addons' ),
	'edit_item'         => __( 'Edit Property Type', 'mttheme-addons' ),
	'update_item'       => __( 'Update Property Type', 'mttheme-addons' ),
	'add_new_item'      => __( 'Add New Property Type', 'mttheme-addons' ),
	'new_item_name'     => __( 'New Property Type Name', 'mttheme-addons' ),
	'menu_name'         => __( 'Property Types', 'mttheme-addons' ),
);
$args = array( 'labels' => $labels, 'hierarchical' => true, 'show_admin_column' => true );

register_taxonomy( 'property-types', 'property', $args );