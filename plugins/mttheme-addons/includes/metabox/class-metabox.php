<?php
/**
 * Metabox main class file
 *
 * @package mttheme-addons
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Metabox Class
 *
 * The main class for metabox
 *
 * @since 1.0.0
 */
abstract class MT_Addons_Meta_Box {
	public static function add_meta_boxes() {
		add_meta_box(
			'mttheme_property_box_id',
			'Mttheme Property Settings',
			[ self::class, 'property_level_settings' ],
			[ 'property' ],
			'advanced',
			'high'
		);

		add_meta_box(
			'mttheme_page_box_id',
			'Mttheme Page Level Settings',
			[ self::class, 'page_level_settings' ],
			['page'],
			'advanced',
			'high'
		);

		add_meta_box(
			'mttheme_service_box_id',
			'Mttheme Service Level Settings',
			[ self::class, 'service_level_settings' ],
			[ 'services' ],
			'advanced',
			'high'
		);
	}
 	
 	public static function admin_metabox_custom_scripts() {

 		// load media script
		wp_enqueue_media();

		wp_enqueue_script( 'jquery-ui-accordion' );
		wp_enqueue_script( 'jquery-ui-sortable' );

		wp_enqueue_script( 'mttheme-admin-metabox', MT_ADDONS_JS_DIR.'/admin/admin-metabox.js', array( 'jquery' ) );

		wp_localize_script( 'mttheme-admin-metabox', 'mtthememeta', array(
				'alertRemoveTab' 	=> __( 'Are you sure you want to remove this tab?', 'mttheme-addons' ),
				'placeholderText' 	=> __( 'Text', 'mttheme-addons' ),
				'placeholderImage' 	=> __( 'Image', 'mttheme-addons' ),
				'browseText' 		=> __( 'Browse', 'mttheme-addons' ),
				'removeText' 		=> __( 'Remove', 'mttheme-addons' ),
			)
		);

		wp_enqueue_style( 'mttheme-admin-metabox', MT_ADDONS_CSS_DIR . '/admin/admin-metabox.css', null, Mt_Addons_Extension::VERSION );
	}

	public static function save_all_meta( $post_id ) {

		$meta_prefix = '_';

		// Delete tab data when empty or not set
		if ( ! isset( $_POST['mttheme_tabdata'] ) ) {
			delete_post_meta( $post_id, $meta_prefix.'mttheme_tabdata' );
		}

		// Delete loan offerd data when empty or not set
		if ( ! isset( $_POST['mttheme_property_loan_offered'] ) ) {
			delete_post_meta( $post_id, $meta_prefix.'mttheme_property_loan_offered' );
		}
		foreach ( $_POST as $key => $value ) {

			// Sanitize the user input.
			$mtthemedata = isset( $_POST[$key] ) ? $_POST[$key] : '';

			if ( strstr( $key, 'mttheme_') ) {

				// Meta Prefix
				
				// Update the meta field.
				update_post_meta( $post_id, $meta_prefix.$key, $mtthemedata );
			}
		}
	}
 
	public static function property_level_settings( $post ) {
		$ordering_value = get_post_meta( $post->ID, '_mttheme_ordering', true );

		$ordering_value = explode( ',', $ordering_value );

		mtaddons_meta_box_accrordion_separator_start();
			mtaddons_meta_box_main_separator_start( 
				'mttheme_propery_general',
				__( 'Property General Settings', 'mttheme-addons' )
			);
				mtaddons_meta_box_upload_multiple( 
					'mttheme_propery_images',
					__( 'Images', 'mttheme-addons' )
				);
				mtaddons_meta_box_text( 
					'mttheme_propery_price', 
					__( 'Price', 'mttheme-addons' ) 
				);
				mtaddons_meta_box_text( 
					'mttheme_property_location', 
					__( 'Location', 'mttheme-addons' ),
					__( 'Postcode of area', 'mttheme-addons' )
				);
				mtaddons_meta_box_textarea( 
					'mttheme_property_address', 
					__( 'Full Address', 'mttheme-addons' )
				);
				$mttheme_google_map_api_key = get_theme_mod( 'mttheme_google_map_api_key', '' ); 
				if ( ! $mttheme_google_map_api_key ) {
					$query['autofocus[control]'] = 'mttheme_google_map_divider';
					$control_link = add_query_arg( $query, admin_url( 'customize.php' ) );
					mtaddons_meta_box_html( 
						sprintf(
							esc_html__( 'Please set Google maps API key before using map functionality. You can create own API key  %1$s. Paste created key on %2$s', 'mttheme-addons' ),
							'<a target="_blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key">' . esc_html__( 'here', 'mttheme-addons' ) . '</a>',
							'<a target="_blank" href="' . esc_url( $control_link ) . '">' . esc_html__( 'settings page', 'mttheme-addons' ) . '</a>'
						)
					);
				}
				mtaddons_meta_box_html( 
					sprintf(
						esc_html__( 'You can get your location Latitude and Longitude from %1$s.', 'mttheme-addons' ),
						'<a target="_blank" href="https://www.latlong.net/">' . esc_html__( 'here', 'mttheme-addons' ) . '</a>'
					)
				);
				mtaddons_meta_box_text(
					'mttheme_property_map_latitude',
					__( 'Map Latitude', 'mttheme-addons' )
				);
				mtaddons_meta_box_text( 
					'mttheme_property_map_longitude',
					__( 'Map Longitude', 'mttheme-addons' )
				);
				mtaddons_meta_box_text( 
					'mttheme_property_map_link',
					__( 'Map Link', 'mttheme-addons' )
				);
				mtaddons_meta_box_text(
					'mttheme_property_contact_shortcode', 
					__( 'Contact Form Shortcode', 'mttheme-addons' )
				);
				mtaddons_meta_box_dropdown( 
					'mttheme_property_availability', 
					__( 'Availability', 'mttheme-addons' ),
					[
						'available'		=> __( 'Available', 'mttheme-addons' ),
						'sold-out'		=> __( 'Sold Out', 'mttheme-addons' )
					],
					__( 'Select Property Availability', 'mttheme-addons' )
				);
			mtaddons_meta_box_main_separator_end();
		mtaddons_meta_box_accrordion_separator_end();
		mtaddons_meta_box_accrordion_separator_start( true );
		mtaddons_meta_box_main_separator_start( 
			'mttheme_propery_aminities',
			__( 'Property Amenities', 'mttheme-addons' ),
			true
		);
			mtaddons_meta_box_dropdown( 
				'mttheme_property_bedrooms', 
				__( 'Bedrooms', 'mttheme-addons' ),
				mttheme_get_bedrooms_array()
			);
			mtaddons_meta_box_text( 
				'mttheme_property_bathrooms', 
				__( 'Bathroom', 'mttheme-addons' ) 
			);
			mtaddons_meta_box_text( 
				'mttheme_propery_squarefeet', 
				__( 'Sq. Ft', 'mttheme-addons' ) 
			);
			mtaddons_meta_box_text( 
				'mttheme_propery_parking_text', 
				__( 'Parking Text', 'mttheme-addons' ) 
			);
		mtaddons_meta_box_main_separator_end();

		mtaddons_meta_box_main_separator_start( 
			'mttheme_propery_characteristic',
			__( 'Property Characteristics', 'mttheme-addons' ),
			true
		);
			mtaddons_meta_box_text(
				'mttheme_property_built', 
				__( 'Year Built', 'mttheme-addons' )
			);
			mtaddons_meta_box_text(
				'mttheme_property_lot', 
				__( 'Lot Size', 'mttheme-addons' )
			);
			mtaddons_meta_box_text(
				'mttheme_property_hoa', 
				__( 'HOA', 'mttheme-addons' )
			);
		mtaddons_meta_box_main_separator_end();

		mtaddons_meta_box_main_separator_start( 
			'mttheme_propery_floorplan',
			__( 'Property Floorplan', 'mttheme-addons' ),
			true
		);
			mtaddons_meta_box_upload_multiple( 
				'mttheme_propery_flor_plan',
				__( 'Floorplan', 'mttheme-addons' )
			);
		mtaddons_meta_box_main_separator_end();

		mtaddons_meta_box_main_separator_start( 
			'mttheme_propery_neighbourhood',
			__( 'Property Neighbourhood', 'mttheme-addons' ),
			true
		);
			mtaddons_meta_box_text( 
				'mttheme_propery_neighborhood_age', 
				__( 'Neighborhood Age', 'mttheme-addons' ) 
			);
			mtaddons_meta_box_text( 
				'mttheme_propery_neighborhood_married', 
				__( 'Neighborhood Married', 'mttheme-addons' ) 
			);
			mtaddons_meta_box_text( 
				'mttheme_propery_neighborhood_college', 
				__( 'Neighborhood College Grads', 'mttheme-addons' ) 
			);
			mtaddons_meta_box_text( 
				'mttheme_propery_neighborhood_build', 
				__( 'Neighborhood Build Around', 'mttheme-addons' ) 
			);
			mtaddons_meta_box_text(
				'mttheme_propery_neighborhood_ownership',
				__( 'Neighborhood Owernership Own Percentage', 'mttheme-addons' ),
				'',
				__( 'Add only numeric value', 'mttheme-addons' )
			);
		mtaddons_meta_box_main_separator_end();

		mtaddons_meta_box_main_separator_start( 
			'mttheme_propery_loan',
			__( 'Property Loan Offered by Banks', 'mttheme-addons' ),
			true
		);
			mtaddons_meta_box_addmore_images(
				'mttheme_property_loan_offered', 
				__( 'Loan offered by bank', 'mttheme-addons' )
			);
		mtaddons_meta_box_main_separator_end();
		$custom_tab_data = get_post_meta( $post->ID, '_mttheme_tabdata', true );
		if ( ! empty( $custom_tab_data['tabtitle'] ) ) {
			$counter = 0;
			foreach ( $custom_tab_data['tabtitle'] as $title ) {
				?>
					<div class="mttheme_separator_box" data-id="<?php echo esc_attr( $counter ); ?>">
			            <h3 class="mttheme-single-product-tab-title">
			                <?php echo esc_html( $title ); ?>
			                <a class="mttheme-remove-tab" href="javascript:void(0);">❌</a>
			            </h3>

			            <div class="content-wrappper">
			                <div class="mttheme-single-tab-details-title" >
			                    <lable><?php echo esc_html__( 'Tab Title', 'mttheme-addons' ); ?></lable>
			                        <input type="text" class="mttheme_input"  placeholder="<?php echo esc_html__( 'Title', 'mttheme-addons' ); ?>" name="mttheme_tabdata[tabtitle][]" value="<?php echo esc_attr( $title ); ?>" />
			                </div>
			                <div class="mttheme-single-tab-details-description" >

			                <?php
			                	$content = $custom_tab_data['tabdescription'][$counter];
		                        $settings = array(
		                            'wpautop' => true,
		                            'media_buttons' => false,
		                            'textarea_name' => 'mttheme_tabdata[tabdescription][]',
		                            'textarea_rows' => 10,
		                            'teeny' => true,
		                            'tinymce' => true,
		                            'editor_class' => 'mttheme-textarea'
		                        );
		                        $tabdescription_id = 'edit_post' . $counter;
		                        wp_editor( $content, $tabdescription_id, $settings );
			                ?>
			                <input type="hidden" name="tabdata[tabuniquekey][]" value="" />
			                        
			                </div>
			            </div>
			            <input type="hidden" class="newtabid" value="<?php echo esc_attr( $counter ); ?>">
			        </div>
				<?php
				$counter++;
			}
		}
		mtaddons_meta_box_accrordion_separator_end();
		mtaddons_add_more_tab();
	}

	public static function page_level_settings( $post ) {
		mtaddons_meta_box_textarea( 
			'mttheme_page_subtitle_single', 
			__( 'Page Subtitle', 'mttheme-addons' )
		);
		mtaddons_meta_box_dropdown(
			'mttheme_title_enable_single', 
			__( 'Enable Title', 'mttheme-addons' ),
			[ 
				'default'	=> __( 'Default', 'mttheme-addons' ),
				'on'		=> __( 'On', 'mttheme-addons' ),
				'off'		=> __( 'Off', 'mttheme-addons' )
			],
			__( 'If on, a title will display in page', 'mttheme-addons' )
		);

		mtaddons_meta_box_dropdown(
			'mttheme_header_top_space_single', 
			__( 'Header Top Space', 'mttheme-addons' ),
			[ 
				'default'	=> __( 'Default', 'mttheme-addons' ),
				'on'		=> __( 'On', 'mttheme-addons' ),
				'off'		=> __( 'Off', 'mttheme-addons' )
			]
		);

		mtaddons_meta_box_colorpicker( 
			'mttheme_header_bg_color_single', 
			__( 'Header Background Color', 'mttheme-addons' )
		);
	}

	public static function service_level_settings( $post ) {
		mtaddons_meta_box_upload( 
			'mttheme_service_banner_image_single', 
			__( 'Banner Image', 'mttheme-addons' )
		);
		mtaddons_meta_box_textarea( 
			'mttheme_service_subtitle_single', 
			__( 'Service Subtitle', 'mttheme-addons' )
		);
	}
}
 
add_action( 'add_meta_boxes', [ 'MT_Addons_Meta_Box', 'add_meta_boxes' ] );
add_action( 'save_post', [ 'MT_Addons_Meta_Box', 'save_all_meta' ] );
add_action( 'admin_enqueue_scripts', [ 'MT_Addons_Meta_Box', 'admin_metabox_custom_scripts' ] );