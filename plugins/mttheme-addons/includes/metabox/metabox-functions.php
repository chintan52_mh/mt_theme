<?php
/**
 * Metabox map files
 *
 * @package mttheme-addons
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if ( ! function_exists( 'mtaddons_meta_box_text' ) ) {
    function mtaddons_meta_box_text( $mtaddonsid, $mtaddons_label, $mtaddons_placeholder = '', $mtaddons_desc = '', $mtaddons_short_desc = '' ) {
        global $post;

        // Meta Prefix
        $meta_prefix = '_';
        $mtaddons_placeholder = ( $mtaddons_placeholder ) ? 'placeholder="' . $mtaddons_placeholder . '"' : '';

        $html = '';
        $html .= '<div class="'.esc_attr( $mtaddonsid ).'_box description_box">';
        $html .= '<div class="mt-meta-left-part">';
            $html .= $mtaddons_label;
            if( $mtaddons_desc ) {
                $html .= '<span class="description">' . $mtaddons_desc . '</span>';
            }
        $html .='</div>';
        $html .= '<div class="mt-meta-right-part">';
            $html .= '<input type="text" id="' . esc_attr( $mtaddonsid ) . '" name="' . esc_attr( $mtaddonsid ) . '" value="' . esc_attr( get_post_meta( $post->ID, $meta_prefix.$mtaddonsid, true ) ) . '" ' . $mtaddons_placeholder . ' />';
            if( $mtaddons_short_desc ) {
                $html .= '<span class="short-description">' . $mtaddons_short_desc . '</span>';
            }
        $html .= '</div>';
        $html .= '</div>';
        echo sprintf( '%s', $html );
    }
}

if ( ! function_exists( 'mtaddons_meta_box_textarea' ) ) {
    function mtaddons_meta_box_textarea( $mtaddonsid, $mtaddons_label, $mtaddons_desc = '', $mtaddons_short_desc = '' ) {
        global $post;

        // Meta Prefix
        $meta_prefix = '_';

        $html = '';
        $html .= '<div class="'.esc_attr( $mtaddonsid ).'_box description_box">';
        $html .= '<div class="mt-meta-left-part">';
            $html .= $mtaddons_label;
            if( $mtaddons_desc ) {
                $html .= '<span class="description">' . $mtaddons_desc . '</span>';
            }
        $html .='</div>';
        $html .= '<div class="mt-meta-right-part">';
            $html .= '<textarea cols="120" id="' . esc_attr( $mtaddonsid ) . '" name="' . esc_attr( $mtaddonsid ) . '">' . get_post_meta( $post->ID, $meta_prefix.$mtaddonsid, true ) . '</textarea>';
            if( $mtaddons_short_desc ) {
                $html .= '<span class="short-description">' . $mtaddons_short_desc . '</span>';
            }
        $html .= '</div>';
        $html .= '</div>';
        echo sprintf( '%s', $html );
    }
}

if ( ! function_exists( 'mtaddons_meta_box_html' ) ) {
    function mtaddons_meta_box_html( $mtaddons_html = '' ) {
        echo sprintf( '%s', $mtaddons_html );
    }
}

if ( ! function_exists( 'mtaddons_meta_box_dropdown' ) ) {
    function mtaddons_meta_box_dropdown( $mtaddonsid, $mtaddons_label, $mtaddons_options, $mtaddons_desc = '', $mtaddons_short_desc = '' ) {
        global $post;

        // Meta Prefix
        $meta_prefix = '_';

        $selected_value = get_post_meta( $post->ID, $meta_prefix.$mtaddonsid, true );
        $html = '';
        $html .= '<div class="' . esc_attr( $mtaddonsid ) . '_box description_box">';
            $html .= '<div class="mt-meta-left-part">';
                $html .= $mtaddons_label;
                if( $mtaddons_desc ) {
                    $html .= '<span class="description">' . $mtaddons_desc . '</span>';
                }
            $html .='</div>';
            $html .= '<div class="mt-meta-right-part">';
                $html .= '<select id="' . esc_attr( $mtaddonsid ) . '" name="' . esc_attr( $mtaddonsid ) . '">';
                    foreach( $mtaddons_options as $key => $option ) {
                        $html .= '<option ' . selected( $selected_value, $key, false ) . ' value="' . esc_attr( $key ) . '">' . esc_html( $option ) . '</option>';
                    }
                $html .= '</select>';
                if( $mtaddons_short_desc ) {
                    $html .= '<span class="short-description">' . $mtaddons_short_desc . '</span>';
                }
            $html .='</div>';   
        $html .= '</div>';
        echo sprintf( '%s', $html );
    }
}

if ( ! function_exists( 'mtaddons_meta_box_upload' ) ) {
    function mtaddons_meta_box_upload( $mtaddonsid, $mtaddons_label, $mtaddons_desc = '' ) {
        global $post;

        // Meta Prefix
        $meta_prefix = '_';

        $html = '';
        $html .= '<div class="' . esc_attr( $mtaddonsid ) . '_box description_box">';
            $html .= '<div class="mt-meta-left-part">';
                $html .= $mtaddons_label;
                if( $mtaddons_desc ) {
                    $html .= '<span class="description">' . $mtaddons_desc . '</span>';
                }
            $html .='</div>';
            $html .= '<div class="mt-meta-right-part">';
                $html .= '<input name="' . esc_attr( $mtaddonsid ) . '" data-id="' . esc_attr( $mtaddonsid ) . '" class="upload_field" id="mtaddons_upload" type="text" value="' . get_post_meta( $post->ID, $meta_prefix.$mtaddonsid, true ) . '" />';
                $html .= '<input name="'. $mtaddonsid.'_thumb" class="' . $mtaddonsid . '_thumb" id="' . $mtaddonsid . '_thumb" type="hidden" value="' . get_post_meta( $post->ID, $meta_prefix.$mtaddonsid, true ) . '" />';
                        $html .= '<img class="upload_image_screenshort" src="' . get_post_meta( $post->ID, $meta_prefix.$mtaddonsid, true ) . '" />';
                $html .= '<input class="mtaddons_upload_button button" id="mtaddons_upload_button" type="button" value="' . esc_attr__( 'Browse', 'mttheme-addons' ) . '" />';
                $html .= '<span class="mtaddons_remove_button button">' . esc_html__( 'Remove', 'mttheme-addons' ) . '</span>';
                        
            $html .='</div>';
        $html .= '</div>';
        echo sprintf("%s",$html);
    }
}

if ( ! function_exists( 'mtaddons_meta_box_upload_multiple' ) ) {
    function mtaddons_meta_box_upload_multiple( $mtaddons_id, $mtaddons_label, $mtaddons_desc = '' ) {
        global $post;

        // Meta Prefix
        $meta_prefix = '_';

        $html = '';
        $html .= '<div class="' . esc_attr( $mtaddons_id ) . '_box description_box">';
            $html .= '<div class="mt-meta-left-part">';
                $html .= $mtaddons_label;
                if( $mtaddons_desc ) {
                    $html .= '<span class="description">' . $mtaddons_desc . '</span>';
                }
            $html .='</div>';
            $html .= '<div class="mt-meta-right-part">';
            
                $html .= '<input name="' . esc_attr( $mtaddons_id ) . '" class="upload_field upload_field_multiple" id="mtaddons_upload" type="hidden" value="' . get_post_meta( $post->ID, $meta_prefix.$mtaddons_id, true ) . '" />';
                $html .= '<div class="multiple_images">';
                $mtaddons_val = explode( ',', get_post_meta( $post->ID, $meta_prefix.$mtaddons_id, true ) );
                
                foreach ( $mtaddons_val as $key => $value ) {
                    if( ! empty( $value ) ):
                        $mtaddons_image_url = wp_get_attachment_url( $value );
                        $html .='<div id=' . esc_attr( $value ) . '>';
                            $html .= '<img class="upload_image_screenshort_multiple" src="' . esc_url( $mtaddons_image_url ) . '" style="width:100px;" />';
                            $html .= '<a href="javascript:void(0)" class="remove">' . __( 'Remove', 'mttheme-addons' ) . '</a>';
                        $html .= '</div>';
                    endif;
                }
                $html .= '</div>';
                $html .= '<input class="mtaddons_upload_button_multiple button" id="mtaddons_upload_button_multiple" type="button" value="' .__( 'Browse', 'mttheme-addons' ) . '" />' . __( ' Select Files', 'mttheme-addons' );
                        
            $html .='</div>';
        $html .= '</div>';
        echo sprintf( "%s", $html );
    }
}

if ( ! function_exists( 'mtaddons_meta_box_colorpicker' ) ) {
    function mtaddons_meta_box_colorpicker( $mtaddonsid, $mtaddons_label, $mtaddons_desc = '' ) {
        global $post;

        // Meta Prefix
        $meta_prefix = '_';

        $html = '';
        $html .= '<div class="'.esc_attr( $mtaddonsid ).'_box description_box">';
            $html .= '<div class="mt-meta-left-part">';
                $html .= $mtaddons_label;
                if( $mtaddons_desc ) {
                    $html .= '<span class="description">' . $mtaddons_desc . '</span>';
                }
            $html .='</div>';
            $html .= '<div class="mt-meta-right-part">';
                $html .= '<input type="text" class="mttheme-color-picker" id="' . esc_attr( $mtaddonsid ) . '" name="' . esc_attr( $mtaddonsid ) . '" value="' . get_post_meta($post->ID, $meta_prefix.$mtaddonsid, true) . '" />';
            $html .='</div>';
        $html .='</div>';
        echo sprintf( '%s', $html );
    }
}

if ( ! function_exists( 'mtaddons_meta_box_addmore_images' ) ) {
    function mtaddons_meta_box_addmore_images( $mtaddonsid, $mtaddons_label, $mtaddons_desc = '' ) {
        global $post;

        // Meta Prefix
        $meta_prefix = '_';
        $data_value = get_post_meta( $post->ID, $meta_prefix.$mtaddonsid, true );
       
        $html = '';
        $html .= '<div class="' . esc_attr( $mtaddonsid ) . '_box description_box">';
            $html .= '<div class="mt-meta-left-part">';
                $html .= $mtaddons_label;
                if( $mtaddons_desc ) {
                    $html .= '<span class="description">' . $mtaddons_desc . '</span>';
                }
            $html .='</div>';
            $html .= '<div class="mt-meta-right-part">';
                if ( ! empty( $data_value ) ) {
                    $chunks = array_chunk( $data_value, 2 );
                    $text = array_column( $chunks, 0 );
                    $images  = array_column( $chunks, 1 );
                    $counter = 0;
                    foreach ( $text as $value ) {
                        $html .= '<div class="add-more-wrap">';
                            $html .= '<input type="text" id="' . esc_attr( $mtaddonsid ) . '" placeholder="' . esc_attr__( 'Text', 'mttheme-addons' ) . '" name="' . esc_attr( $mtaddonsid ) . '[]" value="' . esc_attr( $value ) . '" />';
                            $html .= '<input name="' . esc_attr( $mtaddonsid ) . '[]" data-id="' . esc_attr( $mtaddonsid ) . '" class="upload_field" id="mtaddons_upload" type="text" placeholder="' . esc_attr__( 'Image', 'mttheme-addons' ) . '" value="' . esc_attr( $images[$counter] ) . '" />';
                            $html .= '<img class="upload_image_screenshort" src="' . esc_url( $images[$counter] ) . '" />';
                            $html .= '<input class="mtaddons_upload_button button" id="mtaddons_upload_button" type="button" value="' . esc_attr__( 'Browse', 'mttheme-addons' ) . '" />';
                            $html .= '<span class="mtaddons_remove_button button">' . esc_html__( 'Remove', 'mttheme-addons' ) . '</span>';
                        $html .= '<a href="javascript:void(0)" class="remove_field_button">' . __( 'Remove', 'mttheme-addons' ) . '</a>';
                        $html .= '</div>';
                        $counter++;
                    }
                }
            $html .='</div>';
            $html .= '<a class="add_field_button button-primary" href="javascript:void(0)" data-id="' . esc_attr( $mtaddonsid ) . '">' . esc_html__( 'Add New', 'mttheme-addons' ) . '</a>';
        $html .= '</div>';
        echo sprintf("%s",$html);
    }
}

// Meta Box Main Wrap Start
if( ! function_exists( 'mtaddons_meta_box_main_separator_start' ) ) {
    function mtaddons_meta_box_main_separator_start( $id, $label, $dradable = false ) {
        $html = '';
        $class = ( $dradable ) ? ' mttheme_separator_box' : '';
        $html .= '<div class="' . esc_attr( $id ) . '_box' . esc_attr( $class ) . '" data-id="' . esc_attr( $id ) . '">';
            $html .= '<h3>' . $label . '</h3>';
                $html .= '<div class="content-wrappper">';
        echo sprintf( '%s', $html );
    }
}

if( ! function_exists( 'mtaddons_meta_box_accrordion_separator_start' ) ) {
    function mtaddons_meta_box_accrordion_separator_start( $add_more = false ) {
        global $post;
        $data_value = get_post_meta( $post->ID, '_mttheme_ordering', true );
        $class = ( $add_more ) ? ' mttheme-add-more-box' : '';
        $html = '';
        $html .= '<div class="mttheme-meta-accordion' . esc_attr( $class ) . '">';
            $html .= '<input type="hidden" name="mttheme_ordering" class="ordering" value="' . esc_attr( $data_value ) . '">';
        echo sprintf( '%s', $html );
    }
}

if( ! function_exists( 'mtaddons_meta_box_accrordion_separator_end' ) ) {
    function mtaddons_meta_box_accrordion_separator_end() {
        $html = '';
        $html .= '</div>';
        echo sprintf( '%s', $html );
    }
}

if( ! function_exists( 'mtaddons_meta_box_main_separator_end' ) ) {
    function mtaddons_meta_box_main_separator_end() {
        $html = '';
        $html .= '</div></div>';
        echo sprintf( '%s', $html );
    }
}

if( ! function_exists( 'mtaddons_add_more_tab' ) ) {
    function mtaddons_add_more_tab() {
        $html = '';
        $html .= '<a id="add-row" class="button mttheme-add-tab button-primary" href="javascript:void(0);">' . esc_html__( 'Add New Tab', 'mttheme-addons' ) . '</a>';
        echo sprintf( '%s', $html );
    }
}

// Add More Product Tab Ajax Call
if(! function_exists( 'mtaddons_custom_tab_add' ) ) {
    function mtaddons_custom_tab_add() {
        $newtabid = ( ! empty ( $_POST['tabid'] ) ) ? $_POST['tabid'] : '';
        ?>
        <div class="mttheme_separator_box" data-id="<?php echo esc_attr( $newtabid ); ?>">
            <h3 class="mttheme-single-product-tab-title">
                <?php esc_html_e( 'New Tab', 'mttheme-addons' ); ?>
                <a class="mttheme-remove-tab" href="javascript:void(0);">❌</a>
            </h3>

            <div class="content-wrappper">
                <div class="mttheme-single-tab-details-title" >
                    <?php 
                        if( $field['tabuniquekey'] == '' ) {
                    ?>
                        <lable><?php echo esc_html__( 'Tab Title', 'mttheme-addons' ); ?></lable>
                        <input type="text" class="mttheme_input"  placeholder="<?php echo esc_html__( 'Title', 'mttheme-addons' ); ?>" name="mttheme_tabdata[tabtitle][]" value="" />
                    <?php } else { ?>
                        <input type="hidden" class="mttheme_input"  placeholder="<?php echo esc_html__( 'Title', 'mttheme-addons' ); ?>" name="mttheme_tabdata[tabtitle][]" value="" />
                        <p><?php esc_html_e( 'Show default content', 'mttheme-addons' ); ?></p>
                    <?php } ?>
                </div>
                <div class="mttheme-single-tab-details-description" >

                <?php
                    if( $field['tabuniquekey'] == '' ) {
                        $settings = array(
                            'wpautop' => true,
                            'media_buttons' => false,
                            'textarea_name' => 'mttheme_tabdata[tabdescription][]',
                            'textarea_rows' => 10,
                            'teeny' => true,
                            'tinymce' => true,
                            'editor_class' => 'mttheme-textarea'
                        );
                        $tabdescription_id = 'edit_post' . $newtabid;
                        wp_editor( $content, $tabdescription_id, $settings );
                        ?>
                            <input type="hidden" name="tabdata[tabuniquekey][]" value="" />
                        <?php
                    } else { ?>
                        <input type="hidden" name="mttheme_tabdata[tabdescription][]" />
                        <input type="hidden" name="mttheme_tabdata[tabuniquekey][]" value="" />
                    <?php
                    }
                    ?>
                </div>
            </div>
            <input type="hidden" class="newtabid" value="<?php echo esc_attr( $newtabid ); ?>">
        </div>
        <?php
        die();
    }
}
add_action( 'wp_ajax_mtaddons_custom_tab_add', 'mtaddons_custom_tab_add' );
add_action( 'wp_ajax_nopriv_mtaddons_custom_tab_add', 'mtaddons_custom_tab_add' );