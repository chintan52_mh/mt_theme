jQuery( document ).ready( function( $ ) {
	"use strict";

	var mttheme_addons = {
		init: function() {
			this.$progress = $( '#mttheme-addons-demo-import-progress' );
			this.$log = $( '#mttheme-addons-demo-import-log' );
			this.$importer = $( '#mttheme-addons-demo-importer' );
			this.steps = ['content', 'customizer', 'widgets', 'sliders'];

			if ( ! this.$importer.length ) {
				return;
			}

			/**
			 * The first step: download content file
			 */
			this.download( mttheme_addons.steps.shift() );
		},

		download: function( type ) {
			mttheme_addons.log( 'Downloading ' + type + ' file' );

			$.get(
				ajaxurl,
				{
					action: 'mttheme_addons_download_file',
					type: type,
					demo: mttheme_addons.$importer.find( 'input[name="demo"]' ).val(),
					_wpnonce: mttheme_addons.$importer.find( 'input[name="_wpnonce"]' ).val()
				},
				function( response ) {
					if ( response.success ) {
						mttheme_addons.import( type );
					} else {
						mttheme_addons.log( response.data );

						if ( mttheme_addons.steps.length ) {
							mttheme_addons.download( mttheme_addons.steps.shift() );
						} else {
							mttheme_addons.configTheme();
						}
					}
				}
			).fail( function() {
				mttheme_addons.log( 'Failed' );
			} );
		},

		import: function( type ) {
			mttheme_addons.log( 'Importing ' + type );

			var data = {
					action: 'mttheme_addons_import',
					type: type,
					_wpnonce: mttheme_addons.$importer.find( 'input[name="_wpnonce"]' ).val()
				};
			var url = ajaxurl + '?' + $.param( data );
			var evtSource = new EventSource( url );

			evtSource.addEventListener( 'message', function ( message ) {
				var data = JSON.parse( message.data );

				switch ( data.action ) {
					case 'updateTotal':
						console.log( data.delta );
						break;

					case 'updateDelta':
						console.log(data.delta);
						break;

					case 'complete':
						evtSource.close();
						mttheme_addons.log( type + ' has been imported successfully!' );

						if ( mttheme_addons.steps.length ) {
							mttheme_addons.download( mttheme_addons.steps.shift() );
						} else {
							mttheme_addons.configTheme();
						}

						break;
				}
			} );

			evtSource.addEventListener( 'log', function ( message ) {
				var data = JSON.parse( message.data );
				mttheme_addons.log( data.message );
			});
		},

		configTheme: function() {
			$.get(
				ajaxurl,
				{
					action: 'mttheme_addons_config_theme',
					demo: mttheme_addons.$importer.find( 'input[name="demo"]' ).val(),
					_wpnonce: mttheme_addons.$importer.find( 'input[name="_wpnonce"]' ).val()
				},
				function( response ) {
					if ( response.success ) {
						mttheme_addons.generateImages();
					}

					mttheme_addons.log( response.data );
				}
			).fail( function() {
				mttheme_addons.log( 'Failed' );
			} );
		},

		generateImages: function() {
			$.get(
				ajaxurl,
				{
					action: 'mttheme_addons_get_images',
					_wpnonce: mttheme_addons.$importer.find( 'input[name="_wpnonce"]' ).val()
				},
				function( response ) {
					if ( ! response.success ) {
						mttheme_addons.log( response.data );
						mttheme_addons.log( 'Import completed!' );
						mttheme_addons.$progress.find( '.spinner' ).hide();
						return;
					} else {
						var ids = response.data;

						if ( ! ids.length ) {
							mttheme_addons.log( 'Import completed!' );
							mttheme_addons.$progress.find( '.spinner' ).hide();
						}

						mttheme_addons.log( 'Starting generate ' + ids.length + ' images' );

						mttheme_addons.generateSingleImage( ids );
					}
				}
			);
		},

		generateSingleImage: function( ids ) {
			if ( ! ids.length ) {
				mttheme_addons.log( 'Import completed!' );
				mttheme_addons.$progress.find( '.spinner' ).hide();
				return;
			}

			var id = ids.shift();

			$.get(
				ajaxurl,
				{
					action: 'mttheme_addons_generate_image',
					id: id,
					_wpnonce: mttheme_addons.$importer.find( 'input[name="_wpnonce"]' ).val()
				},
				function( response ) {
					mttheme_addons.log( response.data + ' (' + ids.length + ' images left)' );

					mttheme_addons.generateSingleImage( ids );
				}
			);
		},

		log: function( message ) {
			mttheme_addons.$progress.find( '.text' ).text( message );
			mttheme_addons.$log.append( '<p>' + message + '</p>' );
		}
	};


	mttheme_addons.init();
} );
