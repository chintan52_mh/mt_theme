<?php
/**
 * The main template file
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header();

$mttheme_title_enable = mttheme_option( 'mttheme_title_enable', 'on' );

$mttheme_home_right = get_theme_mod( 'mttheme_home_right_sidebar', 'sidebar-1' );
$mttheme_home_right_sidebar = mttheme_check_active_sidebar( $mttheme_home_right );
$mttheme_home_layout_width   = get_theme_mod( 'mttheme_home_layout_width', 'container-fluid' );

global $wp_query;
?>
	<div class="mttheme-main-content-wrap">
		<?php
			if ( $mttheme_title_enable == 'on' ) {
				get_template_part( 'templates/page-title/title' );
			}
		?>
		<div class="<?php echo esc_attr( $mttheme_home_layout_width ); ?> mt-5 mb-5">
			<div class="mtelements-blog-wrapper blog-listing-style-1">
				<?php
				if ( have_posts() ) :
					$column_class = ( $mttheme_home_right_sidebar ) ? 'col-lg-9 col-md-8' : 'col-md-12';
					?>
						<div class="row">
							<div class="<?php echo esc_attr( $column_class ); ?>">
								<div class="row">
									<?php
										while ( have_posts() ) : the_post();
											$author_displayname = get_the_author_meta( 'display_name' );
											$author_email = get_the_author_meta( 'email' );
											$author_id = get_the_author_meta( 'ID' );
											$author_designation = get_the_author_meta( 'designation', $author_id );
											$release_date = get_the_date();
											$categories = get_the_category();
											?>
												<div class="col-lg-4 col-md-6 col-sm-12">
													<div <?php post_class( 'blog' ); ?>>
														<?php if ( has_post_thumbnail() ) { ?>
															<a href="<?php echo esc_url( get_permalink() ); ?>" class="blog-category">
																<div class="blog-image">
																	<?php
																		echo get_the_post_thumbnail( get_the_ID(), 'mttheme-medium-image' );
																	?>
																</div>
															</a>
														<?php } ?>
														<div class="blog-detail">
															<?php if ( ! is_wp_error( $categories ) && ! empty( $categories ) ) { ?>
																<div class="blog-categories">
																	<?php
																		foreach( $categories as $category ) {
																			$category_link = get_category_link( $category->term_id );
																			?>
																				<a href="<?php echo esc_url( $category_link ); ?>" >
																					<?php echo esc_html( $category->name ); ?>
																				</a>
																			<?php
																		}
																	?>
																</div>
															<?php }	?>
															<div class="blog-title">
																<a href="<?php echo esc_url( get_permalink() ); ?>" >
																	<?php echo get_the_title(); ?>
																</a>
															</div>
															<div class="blog-author">
																<div class="left">
																	<div class="author-image">
																		<?php echo get_avatar( $author_email, 50 ); ?>
																	</div>
																	<div class="author-content">
																		<div class="author-name">
																			<?php echo esc_html( $author_displayname ); ?>
																		</div>
																		<?php if ( ! empty( $author_designation ) ) { ?>
																			<div class="author-designation">
																				<?php echo esc_html( $author_designation ); ?>
																			</div>
																		<?php } ?>
																	</div>
																</div>
																<div class="right">
																	<div class="release-date">
																		<?php echo esc_html( $release_date ); ?>
																	</div>
																</div>
															</div>
															<div class="read-more-btn">
																<a class="btn btn-link" href="<?php echo esc_url( get_permalink() ); ?>" >
																	<?php esc_html_e( 'Read More', 'mtelements' ); ?>
																</a>
															</div>
														</div>
													</div>
												</div>
											<?php
										endwhile;
									?>
									<?php if ( $wp_query->max_num_pages > 1 ) { ?>
										<div class="col-xl-12">
											<div class="pagination">
												<?php
													$current = ( $wp_query->query_vars['paged'] > 1 ) ? $wp_query->query_vars['paged'] : 1; 
													$big = 999999999; // need an unlikely integer
													echo paginate_links( array(
														'base'		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
														'format'	=> '',
														'add_args'	=> '',
														'current'	=> $current,
														'total'		=> $wp_query->max_num_pages,
														'prev_text'	=> '<span>' . __( 'Prev', 'mttheme' ) . '</span>',
														'next_text'	=> '<span>' . __( 'Next', 'mttheme' ) . '</span>',
														'type'		=> 'plain',
													) );
												?>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<?php if ( $mttheme_home_right_sidebar ) { ?>
								<div class="col-lg-3 col-md-4 sidebar" itemtype="http://schema.org/WPSideBar" itemscope="itemscope" role="complementary">
									<?php dynamic_sidebar( $mttheme_home_right ); ?>
								</div>
							<?php } ?>
						</div>
					<?php
				else :
					if ( is_search() ) {
						?>
							<div class="row">
								<div class="col-lg-12 no-post-found">
									<h3>
										<?php echo esc_html__( 'Please try again with some different keywords', 'mttheme' ); ?>
									</h3>
									<form action="<?php echo esc_url( home_url( '/' ) ) ?>" method="get" class="search-form ">
	                                    <div class="input-group">
	                                        <input type="text" id="search" class="search-input" value="<?php echo get_search_query(); ?>" name="s">
	                                        <div class="input-group-btn">
	                                            <button type="submit" class="btn">
	                                                <i class="icon-search"></i>
	                                            </button>
	                                        </div>
	                                    </div>
	                                </form>
								</div>
							</div>
						<?php
					}
				endif;
				?>
			</div>
		</div>
	</div>
<?php
get_footer();