<?php
/**
 * The template for displaying comments
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */

if ( post_password_required() ) {
	return;
}

$mttheme_comment_title	= get_theme_mod( 'mttheme_comment_title', esc_html__( 'Write a comment', 'mttheme' ) );
$no_padding_class		= is_page() ? ' no-padding-lr' : '';
?>
<div class="d-flex w-100 flex-wrap">
	<div class="container-fluid">
		<div class="comment-box pattern-wave pattern-gray">
			<?php if ( have_comments() ) : ?>
			
				
				<div id="comments" class="container mttheme-comment-box<?php echo esc_attr( $no_padding_class );?>">
					<div class="comment-title">
						<?php echo comments_number(); ?>
					</div>
					<?php the_comments_navigation(); ?>
					<ul class="blog-comment">
						<?php
							wp_list_comments( [
								'style'			=> 'li',
								'short_ping'	=> true,
								'avatar_size'	=> 400,
								'callback'		=> 'mttheme_comment_callback',
							] );
						?>
					</ul>
					<?php the_comments_navigation(); ?>
				</div>		
			<?php endif; // Check for have_comments(). ?>
			<div class="container mttheme-comment-form<?php echo esc_attr( $no_padding_class );?>">
				<div class="row">
					<?php
						$user			= wp_get_current_user();
						$user_identity	= $user->exists() ? $user->display_name : '';
						$args			= [];
						$args			= wp_parse_args( $args );
						if ( ! isset( $args['format'] ) ) {
							$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
						}
						$aria_req	= '';
						$html_req	= '';
						$html5		= 'html5' === $args['format'];
						$fields		= [
							'author'=> '<div class="col-md-6 col-sm-12 col-xs-12"> <div class="form-group"> <input id="author" placeholder="'.esc_attr__( 'Name*', 'mttheme' ).'" class="input-field form-control comment-fields" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . $html_req . ' /></div></div>',
							'email'	=> '<div class="col-md-6 col-sm-12 col-xs-12"><div class="form-group"><input id="email" placeholder="'.esc_attr__( 'Email*', 'mttheme' ).'" class="input-field form-control comment-fields" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '"' . $aria_req . $html_req  . ' /> </div></div>',
							'url'	=> '<div class="col-md-6 col-sm-12 col-xs-12"><div class="form-group"><input id="url" placeholder="'.esc_attr__( 'Website', 'mttheme' ).'" class="input-field form-control comment-fields" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" /> </div></div>',
						];
						$fields 	= apply_filters( 'comment_form_default_fields', $fields );

						// To provide filter for change comment text
						$comment_placeholder_field	= apply_filters( 'comment_placeholder_field', __( 'Enter your comment here..', 'mttheme' ) );
						$cancel_reply_link			= apply_filters( 'cancel_reply_link', esc_html__( 'Cancel Comment', 'mttheme' ) );
						$label_submit 				= apply_filters( 'label_submit', esc_html__( 'Submit Now', 'mttheme' ) );
						
						comment_form( array(
							'fields'				=> $fields,
							'comment_field'			=> '<div class="col-md-12 col-sm-12 col-xs-12"><div class="form-group"><textarea id="comment" placeholder="' . esc_attr( $comment_placeholder_field ) . '" rows="6" class="input-field form-control comment-fields" name="comment" required="required"></textarea> </div></div>',
							'title_reply_before'	=> '<div class="reply-comment-title"><span class="comment-title">',
							'title_reply_after'		=> '</span></div>',
							'class_form'			=> 'comment-form blog-comment-form',
							'title_reply'			=> $mttheme_comment_title,
							'title_reply_to'		=> $mttheme_comment_title . esc_html__( ' to %s', 'mttheme' ),
							'cancel_reply_link'		=> $cancel_reply_link,
							'label_submit'			=> $label_submit,
							'comment_notes_before'	=> '',
							'comment_notes_after'	=> '',
							'class_submit'			=> 'btn btn-primary btn-small mttheme-comment-button',
							'submit_button'			=> '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
							'submit_field'			=> '<div class="col-md-12 col-sm-12 col-xs-12 form-submit">%1$s %2$s</div>',
							'logged_in_as'			=> '<p class="logged-in-as col-md-12">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'mttheme' ), esc_url( admin_url( 'profile.php' ) ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</p>',
						) );
					?>
				</div>
			</div>
		</div>
	</div>
</div>