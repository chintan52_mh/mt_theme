<?php
/**
 * The main template file for archive services
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header();
$mttheme_title_enable = mttheme_option( 'mttheme_title_enable', 'on' );
?>
	<div class="mttheme-main-content-wrap">
		<?php
			if ( $mttheme_title_enable == 'on' ) {
				get_template_part( 'templates/page-title/title' );
			}
		?>
		<div class="container-fluid mt-5 mb-5">
			<div class="mtelements-service-wrapper service-listing-style-1">
				<div class="row">
					<?php
						while ( have_posts() ) :
							the_post();
							?>
								<div class="col-xl-3 col-md-6">
									<div <?php post_class(); ?>>
										<a href="<?php echo esc_url( get_permalink() ); ?>" >
											<?php if ( has_post_thumbnail() ) { ?>
												<div class="service-image">
													 <?php echo get_the_post_thumbnail(); ?>
												</div>
											<?php } ?>
											<div class="service-text">
												<?php 	
													$post_title = get_the_title();
													$title_as_array = explode( ' ', $post_title );
													$last_word = array_pop( $title_as_array );
													$last_word_with_span = '<span>' . $last_word . '</span>';
													array_push( $title_as_array, $last_word_with_span );
													$modified_title = implode( ' ', $title_as_array );
													echo sprintf( '%s', $modified_title );
												?>
											</div>
										</a>
									</div>
								</div>
							<?php
						endwhile;
						?>
						<?php if ( $wp_query->max_num_pages > 1 ) { ?>
							<div class="col-xl-12">
								<div class="pagination">
									<?php
										$current = ( $wp_query->query_vars['paged'] > 1 ) ? $wp_query->query_vars['paged'] : 1; 
										$big = 999999999; // need an unlikely integer
										echo paginate_links( array(
											'base'		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
											'format'	=> '',
											'add_args'	=> '',
											'current'	=> $current,
											'total'		=> $wp_query->max_num_pages,
											'prev_text'	=> __( 'Prev', 'mttheme' ),
											'next_text'	=> __( 'Next', 'mttheme' ),
											'type'		=> 'plain',
										) );
									?>
								</div>
							</div>
						<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php
get_footer();