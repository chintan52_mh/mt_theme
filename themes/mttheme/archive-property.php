<?php
/**
 * The main template file for archive properties
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header();
$mttheme_title_enable = mttheme_option( 'mttheme_title_enable', 'on' );
?>
	<div class="mttheme-main-content-wrap">
		<?php
			if ( $mttheme_title_enable == 'on' ) {
				get_template_part( 'templates/page-title/title' );
			}
		?>
		<div class="mtelements-property-wrapper property-listing-style-1">
			<div class="container-fluid mt-5 mb-5">
				<div class="row">
					<?php
						$property_listing_count = '1';
						while ( have_posts() ) :
							the_post();
							$property_price = get_post_meta( get_the_ID(), '_mttheme_propery_price', true );
							if ( $property_listing_count == '1' || $property_listing_count == '2' || $property_listing_count == '5' ) {
			                	$column_class = 'col-lg-6 col-md-6';
			                } else {
			                	$column_class = 'col-lg-3 col-md-6';
			                }
			                if ( $property_listing_count == '5' ) {
			                	$property_listing_count = '1';
			                }
							?>
								<div class="<?php echo esc_attr( $column_class ); ?>">
									<div <?php post_class(); ?>>
										<?php if ( has_post_thumbnail() ) { ?>
											<a href="<?php echo esc_url( get_permalink() ); ?>" >
												<div class="property-image" style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url() ); ?>);">
												</div>
												<div class="property-detail">
													<div class="property-title">
														<?php echo get_the_title(); ?>
													</div>
													<?php if ( ! empty( $property_price ) ) { ?>
														<div class="property-price">
															<span class="price-symbol text-warning">$</span>
															<?php echo esc_html( $property_price ); ?>
														</div>
													<?php } ?>
												</div>
											</a>
										<?php } ?>
									</div>
								</div>
							<?php
							$property_listing_count++;
						endwhile;
						?>
						<?php if ( $wp_query->max_num_pages > 1 ) { ?>
							<div class="col-xl-12">
								<div class="pagination">
									<?php
										$current = ( $wp_query->query_vars['paged'] > 1 ) ? $wp_query->query_vars['paged'] : 1; 
										$big = 999999999; // need an unlikely integer
										echo paginate_links( array(
											'base'		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
											'format'	=> '',
											'add_args'	=> '',
											'current'	=> $current,
											'total'		=> $wp_query->max_num_pages,
											'prev_text'	=> __( 'Prev', 'mttheme' ),
											'next_text'	=> __( 'Next', 'mttheme' ),
											'type'		=> 'plain',
										) );
									?>
								</div>
							</div>
						<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php
get_footer();