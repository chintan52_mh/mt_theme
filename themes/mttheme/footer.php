<?php
/**
 * The template for displaying the footer
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

		/* Footer */
		get_template_part( 'templates/footer/footer' );
		?>
			</div><!-- End main wrap -->
		<?php wp_footer(); ?>
	</body>
</html>