<?php
/**
* Template Name: Property Listing
*
* @package Mttheme
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header();
global $WP_Query;

if ( get_query_var('paged') ) {
    $paged = get_query_var('paged'); 
} elseif ( get_query_var('page') ) {
    $paged = get_query_var('page'); 
} else {
    $paged = 1;
}

$pagination_per_page = ( ! empty( $_GET['per_page'] ) ) ? $_GET['per_page'] : '10';

$property_query_args = [
	'post_type' 	=> 'property',
	'post_status'   => 'publish',
	'paged'         => $paged,
	'posts_per_page' => $pagination_per_page,
];

// Property availbility
$availbility = ( ! empty( $_GET['availbility'] ) ) ? $_GET['availbility'] : 'available';

if ( isset( $_GET ) ) {
	$meta_query_filter_array = [];
	if ( ! empty( $_GET['location'] ) ) {
		$meta_query_filter_array[] = [
								        'key' => '_mttheme_property_location',
								        'value' => $_GET['location'],
								    ];
	}
	if ( ! empty( $_GET['bedrooms'] ) ) {
		$meta_query_filter_array[] = [
								        'key' => '_mttheme_property_bedrooms',
								        'value' => $_GET['bedrooms'],
								    ];
	}
	if ( ! empty( $_GET['propery-type'] ) ) {
		$property_query_args['tax_query'] = [
												[
													'taxonomy' => 'property-types',
										            'field' => 'slug',
										            'terms' => $_GET['propery-type'],
												]
								    		];
	}
	if ( ! empty( $availbility ) && $availbility != 'all' ) {
		$meta_query_filter_array[] = [
										[
											'key' => '_mttheme_property_availability',
								        	'value' => $availbility,
										]
						    		];
	}
	$property_query_args['meta_query'] = [
										    'relation' => 'AND',
										    $meta_query_filter_array
										];
}
$properties = new WP_Query( $property_query_args );

// Get pagination filter array
$pagination_filter_array = mttheme_get_pagination_filter_array();

// Get availbility filter array
$availbility_filter_array = mttheme_get_property_availbility_array();
$property_view = ( ! empty( $_GET['property-view'] ) ) ? $_GET['property-view'] : 'property-listing-style-1';

// Get view filter array
$property_view_select = mttheme_get_property_view_array();

// Get current page url
$obj_id = get_queried_object_id();
$current_url = get_permalink( $obj_id );

// Container class as per design
$layout_class = ( $property_view == 'property-listing-style-2' ) ? 'container' : 'container-fluid';
?>
	<div class="mttheme-main-content-wrap">

		<form class="property-filter-form" method="get" action="<?php echo esc_url( $current_url ); ?>">
			<?php
				get_template_part( 'templates/page-title/title', 'property' );
			?>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="property-filter-wrap">
							<div class="pagination-filter">
								<label class="filter-label"><?php esc_html_e( 'Properties per page:', 'mttheme' ); ?></label>
								<?php 
									if ( ! empty( $pagination_filter_array ) ) {
										foreach( $pagination_filter_array as $value ) {
											echo '<div class="custom-control custom-radio custom-control-inline">';
												echo '<input type="radio" id="radio-' . esc_attr( $value ) . '" name="per_page" class="custom-control-input" value="' . esc_attr( $value ) . '" ' . checked( $pagination_per_page, $value, false ) . '>';
												echo '<label class="custom-control-label" for="radio-' . esc_attr( $value ) . '">' . esc_html( $value ) . '</label>';
											echo '</div>';
										}
									}
								?>
							</div>
							<div class="availbility-filter">
								<?php 
									if ( ! empty( $availbility_filter_array ) ) {
										foreach( $availbility_filter_array as $key => $value ) {
											echo '<div class="custom-control custom-radio custom-control-inline">';
												echo '<input type="radio" id="radio-' . esc_attr( $key ) . '" name="availbility" class="custom-control-input" value="' . esc_attr( $key ) . '" ' . checked( $availbility, $key, false ) . '>';
												echo '<label class="custom-control-label" for="radio-' . esc_attr( $key ) . '">' . esc_html( $value ) . '</label>';
											echo '</div>';
										}
									}
								?>
							</div>
							<?php if ( ! empty( $property_view_select ) ) { ?>
								<div class="view-filter form-inline">
									<label class="filter-label"><?php esc_html_e( 'Properties view', 'mttheme' ); ?></label>
									<select name="property-view" class="form-control">
										<?php 
										foreach( $property_view_select as $key => $value ) {
											echo  '<option value="' . esc_attr( $key ) . '" ' . selected( $property_view, $key, false ) . '>' . esc_html( $value ) . '</option>';
										}
										?>
									</select>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="mtelements-property-wrapper <?php echo esc_attr( $property_view ); ?>">
			<div class="<?php echo esc_attr( $layout_class ); ?>">
				<div class="row">
					<?php
					if ( $properties->have_posts() ) {
						$property_listing_count = '1';
						switch ( $property_view ) {
							case 'property-listing-style-1':
								while( $properties->have_posts() ) : 
									$properties->the_post();
									$property_price = get_post_meta( get_the_ID(), '_mttheme_propery_price', true );

									if ( $property_listing_count == '1' || $property_listing_count == '2' || $property_listing_count == '5' ) {
					                	$column_class = 'col-lg-6 col-md-6';
					                } else {
					                	$column_class = 'col-lg-3 col-md-6';
					                }
					                if ( $property_listing_count == '5' ) {
					                	$property_listing_count = '0';
					                }
									?>
										<div class="<?php echo esc_attr( $column_class ); ?>">
											<div <?php post_class(); ?>>
												<?php if ( has_post_thumbnail() ) { ?>
													<a href="<?php echo esc_url( get_permalink() ); ?>" >
														<?php
															$property_image = '';
															if ( $property_listing_count == '1' || $property_listing_count == '2' || $property_listing_count == '5' ) {
																$property_image = get_the_post_thumbnail_url( get_the_ID(), 'mttheme-extra-medium-image' );
															} else {
																$property_image = get_the_post_thumbnail_url( get_the_ID(), 'mttheme-medium-image' );
															}
														?>
														<div class="property-image" style="background-image: url(<?php echo esc_url( $property_image ); ?>);">
														</div>
														<div class="property-detail">
															<div class="property-title">
																<?php echo get_the_title(); ?>
															</div>
															<?php if ( ! empty( $property_price ) ) { ?>
																<div class="property-price">
																	<?php echo mttheme_get_price_wrap( $property_price ); ?>
																</div>
															<?php } ?>
														</div>
													</a>
												<?php } ?>
											</div>
										</div>
									<?php
									$property_listing_count++;
								endwhile;
								wp_reset_postdata();
							break;
							
							case 'property-listing-style-2':
								while( $properties->have_posts() ) : 
									$properties->the_post();
									$property_price = get_post_meta( get_the_ID(), '_mttheme_propery_price', true );

									$property_images_ids = get_post_meta( get_the_ID(), '_mttheme_propery_images', true );
									$property_images_array = ! empty( $property_images_ids ) ? explode( ',', $property_images_ids ) : [];
									$property_image_count = 0;
									?>
										<div class="col-lg-12">
											<div <?php post_class(); ?>>
												<?php if ( has_post_thumbnail() ) { ?>
													<div class="property-large-img">
														<?php
															$property_image = get_the_post_thumbnail_url( get_the_ID(), 'mttheme-extra-medium-image' ); 
														?>
														<div class="property-image" style="background-image: url(<?php echo esc_url( $property_image ); ?>);">
														</div>
														<div class="property-detail">
															<div class="property-title">
																<?php echo get_the_title(); ?>
															</div>
															<?php if ( ! empty( $property_price ) ) { ?>
																<div class="property-price">
																	<?php echo mttheme_get_price_wrap( $property_price ); ?>
																</div>
															<?php } ?>
														</div>
													</div>
													<?php if ( $property_images_array ) { ?>
														<div class="property-gallery">
															<?php
																foreach ( $property_images_array as $property_image ) {
																	if ( $property_image_count == 2 ) {
																		break;
																	}
																	if ( $property_image_count == 1 ) {
																		?>
																			<div class="property-gallery-image">
																				<?php echo wp_get_attachment_image( $property_image, 'mttheme-medium-image' ); ?>
																			</div>
																			<div class="property-link">
																				<a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-1 btn-white view-property-btn">
																					<span><?php esc_html_e( 'View all', 'mttheme' ); ?></span>
																				</a>
																			</div>
																		<?php
																	} else {
																		?>
																			<div class="property-gallery-image">
																				<?php echo wp_get_attachment_image( $property_image, 'mttheme-medium-image' ); ?>
																			</div>
																		<?php
																	}
																	$property_image_count++;
																}
															?>
														</div>
													<?php } ?>													
												<?php } ?>
											</div>
										</div>
									<?php
								endwhile;
								wp_reset_postdata();
							break;

							default:
								
								break;
						}
					} else{
						?>
							<div class="col-lg-12">
								<h3 class="text-center">
									<?php echo apply_filters( 'mttheme_property_not_found_text', esc_html__( 'No Property Found...', 'mttheme' ) ); ?>
								</h3>
							</div>
						<?php
					}
					?>
					<?php if ( $properties->max_num_pages > 1 ) { ?>
						<div class="col-xl-12">
							<div class="pagination">
								<?php
									$current = ( $properties->query_vars['paged'] > 1 ) ? $properties->query_vars['paged'] : 1; 
									$big = 999999999; // need an unlikely integer
									echo paginate_links( array(
										'base'		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
										'format'	=> '',
										'add_args'	=> '',
										'current'	=> $current,
										'total'		=> $properties->max_num_pages,
										'prev_text'	=> '<span>' . __( 'Prev', 'mttheme' ) . '</span>',
										'next_text'	=> '<span>' . __( 'Next', 'mttheme' ) . '</span>',
										'type'		=> 'plain',
									) );
								?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php
get_footer();