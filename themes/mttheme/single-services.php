<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header();

/* Start of the loop. */
while ( have_posts() ) : the_post();

	global $post;
	$mttheme_service_subtitle = get_post_meta( $post->ID, '_mttheme_service_subtitle_single', true );
	$mttheme_service_banner_image = get_post_meta( $post->ID, '_mttheme_service_banner_image_single', true );

	/* Post main class */
	$class = 'mttheme-main-content-wrap single-service-main-section';

	$author_url = get_author_posts_url( get_the_author_meta( 'ID' ) );

?>
	<div id="post-<?php echo esc_attr( get_the_ID() ); ?>" <?php post_class( $class ); ?>>
		<div class="mttheme-rich-snippet d-none">
			<span class="entry-title">
				<?php echo esc_html( get_the_title() ); ?>
			</span>
			<span class="author vcard">
				<a class="url fn n" href="<?php echo esc_url( $author_url ); ?>">
					<?php echo esc_html( get_the_author() ); ?>
				</a>
			</span>
			<span class="published">
				<?php echo esc_html( get_the_date() ); ?>
			</span>
			<time class="updated" datetime="<?php echo esc_attr( get_the_modified_date( 'c' ) ); ?>">
				<?php echo esc_html( get_the_modified_date() ); ?>
			</time>
		</div>
		<div class="single-service-image">
			<div class="container-fluid">
				<div class="image-wrapper">
					<?php
						if ( ! empty( $mttheme_service_banner_image ) ) {
							echo '<img src=" ' . esc_url( $mttheme_service_banner_image ) . ' " alt="service-banner-image" />';
						} elseif ( has_post_thumbnail() ) {
							echo get_the_post_thumbnail();
						}
					?>
				</div>
			</div>
		</div>
		<div class="single-service-text">
			<div class="container">
				<div class="heading heading-style-1">
					<?php the_title( '<h2 class="heading-title">', '</h2>' ); ?>
					<?php if ( ! empty( $mttheme_service_subtitle ) ) { ?>
						<h4 class="heading-subtitle">
							<?php echo esc_html( $mttheme_service_subtitle ); ?>
						</h4>				
					<?php } ?>
				</div>
			</div>
			<?php the_content(); ?>
			<?php the_posts_pagination(); ?>
		</div>
	</div>
<?php
endwhile; // End of the loop.
get_footer();