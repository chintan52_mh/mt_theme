<?php
/**
 * The template for displaying all pages
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header(); 

/* Start of the loop. */
while ( have_posts() ) : the_post();
    
    /* Page main class */
    $class = 'mttheme-main-content-wrap';
    $mttheme_header_top_space = mttheme_option( 'mttheme_header_top_space', 'off' );
    $mttheme_title_enable = mttheme_option( 'mttheme_title_enable', 'on' );
    $class .= ( $mttheme_header_top_space == 'on' ) ? ' header-top-space' : '';

	/* Get post class and id */
	$mttheme_page_classes = '';
	ob_start();
		post_class( $class );
		$mttheme_page_classes .= ob_get_contents();
	ob_end_clean();
	
	echo '<div id="post-' . esc_attr( get_the_ID() ) . '" ' . $mttheme_page_classes . '>'; // PHPCS:Ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		if ( $mttheme_title_enable == 'on' ) {
			get_template_part( 'templates/page-title/title' );
		}
		
		get_template_part( 'templates/page/content', 'page' );

	echo '</div>'; // @codingStandardsIgnoreLine
endwhile;  // End of the loop.
get_footer();