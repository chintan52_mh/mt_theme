<?php
/**
 * This file use for define custom function
 * Also include required files.
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 *  Mttheme Theme namespace.
 */
define( 'MTTHEME_THEME_VERSION', '1.0' );
define( 'MTTHEME_ADDONS_VERSION', '1.0' );
define( 'MTTHEME_ELEMENTS_VERSION', '1.0' );

/**
 *  Mttheme Theme Folders
 */
define( 'MTTHEME_THEME_DIR',                    get_template_directory());
define( 'MTTHEME_THEME_ASSETS',                 MTTHEME_THEME_DIR . '/assets' );
define( 'MTTHEME_THEME_JS',                     MTTHEME_THEME_ASSETS . '/js' );
define( 'MTTHEME_THEME_CSS',                    MTTHEME_THEME_ASSETS . '/css' );
define( 'MTTHEME_THEME_IMAGES',                 MTTHEME_THEME_ASSETS . '/images' );
define( 'MTTHEME_THEME_ADMIN_JS',               MTTHEME_THEME_JS . '/admin' );
define( 'MTTHEME_THEME_ADMIN_CSS',              MTTHEME_THEME_CSS . '/admin' );
define( 'MTTHEME_THEME_LIB',                    MTTHEME_THEME_DIR . '/lib' );
define( 'MTTHEME_THEME_CUSTOMIZER',             MTTHEME_THEME_LIB . '/customizer' );
define( 'MTTHEME_THEME_CUSTOMIZER_MAPS',        MTTHEME_THEME_CUSTOMIZER . '/customizer-maps' );
define( 'MTTHEME_THEME_CUSTOMIZER_CONTROLS',    MTTHEME_THEME_CUSTOMIZER . '/customizer-control' );
define( 'MTTHEME_THEME_TGM',                    MTTHEME_THEME_LIB . '/tgm' );

/**
 *  Mttheme Theme Folder URI
 */
define( 'MTTHEME_THEME_URI',                    get_template_directory_uri());
define( 'MTTHEME_THEME_ASSETS_URI',             MTTHEME_THEME_URI     . '/assets' );
define( 'MTTHEME_THEME_JS_URI',                 MTTHEME_THEME_ASSETS_URI . '/js' );
define( 'MTTHEME_THEME_CSS_URI',                MTTHEME_THEME_ASSETS_URI . '/css' );
define( 'MTTHEME_THEME_IMAGES_URI',             MTTHEME_THEME_ASSETS_URI . '/images' );
define( 'MTTHEME_THEME_ADMIN_JS_URI',           MTTHEME_THEME_JS_URI . '/admin' );
define( 'MTTHEME_THEME_ADMIN_CSS_URI',          MTTHEME_THEME_CSS_URI . '/admin' );
define( 'MTTHEME_THEME_LIB_URI',                MTTHEME_THEME_URI . '/lib' );
define( 'MTTHEME_THEME_CUSTOMIZER_URI',         MTTHEME_THEME_LIB_URI . '/customizer' );
define( 'MTTHEME_THEME_CUSTOMIZER_MAPS_URI',    MTTHEME_THEME_CUSTOMIZER_URI . '/customizer-maps' );
define( 'MTTHEME_THEME_TGM_URI',                MTTHEME_THEME_LIB_URI . '/tgm' );


if ( file_exists( MTTHEME_THEME_LIB . '/theme-require-files.php' ) ) {
	require_once( MTTHEME_THEME_LIB . '/theme-require-files.php' );
}