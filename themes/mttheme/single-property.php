<?php
/**
 * The template for displaying all single property
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header();

/* Start of the loop. */
while ( have_posts() ) : the_post();

	$property_images_ids = get_post_meta( get_the_ID(), '_mttheme_propery_images', true );
	$property_images_array = ! empty( $property_images_ids ) ? explode( ',', $property_images_ids ) : [];

	// Get all property details
	$mttheme_property_address = get_post_meta( get_the_ID(), '_mttheme_property_address', true );
	$mttheme_get_property_types = mttheme_get_property_type();
	$mttheme_property_bedrooms = get_post_meta( get_the_ID(), '_mttheme_property_bedrooms', true );
	$mttheme_property_bathrooms = get_post_meta( get_the_ID(), '_mttheme_property_bathrooms', true );
	$mttheme_propery_squarefeet = get_post_meta( get_the_ID(), '_mttheme_propery_squarefeet', true );
	$mttheme_propery_parking_text = get_post_meta( get_the_ID(), '_mttheme_propery_parking_text', true );
	$mttheme_propery_flor_plan = get_post_meta( get_the_ID(), '_mttheme_propery_flor_plan', true );
	$flor_plan_images_array = ! empty( $mttheme_propery_flor_plan ) ? explode( ',', $mttheme_propery_flor_plan ) : [];
	$mttheme_property_built = get_post_meta( get_the_ID(), '_mttheme_property_built', true );
	$mttheme_property_lot = get_post_meta( get_the_ID(), '_mttheme_property_lot', true );
	$mttheme_property_hoa = get_post_meta( get_the_ID(), '_mttheme_property_hoa', true );

	$mttheme_property_contact_shortcode = get_post_meta( get_the_ID(), '_mttheme_property_contact_shortcode', true );
	$mttheme_property_availability = get_post_meta( get_the_ID(), '_mttheme_property_availability', true );
	$mttheme_property_map_latitude = get_post_meta( get_the_ID(), '_mttheme_property_map_latitude', true );
	$mttheme_property_map_longitude = get_post_meta( get_the_ID(), '_mttheme_property_map_longitude', true );
	$mttheme_property_map_link = get_post_meta( get_the_ID(), '_mttheme_property_map_link', true );
	$mttheme_property_map_uniq = uniqid( 'mttheme-property-' );
	$mttheme_propery_price = get_post_meta( get_the_ID(), '_mttheme_propery_price', true );

	$mttheme_propery_neighborhood_age = get_post_meta( get_the_ID(), '_mttheme_propery_neighborhood_age', true );
	$mttheme_propery_neighborhood_married = get_post_meta( get_the_ID(), '_mttheme_propery_neighborhood_married', true );
	$mttheme_propery_neighborhood_college = get_post_meta( get_the_ID(), '_mttheme_propery_neighborhood_college', true );
	$mttheme_propery_neighborhood_build = get_post_meta( get_the_ID(), '_mttheme_propery_neighborhood_build', true );
	$mttheme_propery_neighborhood_ownership = get_post_meta( get_the_ID(), '_mttheme_propery_neighborhood_ownership', true );
	$mttheme_propery_neighborhood_ownership_rent = 100 - intval( get_post_meta( get_the_ID(), '_mttheme_propery_neighborhood_ownership', true ) );

	$mttheme_property_loan_offered = get_post_meta( get_the_ID(), '_mttheme_property_loan_offered', true );

	$custom_tab_data = get_post_meta( get_the_ID(), '_mttheme_tabdata', true );

	// Recommended Properties
	$property_types_terms = wp_get_object_terms( get_the_ID(), 'property-types', array('fields' => 'ids') );
	$recommended_properties_args = array(
        'ignore_sticky_posts'   => 1,
        'post_type'             => 'property',
        'post_status'           => 'publish',
        'posts_per_page'        => 2,
        'tax_query' => array(
	        array(
	            'taxonomy' => 'property-types',
	            'field' => 'id',
	            'terms' => $property_types_terms
	        )
	    ),
        'post__not_in'          => array( get_the_ID() ),
    );

    $recommended_properties = new WP_Query( $recommended_properties_args );

	/* Post main class */
	$class = 'mttheme-main-content-wrap single-property-main-section';

	$author_url = get_author_posts_url( get_the_author_meta( 'ID' ) );
?>
	<div id="post-<?php echo esc_attr( get_the_ID() ); ?>" <?php post_class( $class ); ?>>
		<div class="mttheme-rich-snippet d-none">
			<span class="entry-title">
				<?php echo esc_html( get_the_title() ); ?>
			</span>
			<span class="author vcard">
				<a class="url fn n" href="<?php echo esc_url( $author_url ); ?>">
					<?php echo esc_html( get_the_author() ); ?>
				</a>
			</span>
			<span class="published">
				<?php echo esc_html( get_the_date() ); ?>
			</span>
			<time class="updated" datetime="<?php echo esc_attr( get_the_modified_date( 'c' ) ); ?>">
				<?php echo esc_html( get_the_modified_date() ); ?>
			</time>
		</div>
		<div class="property-image-gallery">
			<div class="container-fluid">
				<?php if ( has_post_thumbnail() ) { ?>
					<div class="main-image">
						<?php echo get_the_post_thumbnail(); ?>
					</div>
				<?php } ?>
				<?php if ( ! empty( $property_images_array ) ) { ?>
					<div class="image-gallery">
						<?php
							$counter = 0;
							$gallery_image_count = count( $property_images_array );
							echo '<div class="image-block">';
							echo '<a class="popup-image-gallery" href="' . esc_url( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ). '">';
								echo get_the_post_thumbnail( get_the_ID(), 'thumbnail' );
							echo '</a>';
							echo '<div class="image-overlay">' . $gallery_image_count . __( ' Images', 'mttheme' ) . '</div>';
							echo '</div>';
							foreach ( $property_images_array as $property_image ) {
								if ( $counter == 3 ) {
									break;
								}
								echo '<a class="popup-image-gallery" href="' . esc_url( wp_get_attachment_image_url( $property_image, 'full' ) ). '">';
									echo wp_get_attachment_image( $property_image, 'thumbnail' );
								echo '</a>';
								$counter++;
							}
						?>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="single-property-text">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="heading heading-style-1">
							<?php if ( ! empty( $mttheme_get_property_types ) ) {
								?>
								<div class="property-type">
									<?php 
										foreach ( $mttheme_get_property_types as $mttheme_get_property_type ) {
											?>
												<span class="badge badge-pill badge-success"><?php echo esc_html( $mttheme_get_property_type ); ?></span>
											<?php
										}
									?>
								</div>
								<?php
							}?>
							<?php the_title( '<h2 class="heading-title">', '</h2>' ); ?>
							<?php if ( ! empty( $mttheme_property_address ) ) { ?>
								<div class="property-address">
									<i class="fas fa-map-marker-alt"></i>
									<?php echo esc_html( $mttheme_property_address ); ?>
								</div>
							<?php } ?>
						</div>
						<div class="property-content">
							<?php the_content(); ?>
						</div>
						<?php if ( ! empty( $mttheme_property_bedrooms ) || ! empty( $mttheme_property_bathrooms ) || ! empty( $mttheme_propery_squarefeet ) || ! empty( $mttheme_propery_parking_text ) ) { ?>
							<div class="property-detail">
								<div class="row">
									<?php if ( ! empty( $mttheme_property_bedrooms ) ) {
										?>
										<div class="col-auto">
											<img src="<?php echo esc_url( MTTHEME_THEME_IMAGES_URI.'/rooms.png' ); ?>" alt="Rooms">
											<p><?php echo esc_html( $mttheme_property_bedrooms ).esc_html__( ' rooms', 'mttheme' ); ?></p>
										</div>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_property_bathrooms ) ) {
										?>
										<div class="col-auto">
											<img src="<?php echo esc_url( MTTHEME_THEME_IMAGES_URI.'/baths.png' ); ?>" alt="baths">
											<p><?php echo esc_html( $mttheme_property_bathrooms ).esc_html__( ' baths', 'mttheme' ); ?></p>
										</div>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_propery_squarefeet ) ) {
										?>
										<div class="col-auto">
											<img src="<?php echo esc_url( MTTHEME_THEME_IMAGES_URI.'/plan.png' ); ?>" alt="plan">
											<p><?php echo esc_html( $mttheme_propery_squarefeet ).esc_html__( ' Sq. Ft', 'mttheme' ); ?></p>
										</div>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_propery_parking_text ) ) {
										?>
										<div class="col-auto">
											<img src="<?php echo esc_url( MTTHEME_THEME_IMAGES_URI.'/cars.png' ); ?>" alt="cars">
											<p><?php echo esc_html( $mttheme_propery_parking_text ); ?></p>
										</div>
										<?php
									}?>
								</div>
							</div>
							<hr />
						<?php } ?>
						<?php if ( ! empty( $mttheme_property_built ) || ! empty( $mttheme_propery_squarefeet ) || ! empty( $mttheme_property_bedrooms ) || ! empty( $mttheme_property_lot ) || ! empty( $mttheme_property_hoa ) || ! empty( $mttheme_property_bathrooms ) ) { ?>
							<div class="property-characteristies">
								<h4 class="block-title">
									<?php esc_html_e( 'Characteristies', 'mttheme' ); ?>
								</h4>
								<ul class="list-group list-group-flush">
									<?php if ( ! empty( $mttheme_property_built ) ) {
										?>
										<li class="list-group-item">
											<span class="label"><?php esc_html_e( 'Year Built', 'mttheme' ); ?></span>
											<span class="value"><?php echo esc_html( $mttheme_property_built ); ?></span>
										</li>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_propery_squarefeet ) ) {
										?>
										<li class="list-group-item">
											<span class="label"><?php esc_html_e( 'Sq. Ft', 'mttheme' ); ?></span>
											<span class="value"><?php echo esc_html( $mttheme_propery_squarefeet ); ?></span>
										</li>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_property_bedrooms ) ) {
										?>
										<li class="list-group-item">
											<span class="label"><?php esc_html_e( 'Bedrooms', 'mttheme' ); ?></span>
											<span class="value"><?php echo esc_html( $mttheme_property_bedrooms ); ?></span>
										</li>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_property_lot ) ) {
										?>
										<li class="list-group-item">
											<span class="label"><?php esc_html_e( 'Lot size', 'mttheme' ); ?></span>
											<span class="value"><?php echo esc_html( $mttheme_property_lot ); ?></span>
										</li>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_property_hoa ) ) {
										?>
										<li class="list-group-item">
											<span class="label"><?php esc_html_e( 'HOA', 'mttheme' ); ?></span>
											<span class="value"><?php echo esc_html( $mttheme_property_hoa ); ?></span>
										</li>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_property_bathrooms ) ) {
										?>
										<li class="list-group-item">
											<span class="label"><?php esc_html_e( 'Bathroom', 'mttheme' ); ?></span>
											<span class="value"><?php echo esc_html( $mttheme_property_bathrooms ); ?></span>
										</li>
										<?php
									}?>
								</ul>
							</div>
							<hr />
						<?php } ?>
						<?php if ( ! empty( $flor_plan_images_array ) ) { ?>
							<div class="property-floorplan">
								<h4 class="block-title">
									<?php esc_html_e( 'Floorplan', 'mttheme' ); ?>
								</h4>
								<div class="floorplan-slider">
									<div class="swiper-container floorplan">
										<div class="swiper-wrapper">
											<?php
												foreach ( $flor_plan_images_array as $flor_plan_image ) {
													echo '<div class="swiper-slide">'. wp_get_attachment_image( $flor_plan_image, 'mttheme-extra-medium-image' ) .'</div>';
												}
											?>
										</div>
										<div class="swiper-button-prev floorplan-prev"></div>
										<div class="swiper-button-next floorplan-next"></div>
	    								<div class="swiper-pagination floorplan-pagination"></div>
									</div>
								</div>
							</div>
							<hr />
						<?php } ?>
						<?php if ( ! empty( $mttheme_propery_neighborhood_age ) || ! empty( $mttheme_propery_neighborhood_married ) || ! empty( $mttheme_propery_neighborhood_college ) || ! empty( $mttheme_propery_neighborhood_build ) || ! empty( $mttheme_propery_neighborhood_ownership ) ) { ?>
							<div class="property-neighborhood">
								<h4 class="block-title">
									<?php esc_html_e( 'Naighborhood', 'mttheme' ); ?>
								</h4>
								<div class="row">
									<?php if ( ! empty( $mttheme_propery_neighborhood_age ) ) {
										?>
										<div class="col-auto">
											<h4 class="title"><?php echo esc_html( $mttheme_propery_neighborhood_age ); ?></h4>
											<p><?php esc_html_e( 'Average Age', 'mttheme' ); ?></p>
										</div>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_propery_neighborhood_married ) ) {
										?>
										<div class="col-auto">
											<h4 class="title"><?php echo esc_html( $mttheme_propery_neighborhood_married ); ?></h4>
											<p><?php esc_html_e( 'Married', 'mttheme' ); ?></p>
										</div>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_propery_neighborhood_college ) ) {
										?>
										<div class="col-auto">
											<h4 class="title"><?php echo esc_html( $mttheme_propery_neighborhood_college ); ?></h4>
											<p><?php esc_html_e( 'College Grads', 'mttheme' ); ?></p>
										</div>
										<?php
									}?>
									<?php if ( ! empty( $mttheme_propery_neighborhood_build ) ) {
										?>
										<div class="col-auto">
											<h4 class="title"><?php echo esc_html( $mttheme_propery_neighborhood_build ); ?></h4>
											<p><?php esc_html_e( 'Build Around', 'mttheme' ); ?></p>
										</div>
									<?php
									}?>
								</div>
								<?php if ( ! empty( $mttheme_propery_neighborhood_ownership ) ) { ?>
									<div class="ownership">
										<div class="row">
											<div class="col-md-12">
												<label><?php esc_html_e( 'Ownership', 'mttheme' ); ?></label>
												<div class="progress">
													<div class="progress-bar bg-success" role="progressbar" style="width: <?php echo esc_attr( $mttheme_propery_neighborhood_ownership ); ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
												</div>
												<div class="progress-info">
													<div class="text-left"><?php echo esc_html( $mttheme_propery_neighborhood_ownership ); ?>%Own</div>
													<div class="text-right"><?php echo esc_html( $mttheme_propery_neighborhood_ownership_rent ); ?>%Rent</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
							<hr />
						<?php } ?>
						<?php if ( ! empty( $mttheme_property_loan_offered ) ) { ?>
							<div class="property-loan">
								<div class="row">
									<div class="col-md-12">
										<h4 class="block-title">
											<?php esc_html_e( 'Loan offered by bank', 'mttheme' ); ?>
										</h4>
										<div class="swiper-container loanoffered">
											<div class="swiper-wrapper">
												<?php
													if ( ! empty( $mttheme_property_loan_offered ) ) {
														$chunks = array_chunk( $mttheme_property_loan_offered, 2 );
														$text = array_column( $chunks, 0 );
														$images  = array_column( $chunks, 1 );
														$counter = 0;
														foreach ( $text as $value ) {
															echo '<div class="swiper-slide">';
																echo '<img src="' . esc_url( $images[$counter] ). '" alt="loan-image"/>';
																echo '<div class="loan-percentage">' . esc_html( $value ) . '</div>';
															echo '</div>';
															$counter++;
														}
													}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr />
						<?php } ?>
						<?php
							if ( ! empty( $custom_tab_data['tabtitle'] ) ) {
								$counter = 0;
								foreach ( $custom_tab_data['tabtitle'] as $title ) {
									$content = $custom_tab_data['tabdescription'][$counter];
									?>
										<div class="property-custom">
											<h4 class="block-title">
												<?php echo esc_html( $title ); ?>
											</h4>
								            <div class="content-wrappper">
								                <?php echo sprintf( '%s', $content ); ?>
								            </div>
								        </div>
								        <hr />
									<?php
									$counter++;
								}
							}
						?>
					</div>
					<div class="col-lg-4 pl-xl-5">
						<div class="property-sidebar">
							<?php if ( ! empty( $mttheme_property_availability ) || ! empty( $mttheme_propery_price ) || ! empty( $mttheme_property_contact_shortcode ) ) { ?>
								<div class="sidebar-block">
									<?php if ( ! empty( $mttheme_property_availability ) || ! empty( $mttheme_propery_price ) ) { ?>
										<div class="block-inner availibility-block">
											<?php if ( ! empty( $mttheme_property_availability ) ) {
												$badge_class = ( $mttheme_property_availability == 'sold-out' ) ? 'danger' : 'success';
												$mttheme_property_availability = str_replace( '-', ' ', $mttheme_property_availability );
											?>
												<div class="property-availability">
													<span class="badge badge-<?php echo esc_attr( $badge_class ); ?>"><?php echo esc_html( ucwords( $mttheme_property_availability ) ); ?></span>
												</div>
											<?php } ?>
											<?php if ( ! empty( $mttheme_propery_price ) ) { ?>
												<div class="property-price">
													<?php echo mttheme_get_price_wrap( $mttheme_propery_price ); ?>
													<p class="offer"><?php esc_html_e( 'Equity offered', 'mttheme' ); ?></p>
												</div>
											<?php } ?>
										</div>
									<?php } ?>
									<?php if ( ! empty( $mttheme_property_contact_shortcode ) ) { ?>
										<div class="block-inner">
											<div class="property-contact-wrapper">
												<?php echo do_shortcode( $mttheme_property_contact_shortcode ); ?>
											</div>
										</div>
									<?php } ?>
								</div>
							<?php } ?>
							<?php if ( ! empty( $mttheme_property_map_latitude ) && ! empty( $mttheme_property_map_longitude ) ) { ?>
								<div class="sidebar-block map-block">
									<div class="block-title	">
										<?php esc_html_e( 'Location', 'mttheme' ); ?>
										<?php if ( ! empty( $mttheme_property_map_link ) ) { ?>
											<a href="<?php echo esc_url( $mttheme_property_map_link ); ?>" class="map-link float-right" target="_blank">
												<i class="fas fa-external-link-alt"></i>
											</a>
										<?php } ?>
									</div>
									<div class="block-inner">
										<div class="property-map" id="<?php echo esc_attr( $mttheme_property_map_uniq ); ?>" data-id="<?php echo esc_attr( $mttheme_property_map_uniq ); ?>" style="height: 300px;" data-lat="<?php echo esc_attr( $mttheme_property_map_latitude ); ?>" data-long="<?php echo esc_attr( $mttheme_property_map_longitude ); ?>">
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php if ( $recommended_properties->have_posts() ) { ?>
				<div class="recommanded-properties">
					<div class="container">
						<div class="recommanded-title">
							<div class="row">
								<div class="col-xl-9 col-lg-8">
									<div class="heading heading-style-1">
										<h2 class="heading-title"><?php esc_html_e( 'Recommended Properties', 'mttheme' ); ?></h2>
									</div>
								</div>
								<div class="col-xl-3 col-lg-4">
									<a href="#" class="btn btn-1"><span><?php esc_html_e( 'View all', 'mttheme' ); ?></span></a>
								</div>
							</div>
						</div>
					</div>
					<div class="container-fluid">
						<div class="row">
							<?php 
								while( $recommended_properties->have_posts() ) :
									$recommended_properties->the_post();
									$property_price = get_post_meta( get_the_ID(), '_mttheme_propery_price', true );
									?>
										<div class="col-lg-6">
											<div <?php post_class(); ?>>
													<a href="<?php echo esc_url( get_permalink() ); ?>" >
														<?php if ( has_post_thumbnail() ) { 

															$property_image = get_the_post_thumbnail_url( get_the_ID(), 'mttheme-extra-medium-image' ); 
														?>
															<div class="property-image" style="background-image: url(<?php echo esc_url( $property_image ); ?>);">
															</div>
														<?php } ?>
														<div class="property-detail">
															<div class="property-title">
																<?php echo get_the_title(); ?>
															</div>
															<?php if ( ! empty( $property_price ) ) { ?>
																<div class="property-price">
																	<?php echo mttheme_get_price_wrap( $property_price ); ?>
																</div>
															<?php } ?>
														</div>
													</a>
											</div>
										</div>
									<?php
								endwhile;
								wp_reset_postdata();
							?>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php the_posts_pagination(); ?>
		</div>
	</div>
<?php
endwhile; // End of the loop.
get_footer();