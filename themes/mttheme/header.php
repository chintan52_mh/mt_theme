<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "header" tag.
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage" class="no-js">
	<head>
		<!-- keywords -->
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<!-- viewport -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
		<!-- profile -->
		<link rel="profile" href="//gmpg.org/xfn/11">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php
			if ( function_exists( 'wp_body_open' ) ) {
				wp_body_open();
			} else {
				do_action( 'wp_body_open' );
			}
		?>
		<div class="site-main-wrapper">
			<?php get_template_part( 'templates/header/header' ); // Header ?>