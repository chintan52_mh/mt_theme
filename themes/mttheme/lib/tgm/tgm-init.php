<?php
/**
 * TGM Init Class
 *
 * @package Mttheme
 */
?>
<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Return if use is not logged in
if ( ! is_admin() ) { return; }

define( 'MTTHEME_PLUGINS_URI',                    'http://project-demo-server.com/wp_theme/versions/' );
define( 'MTTHEME_PLUGINS_CURRENT_USER_URI',       MTTHEME_PLUGINS_URI . MTTHEME_THEME_VERSION );

require_once MTTHEME_THEME_TGM . '/class-tgm-plugin-activation.php';

if ( ! function_exists( 'mttheme_register_required_plugins' ) ) :
    function mttheme_register_required_plugins() {

        $plugin_list = array(
            array(
                'name'               => esc_html__( 'MtElements', 'mttheme' ),       // The plugin name
                'slug'               => 'mtelements',                              // The plugin slug (typically the folder name)
                'source'             =>  MTTHEME_PLUGINS_CURRENT_USER_URI . '/mtelements.zip', // The plugin source
                'required'           => true,                                          // If false, the plugin is only 'recommended' instead of required
                'version'            => MTTHEME_ELEMENTS_VERSION,                        // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,                                         // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,                                         // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',                                            // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'Mttheme Addons', 'mttheme' ),       // The plugin name
                'slug'               => 'mttheme-addons',                              // The plugin slug (typically the folder name)
                'source'             =>  MTTHEME_PLUGINS_CURRENT_USER_URI . '/mttheme-addons.zip', // The plugin source
                'required'           => true,                                          // If false, the plugin is only 'recommended' instead of required
                'version'            => MTTHEME_ADDONS_VERSION,                        // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
                'force_activation'   => false,                                         // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
                'force_deactivation' => false,                                         // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
                'external_url'       => '',                                            // If set, overrides default API URL and points to an external URL
            ),
            array(
                'name'               => esc_html__( 'Elementor', 'mttheme' ),                     // The plugin name.
                'slug'               => 'elementor',                          // The plugin slug (typically the folder name).
                'required'           => true,                                         // If false, the plugin is only 'recommended' instead of required.
            ),
            array(
                'name'               => esc_html__( 'Contact Form 7', 'mttheme' ),                              // The plugin name.
                'slug'               => 'contact-form-7',                              // The plugin slug (typically the folder name).
                'required'           => false,                                         // If false, the plugin is only 'recommended' instead of required.
            ),
        );

        $mainconfig = array(
                'id'           => 'mttheme_tgmpa',                // Unique ID for hashing notices for multiple instances of TGMPA.
                'default_path' => '',                           // Default absolute path to bundled plugins.
                'menu'         => 'mttheme-theme-setup',          // Menu slug.
                'step'         => '2',
                'parent_slug'  => 'themes.php',                 // Parent menu slug.
                'capability'   => 'edit_theme_options',         // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
                'has_notices'  => true,                         // Show admin notices or not.
                'dismissable'  => true,                         // If false, a user cannot dismiss the nag message.
                'dismiss_msg'  => '',                           // If 'dismissable' is false, this message will be output at top of nag.
                'is_automatic' => false,                        // Automatically activate plugins after installation or not.
                'message'      => '',                           // Message to output right before the plugins table.
        );

        tgmpa( $plugin_list, $mainconfig );

    }
endif;
add_action( 'tgmpa_register', 'mttheme_register_required_plugins' );