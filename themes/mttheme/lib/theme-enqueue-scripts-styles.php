<?php
/**
 * Theme Register Style Js.
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Register custom fonts.
 */
function mttheme_fonts_url() {
	$fonts_url = '';

	$font_families = array();

	$font_families[] = 'Karla:400,400i,600,600i,800,800i|Playfair Display:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i';

	$query_args = array(
		'family'  => urlencode( implode( '|', $font_families ) ),
		'subset'  => urlencode( 'latin,latin-ext' ),
		'display' => urlencode( 'swap' ),
	);

	$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

	return esc_url_raw( $fonts_url );
}

/*
 * Enqueue scripts and styles.
 */
if( ! function_exists( 'mttheme_register_style_js' ) ) {
	function mttheme_register_style_js() {

		/*
		 * Load Mttheme main and other required jquery files.
		 */

		$mttheme_google_map_api_key = get_theme_mod( 'mttheme_google_map_api_key', '' ); 
		if ( $mttheme_google_map_api_key ) {
			$map_query_args = array(
				'key'  	  => urlencode( $mttheme_google_map_api_key ),
			);
			$google_map_url = add_query_arg( $map_query_args, 'https://maps.googleapis.com/maps/api/js' );
			wp_enqueue_script( 'mttheme-google-map', $google_map_url, array(), null, true );

			if ( is_singular( 'property' ) ) {
				wp_enqueue_script( 'mttheme-property-map', MTTHEME_THEME_JS_URI.'/property-map.js', array( 'mttheme-google-map' ), MTTHEME_THEME_VERSION, true );
			}
		}

		wp_register_script( 'bootstrap', MTTHEME_THEME_JS_URI.'/bootstrap.min.js', array( 'jquery' ), '4.5.0', true);
		wp_enqueue_script( 'bootstrap' );
		
		wp_register_script( 'magnific-popup', MTTHEME_THEME_JS_URI.'/magnific-popup.min.js', array( 'jquery' ), '1.1.0', true);
		wp_enqueue_script( 'magnific-popup' );
		
		wp_register_script( 'mttheme-custom', MTTHEME_THEME_JS_URI.'/custom.js', array( 'jquery' ), MTTHEME_THEME_VERSION, true );
		wp_enqueue_script( 'mttheme-custom' );
		wp_localize_script( 'mttheme-custom', 'mtthemecustom', array(
				'mapIconImage' => apply_filters( 'mttheme_property_map_pin_image', MTTHEME_THEME_IMAGES_URI.'/svg/map-marker-alt-solid.svg' ),
				'mapZoom'  => apply_filters( 'mttheme_property_map_zoom', '15' ),
			)
		);

		/*
		 * Load Mttheme main and other required CSS files.
		 */
		
		wp_register_style( 'font-awesome', MTTHEME_THEME_CSS_URI . '/font-awesome.min.css', null, '5.13.0' );
		wp_enqueue_style( 'font-awesome' );
		
		wp_register_style( 'maginic-popup', MTTHEME_THEME_CSS_URI . '/maginic-popup.css', null, '1.1.0' );
		wp_enqueue_style( 'maginic-popup' );

		wp_enqueue_style( 'mttheme-fonts', mttheme_fonts_url(), array(), null );

		wp_register_style( 'custom', MTTHEME_THEME_CSS_URI . '/custom.css', null, MTTHEME_THEME_VERSION );
		wp_enqueue_style( 'custom' );
		
		wp_register_style( 'mttheme-style', get_stylesheet_uri(), null, MTTHEME_THEME_VERSION );
		wp_enqueue_style( 'mttheme-style' );

		// Load for wordpress comments
		if ( is_singular() && ( comments_open() || get_comments_number() ) && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'mttheme_register_style_js', -1 );

/*
 * Enqueue scripts and styles after elementor.
 */
if( ! function_exists( 'mttheme_register_style_js_after_elementor' ) ) {
	function mttheme_register_style_js_after_elementor() {
		wp_enqueue_script( 'swiper' );
		wp_enqueue_style( 'elementor-frontend' );
	}
}
add_action( 'wp_enqueue_scripts', 'mttheme_register_style_js_after_elementor', 10 );

/* Enqueue Custom css base on customizer settings */
if( ! function_exists( 'mttheme_enqueue_custom_style' ) ) {
	function mttheme_enqueue_custom_style() {

		$output_css = '';
		ob_start();
		/* Include navigation css */
		require_once MTTHEME_THEME_CUSTOMIZER . '/customizer-output/custom-css.php';
		$output_css = ob_get_contents();
		ob_end_clean();

		// apply_filters for custom css so user can add its own custom css
		$output_css = apply_filters( 'mttheme_inline_custom_css', $output_css );

		// 1. Remove comments.
		// 2. Remove whitespace.
		// 3. Remove starting whitespace.
		$output_css = preg_replace( '#/\*.*?\*/#s', '', $output_css );
		$output_css = preg_replace( '/\s*([{}|:;,])\s+/', '$1', $output_css );
		$output_css = preg_replace( '/\s\s+(.*)/', '$1', $output_css );

		// apply_filters for custom css after minified so user can add its own custom css
		$output_css = apply_filters( 'mttheme_inline_custom_css_after_minified', $output_css );

		wp_add_inline_style( 'mttheme-style', $output_css );
	}
}
add_action( 'wp_enqueue_scripts', 'mttheme_enqueue_custom_style', 999 );

/*
 * Load mttheme customizer script.
 */
if( ! function_exists( 'mttheme_customizer_scripts_preview' ) ) {
	function mttheme_customizer_scripts_preview() {

		wp_enqueue_script( 'mttheme-customizer', MTTHEME_THEME_ADMIN_JS_URI.'/theme-customizer.js', array( 'jquery','customize-preview' ) );
	}
}
add_action( 'customize_preview_init','mttheme_customizer_scripts_preview' );

/*
 * Load theme admin css and script.
 */
if( ! function_exists( 'mttheme_admin_custom_scripts' ) ) {
	function mttheme_admin_custom_scripts() {
		
		// load media script
		wp_enqueue_media();

		wp_enqueue_style( 'wp-color-picker');
		wp_enqueue_script( 'wp-color-picker');

		wp_register_script( 'mttheme-admin-custom', MTTHEME_THEME_ADMIN_JS_URI . '/theme-admin-custom.js', array( 'jquery' ), MTTHEME_THEME_VERSION );
		wp_enqueue_script( 'mttheme-admin-custom' );

		wp_register_style( 'mttheme-admin-custom', MTTHEME_THEME_ADMIN_CSS_URI . '/theme-admin-custom.css', null, MTTHEME_THEME_VERSION );
		wp_enqueue_style( 'mttheme-admin-custom' );

	}
}
add_action( 'admin_enqueue_scripts', 'mttheme_admin_custom_scripts' );
