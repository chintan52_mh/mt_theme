<?php
/**
 * Theme Require Files
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

if( ! class_exists( 'Mttheme_Require_Files_Class' ) ) {
    class Mttheme_Require_Files_Class {
        /*
         * Includes all (require_once) php file(s) inside selected folder using class.
         */
        public function __construct( $fileName ) { 
            if( is_array( $fileName ) ) {
                foreach( $fileName as $name ) {
                    require get_parent_theme_file_path( "/lib/{$name}.php" );
                }
            } else {
                throw new Exception( __( 'File is not found...', 'mttheme' ) );
            }
        }
    }
}

/*
 *  Includes all required files for Mttheme Theme.
 */
new Mttheme_Require_Files_Class(
    array(
        'theme-register-sidebars',
        'theme-extra-functions',
        'theme-property-functions',
        'theme-enqueue-scripts-styles',
        'tgm/tgm-init',
        'kirki/kirki',
        'navigation-menu/standard-menu'
    )
);