<?php
/**
 * Theme extra function file
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Get property type array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_propery_types_array' ) ) {
    function mttheme_get_propery_types_array() {
        $terms = get_terms( array(
            'taxonomy' => 'property-types',
            'hide_empty' => false,
        ) );
        return $terms;
    }
}

/**
 * Get bedrooms array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_bedrooms_array' ) ) {
    function mttheme_get_bedrooms_array() {
        
        $bedrooms_options = [
                                ''  => __( 'Select', 'mttheme' ),
                                '1' => '1', 
                                '2' => '2',
                                '3' => '3',
                                '4' => '4',
                                '5' => '5',
                            ];
        return apply_filters( 'mttheme_bedrooms_options', $bedrooms_options );
    }
}

/**
 * Get pagination array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_pagination_filter_array' ) ) {
    function mttheme_get_pagination_filter_array() {
        
        $pagination_filter = [ '10', '15', '20' ];
        return apply_filters( 'mttheme_pagination_filter_options', $pagination_filter );
    }
}

/**
 * Get property availablity array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_property_availbility_array' ) ) {
    function mttheme_get_property_availbility_array() {
        
        $property_view_select = [ 
                                'available' => __( 'Available only', 'mttheme' ),
                                'all'       => __( 'All properties', 'mttheme' ),
                            ];
        return apply_filters( 'mttheme_property_availbility_options', $property_view_select );
    }
}

/**
 * Get property view array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_property_view_array' ) ) {
    function mttheme_get_property_view_array() {
        
        $property_view_select = [ 
                                'property-listing-style-1' => __( 'Grid view', 'mttheme' ),
                                'property-listing-style-2' => __( 'List view', 'mttheme' ),
                            ];
        return apply_filters( 'mttheme_property_view_options', $property_view_select );
    }
}

/**
 * Get property types array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_property_type' ) ) {
    function mttheme_get_property_type() {
        
        $mttheme_property_types = get_the_terms( get_the_ID(), 'property-types' );
        $mttheme_property_types_array = ! empty( $mttheme_property_types ) ? wp_list_pluck( $mttheme_property_types, 'name' ) : [];
        return $mttheme_property_types_array;
    }
}

/**
 * Trim trailing zeros off prices.
 *
 * @param string|float|int $price Price.
 * @return string
 */
if ( ! function_exists( 'mttheme_trim_zeros' ) ) {
    function mttheme_trim_zeros( $price ) {
        $mttheme_property_decimal_separator = get_theme_mod( 'mttheme_property_decimal_separator', '.' );
        return preg_replace( '/' . preg_quote( $mttheme_property_decimal_separator, '/' ) . '0++$/', '', $price );
    }
}

/**
 * Get full list of currency codes.
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_currencies' ) ) {
    function mttheme_get_currencies() {
        static $currencies;

        if ( ! isset( $currencies ) ) {
            $currencies = array_unique(
                apply_filters(
                    'mttheme_currencies',
                    array(
                        'AED' => __( 'United Arab Emirates dirham د.إ د.إ', 'mttheme' ),
                        'AFN' => __( 'Afghan afghani ؋', 'mttheme' ),
                        'ALL' => __( 'Albanian lek L', 'mttheme' ),
                        'AMD' => __( 'Armenian dram AMD', 'mttheme' ),
                        'ANG' => __( 'Netherlands Antillean guilder ƒ', 'mttheme' ),
                        'AOA' => __( 'Angolan kwanza Kz', 'mttheme' ),
                        'ARS' => __( 'Argentine peso $', 'mttheme' ),
                        'AUD' => __( 'Australian dollar $', 'mttheme' ),
                        'AWG' => __( 'Aruban florin Afl.', 'mttheme' ),
                        'AZN' => __( 'Azerbaijani manat AZN', 'mttheme' ),
                        'BAM' => __( 'Bosnia and Herzegovina convertible mark KM', 'mttheme' ),
                        'BBD' => __( 'Barbadian dollar $', 'mttheme' ),
                        'BDT' => __( 'Bangladeshi taka ৳ ', 'mttheme' ),
                        'BGN' => __( 'Bulgarian lev лв.', 'mttheme' ),
                        'BHD' => __( 'Bahraini dinar .د.ب', 'mttheme' ),
                        'BIF' => __( 'Burundian franc Fr', 'mttheme' ),
                        'BMD' => __( 'Bermudian dollar $', 'mttheme' ),
                        'BND' => __( 'Brunei dollar $', 'mttheme' ),
                        'BOB' => __( 'Bolivian boliviano Bs.', 'mttheme' ),
                        'BRL' => __( 'Brazilian real R$', 'mttheme' ),
                        'BSD' => __( 'Bahamian dollar $', 'mttheme' ),
                        'BTC' => __( 'Bitcoin ฿', 'mttheme' ),
                        'BTN' => __( 'Bhutanese ngultrum Nu.', 'mttheme' ),
                        'BWP' => __( 'Botswana pula P', 'mttheme' ),
                        'BYR' => __( 'Belarusian ruble (old) Br', 'mttheme' ),
                        'BYN' => __( 'Belarusian ruble Br', 'mttheme' ),
                        'BZD' => __( 'Belize dollar $', 'mttheme' ),
                        'CAD' => __( 'Canadian dollar $', 'mttheme' ),
                        'CDF' => __( 'Congolese franc Fr', 'mttheme' ),
                        'CHF' => __( 'Swiss franc CHF', 'mttheme' ),
                        'CLP' => __( 'Chilean peso $', 'mttheme' ),
                        'CNY' => __( 'Chinese yuan ¥', 'mttheme' ),
                        'COP' => __( 'Colombian peso $', 'mttheme' ),
                        'CRC' => __( 'Costa Rican colón ₡', 'mttheme' ),
                        'CUC' => __( 'Cuban convertible peso $', 'mttheme' ),
                        'CUP' => __( 'Cuban peso $', 'mttheme' ),
                        'CVE' => __( 'Cape Verdean escudo $', 'mttheme' ),
                        'CZK' => __( 'Czech koruna Kč', 'mttheme' ),
                        'DJF' => __( 'Djiboutian franc Fr', 'mttheme' ),
                        'DKK' => __( 'Danish krone DKK', 'mttheme' ),
                        'DOP' => __( 'Dominican peso RD$', 'mttheme' ),
                        'DZD' => __( 'Algerian dinar د.ج', 'mttheme' ),
                        'EGP' => __( 'Egyptian pound EGP', 'mttheme' ),
                        'ERN' => __( 'Eritrean nakfa Nfk', 'mttheme' ),
                        'ETB' => __( 'Ethiopian birr Br', 'mttheme' ),
                        'EUR' => __( 'Euro €', 'mttheme' ),
                        'FJD' => __( 'Fijian dollar $', 'mttheme' ),
                        'FKP' => __( 'Falkland Islands pound £', 'mttheme' ),
                        'GBP' => __( 'Pound sterling £', 'mttheme' ),
                        'GEL' => __( 'Georgian lari ₾', 'mttheme' ),
                        'GGP' => __( 'Guernsey pound £', 'mttheme' ),
                        'GHS' => __( 'Ghana cedi ₵', 'mttheme' ),
                        'GIP' => __( 'Gibraltar pound £', 'mttheme' ),
                        'GMD' => __( 'Gambian dalasi D', 'mttheme' ),
                        'GNF' => __( 'Guinean franc Fr', 'mttheme' ),
                        'GTQ' => __( 'Guatemalan quetzal Q', 'mttheme' ),
                        'GYD' => __( 'Guyanese dollar $', 'mttheme' ),
                        'HKD' => __( 'Hong Kong dollar $', 'mttheme' ),
                        'HNL' => __( 'Honduran lempira L', 'mttheme' ),
                        'HRK' => __( 'Croatian kuna kn', 'mttheme' ),
                        'HTG' => __( 'Haitian gourde G', 'mttheme' ),
                        'HUF' => __( 'Hungarian forint Ft', 'mttheme' ),
                        'IDR' => __( 'Indonesian rupiah Rp', 'mttheme' ),
                        'ILS' => __( 'Israeli new shekel ₪', 'mttheme' ),
                        'IMP' => __( 'Manx pound £', 'mttheme' ),
                        'INR' => __( 'Indian rupee ₹', 'mttheme' ),
                        'IQD' => __( 'Iraqi dinar ع.د', 'mttheme' ),
                        'IRR' => __( 'Iranian rial ﷼', 'mttheme' ),
                        'IRT' => __( 'Iranian toman تومان', 'mttheme' ),
                        'ISK' => __( 'Icelandic króna kr.', 'mttheme' ),
                        'JEP' => __( 'Jersey pound £', 'mttheme' ),
                        'JMD' => __( 'Jamaican dollar $', 'mttheme' ),
                        'JOD' => __( 'Jordanian dinar د.ا', 'mttheme' ),
                        'JPY' => __( 'Japanese yen ¥', 'mttheme' ),
                        'KES' => __( 'Kenyan shilling KSh', 'mttheme' ),
                        'KGS' => __( 'Kyrgyzstani som сом', 'mttheme' ),
                        'KHR' => __( 'Cambodian riel ៛', 'mttheme' ),
                        'KMF' => __( 'Comorian franc Fr', 'mttheme' ),
                        'KPW' => __( 'North Korean won ₩', 'mttheme' ),
                        'KRW' => __( 'South Korean won ₩', 'mttheme' ),
                        'KWD' => __( 'Kuwaiti dinar د.ك', 'mttheme' ),
                        'KYD' => __( 'Cayman Islands dollar $', 'mttheme' ),
                        'KZT' => __( 'Kazakhstani tenge ₸', 'mttheme' ),
                        'LAK' => __( 'Lao kip ₭', 'mttheme' ),
                        'LBP' => __( 'Lebanese pound ل.ل', 'mttheme' ),
                        'LKR' => __( 'Sri Lankan rupee රු', 'mttheme' ),
                        'LRD' => __( 'Liberian dollar $', 'mttheme' ),
                        'LSL' => __( 'Lesotho loti L', 'mttheme' ),
                        'LYD' => __( 'Libyan dinar ل.د', 'mttheme' ),
                        'MAD' => __( 'Moroccan dirham د.م.', 'mttheme' ),
                        'MDL' => __( 'Moldovan leu MDL', 'mttheme' ),
                        'MGA' => __( 'Malagasy ariary Ar', 'mttheme' ),
                        'MKD' => __( 'Macedonian denar ден', 'mttheme' ),
                        'MMK' => __( 'Burmese kyat Ks', 'mttheme' ),
                        'MNT' => __( 'Mongolian tögrög ₮', 'mttheme' ),
                        'MOP' => __( 'Macanese pataca P', 'mttheme' ),
                        'MRU' => __( 'Mauritanian ouguiya UM', 'mttheme' ),
                        'MUR' => __( 'Mauritian rupee ₨', 'mttheme' ),
                        'MVR' => __( 'Maldivian rufiyaa .ރ', 'mttheme' ),
                        'MWK' => __( 'Malawian kwacha MK', 'mttheme' ),
                        'MXN' => __( 'Mexican peso $', 'mttheme' ),
                        'MYR' => __( 'Malaysian ringgit RM', 'mttheme' ),
                        'MZN' => __( 'Mozambican metical MT', 'mttheme' ),
                        'NAD' => __( 'Namibian dollar N$', 'mttheme' ),
                        'NGN' => __( 'Nigerian naira ₦', 'mttheme' ),
                        'NIO' => __( 'Nicaraguan córdoba C$', 'mttheme' ),
                        'NOK' => __( 'Norwegian krone kr', 'mttheme' ),
                        'NPR' => __( 'Nepalese rupee ₨', 'mttheme' ),
                        'NZD' => __( 'New Zealand dollar $', 'mttheme' ),
                        'OMR' => __( 'Omani rial ر.ع.', 'mttheme' ),
                        'PAB' => __( 'Panamanian balboa B/.', 'mttheme' ),
                        'PEN' => __( 'Sol S/', 'mttheme' ),
                        'PGK' => __( 'Papua New Guinean kina K', 'mttheme' ),
                        'PHP' => __( 'Philippine peso ₱', 'mttheme' ),
                        'PKR' => __( 'Pakistani rupee ₨', 'mttheme' ),
                        'PLN' => __( 'Polish złoty zł', 'mttheme' ),
                        'PRB' => __( 'Transnistrian ruble р.', 'mttheme' ),
                        'PYG' => __( 'Paraguayan guaraní ₲', 'mttheme' ),
                        'QAR' => __( 'Qatari riyal ر.ق', 'mttheme' ),
                        'RON' => __( 'Romanian leu lei', 'mttheme' ),
                        'RSD' => __( 'Serbian dinar рсд', 'mttheme' ),
                        'RUB' => __( 'Russian ruble ₽', 'mttheme' ),
                        'RWF' => __( 'Rwandan franc Fr', 'mttheme' ),
                        'SAR' => __( 'Saudi riyal ر.س', 'mttheme' ),
                        'SBD' => __( 'Solomon Islands dollar $', 'mttheme' ),
                        'SCR' => __( 'Seychellois rupee ₨', 'mttheme' ),
                        'SDG' => __( 'Sudanese pound ج.س.', 'mttheme' ),
                        'SEK' => __( 'Swedish krona kr', 'mttheme' ),
                        'SGD' => __( 'Singapore dollar $', 'mttheme' ),
                        'SHP' => __( 'Saint Helena pound £', 'mttheme' ),
                        'SLL' => __( 'Sierra Leonean leone Le', 'mttheme' ),
                        'SOS' => __( 'Somali shilling Sh', 'mttheme' ),
                        'SRD' => __( 'Surinamese dollar $', 'mttheme' ),
                        'SSP' => __( 'South Sudanese pound £', 'mttheme' ),
                        'STN' => __( 'São Tomé and Príncipe dobra Db', 'mttheme' ),
                        'SYP' => __( 'Syrian pound ل.س', 'mttheme' ),
                        'SZL' => __( 'Swazi lilangeni L', 'mttheme' ),
                        'THB' => __( 'Thai baht ฿', 'mttheme' ),
                        'TJS' => __( 'Tajikistani somoni ЅМ', 'mttheme' ),
                        'TMT' => __( 'Turkmenistan manat m', 'mttheme' ),
                        'TND' => __( 'Tunisian dinar د.ت', 'mttheme' ),
                        'TOP' => __( 'Tongan paʻanga T$', 'mttheme' ),
                        'TRY' => __( 'Turkish lira ₺', 'mttheme' ),
                        'TTD' => __( 'Trinidad and Tobago dollar $', 'mttheme' ),
                        'TWD' => __( 'New Taiwan dollar NT$', 'mttheme' ),
                        'TZS' => __( 'Tanzanian shilling Sh', 'mttheme' ),
                        'UAH' => __( 'Ukrainian hryvnia ₴', 'mttheme' ),
                        'UGX' => __( 'Ugandan shilling UGX', 'mttheme' ),
                        'USD' => __( 'United States (US) dollar $', 'mttheme' ),
                        'UYU' => __( 'Uruguayan peso $', 'mttheme' ),
                        'UZS' => __( 'Uzbekistani som UZS', 'mttheme' ),
                        'VEF' => __( 'Venezuelan bolívar Bs F', 'mttheme' ),
                        'VES' => __( 'Bolívar soberano Bs.S', 'mttheme' ),
                        'VND' => __( 'Vietnamese đồng ₫', 'mttheme' ),
                        'VUV' => __( 'Vanuatu vatu Vt', 'mttheme' ),
                        'WST' => __( 'Samoan tālā T', 'mttheme' ),
                        'XAF' => __( 'Central African CFA franc CFA', 'mttheme' ),
                        'XCD' => __( 'East Caribbean dollar $', 'mttheme' ),
                        'XOF' => __( 'West African CFA franc CFA', 'mttheme' ),
                        'XPF' => __( 'CFP franc Fr', 'mttheme' ),
                        'YER' => __( 'Yemeni rial ﷼', 'mttheme' ),
                        'ZAR' => __( 'South African rand R', 'mttheme' ),
                        'ZMW' => __( 'Zambian kwacha ZK', 'mttheme' ),
                    )
                )
            );
        }

        return $currencies;
    }
}


/**
 * Get all available Currency symbols.
 *
 * @return string
 */
if ( ! function_exists( 'mttheme_get_currency_symbol' ) ) {
    function mttheme_get_currency_symbol( $country_code ) {

        $symbols = apply_filters(
            'mttheme_currency_symbols',
            array(
                'AED' => '&#x62f;.&#x625;',
                'AFN' => '&#x60b;',
                'ALL' => 'L',
                'AMD' => 'AMD',
                'ANG' => '&fnof;',
                'AOA' => 'Kz',
                'ARS' => '&#36;',
                'AUD' => '&#36;',
                'AWG' => 'Afl.',
                'AZN' => 'AZN',
                'BAM' => 'KM',
                'BBD' => '&#36;',
                'BDT' => '&#2547;&nbsp;',
                'BGN' => '&#1083;&#1074;.',
                'BHD' => '.&#x62f;.&#x628;',
                'BIF' => 'Fr',
                'BMD' => '&#36;',
                'BND' => '&#36;',
                'BOB' => 'Bs.',
                'BRL' => '&#82;&#36;',
                'BSD' => '&#36;',
                'BTC' => '&#3647;',
                'BTN' => 'Nu.',
                'BWP' => 'P',
                'BYR' => 'Br',
                'BYN' => 'Br',
                'BZD' => '&#36;',
                'CAD' => '&#36;',
                'CDF' => 'Fr',
                'CHF' => '&#67;&#72;&#70;',
                'CLP' => '&#36;',
                'CNY' => '&yen;',
                'COP' => '&#36;',
                'CRC' => '&#x20a1;',
                'CUC' => '&#36;',
                'CUP' => '&#36;',
                'CVE' => '&#36;',
                'CZK' => '&#75;&#269;',
                'DJF' => 'Fr',
                'DKK' => 'DKK',
                'DOP' => 'RD&#36;',
                'DZD' => '&#x62f;.&#x62c;',
                'EGP' => 'EGP',
                'ERN' => 'Nfk',
                'ETB' => 'Br',
                'EUR' => '&euro;',
                'FJD' => '&#36;',
                'FKP' => '&pound;',
                'GBP' => '&pound;',
                'GEL' => '&#x20be;',
                'GGP' => '&pound;',
                'GHS' => '&#x20b5;',
                'GIP' => '&pound;',
                'GMD' => 'D',
                'GNF' => 'Fr',
                'GTQ' => 'Q',
                'GYD' => '&#36;',
                'HKD' => '&#36;',
                'HNL' => 'L',
                'HRK' => 'kn',
                'HTG' => 'G',
                'HUF' => '&#70;&#116;',
                'IDR' => 'Rp',
                'ILS' => '&#8362;',
                'IMP' => '&pound;',
                'INR' => '&#8377;',
                'IQD' => '&#x639;.&#x62f;',
                'IRR' => '&#xfdfc;',
                'IRT' => '&#x062A;&#x0648;&#x0645;&#x0627;&#x0646;',
                'ISK' => 'kr.',
                'JEP' => '&pound;',
                'JMD' => '&#36;',
                'JOD' => '&#x62f;.&#x627;',
                'JPY' => '&yen;',
                'KES' => 'KSh',
                'KGS' => '&#x441;&#x43e;&#x43c;',
                'KHR' => '&#x17db;',
                'KMF' => 'Fr',
                'KPW' => '&#x20a9;',
                'KRW' => '&#8361;',
                'KWD' => '&#x62f;.&#x643;',
                'KYD' => '&#36;',
                'KZT' => '&#8376;',
                'LAK' => '&#8365;',
                'LBP' => '&#x644;.&#x644;',
                'LKR' => '&#xdbb;&#xdd4;',
                'LRD' => '&#36;',
                'LSL' => 'L',
                'LYD' => '&#x644;.&#x62f;',
                'MAD' => '&#x62f;.&#x645;.',
                'MDL' => 'MDL',
                'MGA' => 'Ar',
                'MKD' => '&#x434;&#x435;&#x43d;',
                'MMK' => 'Ks',
                'MNT' => '&#x20ae;',
                'MOP' => 'P',
                'MRU' => 'UM',
                'MUR' => '&#x20a8;',
                'MVR' => '.&#x783;',
                'MWK' => 'MK',
                'MXN' => '&#36;',
                'MYR' => '&#82;&#77;',
                'MZN' => 'MT',
                'NAD' => 'N&#36;',
                'NGN' => '&#8358;',
                'NIO' => 'C&#36;',
                'NOK' => '&#107;&#114;',
                'NPR' => '&#8360;',
                'NZD' => '&#36;',
                'OMR' => '&#x631;.&#x639;.',
                'PAB' => 'B/.',
                'PEN' => 'S/',
                'PGK' => 'K',
                'PHP' => '&#8369;',
                'PKR' => '&#8360;',
                'PLN' => '&#122;&#322;',
                'PRB' => '&#x440;.',
                'PYG' => '&#8370;',
                'QAR' => '&#x631;.&#x642;',
                'RMB' => '&yen;',
                'RON' => 'lei',
                'RSD' => '&#1088;&#1089;&#1076;',
                'RUB' => '&#8381;',
                'RWF' => 'Fr',
                'SAR' => '&#x631;.&#x633;',
                'SBD' => '&#36;',
                'SCR' => '&#x20a8;',
                'SDG' => '&#x62c;.&#x633;.',
                'SEK' => '&#107;&#114;',
                'SGD' => '&#36;',
                'SHP' => '&pound;',
                'SLL' => 'Le',
                'SOS' => 'Sh',
                'SRD' => '&#36;',
                'SSP' => '&pound;',
                'STN' => 'Db',
                'SYP' => '&#x644;.&#x633;',
                'SZL' => 'L',
                'THB' => '&#3647;',
                'TJS' => '&#x405;&#x41c;',
                'TMT' => 'm',
                'TND' => '&#x62f;.&#x62a;',
                'TOP' => 'T&#36;',
                'TRY' => '&#8378;',
                'TTD' => '&#36;',
                'TWD' => '&#78;&#84;&#36;',
                'TZS' => 'Sh',
                'UAH' => '&#8372;',
                'UGX' => 'UGX',
                'USD' => '&#36;',
                'UYU' => '&#36;',
                'UZS' => 'UZS',
                'VEF' => 'Bs F',
                'VES' => 'Bs.S',
                'VND' => '&#8363;',
                'VUV' => 'Vt',
                'WST' => 'T',
                'XAF' => 'CFA',
                'XCD' => '&#36;',
                'XOF' => 'CFA',
                'XPF' => 'Fr',
                'YER' => '&#xfdfc;',
                'ZAR' => '&#82;',
                'ZMW' => 'ZK',
            )
        );

        $symobol = $symbols[$country_code];

        return $symobol;
    }
}

/**
 * Get current selected currency.
 *
 * @return string
 */
if ( ! function_exists( 'mttheme_get_property_currency_symbol' ) ) {
    function mttheme_get_property_currency_symbol() {

        $mttheme_property_currency = get_theme_mod( 'mttheme_property_currency', 'USD' );
        $symobol = mttheme_get_currency_symbol( $mttheme_property_currency );

        return $symobol;
    }
}

/**
 * Get price wrap.
 *
 * @return string
 */
if ( ! function_exists( 'mttheme_get_price_wrap' ) ) {
    function mttheme_get_price_wrap( $price ) {

        $mttheme_property_currency = get_theme_mod( 'mttheme_property_currency', 'USD' );
        $mttheme_property_thousand_separator = get_theme_mod( 'mttheme_property_thousand_separator', ',' );
        $mttheme_property_decimal_separator = get_theme_mod( 'mttheme_property_decimal_separator', '.' );
        $mttheme_property_number_of_desimals = get_theme_mod( 'mttheme_property_number_of_desimals', '2' );

        $price = number_format( $price, $mttheme_property_number_of_desimals, $mttheme_property_decimal_separator, $mttheme_property_thousand_separator );
        if ( $mttheme_property_number_of_desimals > 0 ) {
            $price = mttheme_trim_zeros( $price );
        }
        $symobol = mttheme_get_currency_symbol( $mttheme_property_currency );

        $mttheme_property_currency_position = get_theme_mod( 'mttheme_property_currency_position', 'left_space' );

        $format = '%1$s&nbsp;%2$s';
        switch ( $mttheme_property_currency_position ) {
            case 'left':
                $format = '%1$s%2$s';
                break;
            case 'right':
                $format = '%2$s%1$s';
                break;
            case 'left_space':
                $format = '%1$s&nbsp;%2$s';
                break;
            case 'right_space':
                $format = '%2$s&nbsp;%1$s';
                break;
        }

        $formatted_price = sprintf( $format, '<span class="price-symbol text-warning">' . $symobol. '</span>', $price );

        return $formatted_price;
    }
}