<?php
/**
 * Register Mttheme theme required widget area.
 *
 * @package Mttheme
 */


// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! function_exists( 'mttheme_widgets_init' ) ) {
    function mttheme_widgets_init() {

        register_sidebar( array(
            'name'          => __( 'Main Sidebar', 'mttheme' ),
            'id'            => 'sidebar-1',
            'description'   => __( 'Place widgets to show in your sidebar', 'mttheme' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title"><span>',
            'after_title'   => '</span></div>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Header Sidebar', 'mttheme' ),
            'id'            => 'header-sidebar',
            'description'   => __( 'Place widgets to show in your header right side', 'mttheme' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title">',
            'after_title'   => '</div>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Main Footer 1', 'mttheme' ),
            'id'            => 'footer-middle-column-1',
            'description'   => __( 'Place widgets to show in your footer middle column 1 sidebar', 'mttheme' ),
            'before_widget' => '<div class="widget %2$s" id="%1$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title">',
            'after_title'   => '</div>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Main Footer 2', 'mttheme' ),
            'id'            => 'footer-middle-column-2',
            'description'   => __( 'Place widgets to show in your footer middle column 2 sidebar', 'mttheme' ),
            'before_widget' => '<div class="widget %2$s" id="%1$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title">',
            'after_title'   => '</div>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Main Footer 3', 'mttheme' ),
            'id'            => 'footer-middle-column-3',
            'description'   => __( 'Place widgets to show in your footer middle column 3 sidebar', 'mttheme' ),
            'before_widget' => '<div class="widget %2$s" id="%1$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title">',
            'after_title'   => '</div>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Main Footer 4', 'mttheme' ),
            'id'            => 'footer-middle-column-4',
            'description'   => __( 'Place widgets to show in your footer middle column 4 sidebar', 'mttheme' ),
            'before_widget' => '<div class="widget %2$s" id="%1$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title">',
            'after_title'   => '</div>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Bottom Footer 1', 'mttheme' ),
            'id'            => 'footer-bottom-left',
            'description'   => __( 'Place widgets to show in your footer bottom 1 sidebar', 'mttheme' ),
            'before_widget' => '<div class="widget %2$s" id="%1$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title">',
            'after_title'   => '</div>',
        ) );

        register_sidebar( array(
            'name'          => __( 'Bottom Footer 2', 'mttheme' ),
            'id'            => 'footer-bottom-right',
            'description'   => __( 'Place widgets to show in your footer bottom 2 sidebar', 'mttheme' ),
            'before_widget' => '<div class="widget %2$s" id="%1$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="widget-title">',
            'after_title'   => '</div>',
        ) );
    }
}
add_action( 'widgets_init', 'mttheme_widgets_init' );