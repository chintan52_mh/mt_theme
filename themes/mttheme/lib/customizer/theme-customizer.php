<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

if( ! class_exists('Mttheme_Customizer') ) {

   	/* Main Customizer Class */
  	class Mttheme_Customizer {

	    /* Construct */
	    public function __construct() {

			add_action( 'customize_register', [ $this, 'mttheme_add_customizer_sections' ], 10 );
			add_action( 'customize_controls_print_scripts', [ $this, 'mttheme_add_customize_scripts' ] );
			$this->customizer_required_files();
	    }

	    /**
		 * Remove customizer sections
		 */
	    public function mttheme_add_customizer_sections( $wp_customize ) {

	    	// Remove the section for colors.
	        $wp_customize->remove_section( 'colors' );
	        $wp_customize->remove_control( 'display_header_text' );

	    	// Change site icon section.
        	$wp_customize->get_control( 'site_icon' )->section = 'mttheme_add_logo_favicon_panel';

	    }

	    /**
		 * Add customizer settings files
		 */
	    public static function customizer_required_files() {
	    	$fileName = array(
					        'general-panels',
					        'general-theme-options/page-not-found-settings',
					        'general-theme-options/custom-sidebar',
					        'general-theme-options/property-settings',
					        'general-theme-options/google-map-integration',
					        'header-settings/header-settings',
					        'header-settings/logo-favicon',
					        'title-settings/title-settings',
					        'footer-settings/footer-settings',
					        'layout-content-settings/layout-content-settings',
					        'typography-settings/headings-settings',
					        'typography-settings/body-settings',
					    );
            if( is_array( $fileName ) && class_exists( 'kirki' ) ) {
                foreach( $fileName as $name ) {
                    require get_parent_theme_file_path( "/lib/customizer/customizer-maps/{$name}.php" );
                }
            } else {
                throw new Exception( __( 'Customizer Settings File is not found...', 'mttheme' ) );
            }
        }

	    /**
		 * Scripts to improve our form.
		 */
		public function mttheme_add_customize_scripts() {
			?>
				<script type="text/javascript">
					jQuery( document ).ready( function( $ ) {
			
						wp.customize.section( 'mttheme_add_404_page_panel', function( section ) {
							section.expanded.bind( function( isExpanded ) {
								if ( isExpanded ) {
									wp.customize.previewer.previewUrl.set( '<?php echo esc_js( get_site_url( null, '/404-page' ) ); ?>' );
								}
							} );
						} );

					} );
				</script>
			<?php
		}

	} // end of class

	$Mttheme_Customizer = new Mttheme_Customizer();

} // end of class_exists