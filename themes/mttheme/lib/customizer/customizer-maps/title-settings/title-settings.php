<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

// header section
Kirki::add_section( 'mttheme_title_section', array(
    'title' => __( 'Page Title Settings', 'mttheme' ),
    'panel' => 'mttheme_title_settings_panel',
) );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_title_divider',
        'section'     => 'mttheme_title_section',
        'default'     => '<h3 class="divider-section">' . __( 'Page Title Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'switch',
        'settings'    => 'mttheme_title_enable',
        'label'       => __( 'Enable Title', 'mttheme' ),
        'section'     => 'mttheme_title_section',
        'default'     => 'on',
        'choices'     => [
                            'on'  => esc_html__( 'Yes', 'mttheme' ),
                            'off' => esc_html__( 'No', 'mttheme' ),
                        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_title_color_divider',
        'section'     => 'mttheme_title_section',
        'default'     => '<h3 class="divider-section">' . __( 'Title Color Settings', 'mttheme' ) . '</h3>',
        'active_callback' => function() {
                                $mttheme_title_enable = get_theme_mod( 'mttheme_title_enable', 'on' );
                                if ( $mttheme_title_enable == 'on' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_title_bg_color',
        'label'       => __( 'Background', 'mttheme' ),
        'section'     => 'mttheme_title_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_title_enable = get_theme_mod( 'mttheme_title_enable', 'on' );
                                if ( $mttheme_title_enable == 'on' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_title_color',
        'label'       => __( 'Title', 'mttheme' ),
        'section'     => 'mttheme_title_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_title_enable = get_theme_mod( 'mttheme_title_enable', 'on' );
                                if ( $mttheme_title_enable == 'on' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_left_border_color',
        'label'       => __( 'Title Left Border', 'mttheme' ),
        'section'     => 'mttheme_title_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_title_enable = get_theme_mod( 'mttheme_title_enable', 'on' );
                                if ( $mttheme_title_enable == 'on' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_subtitle_color',
        'label'       => __( 'Subtitle', 'mttheme' ),
        'section'     => 'mttheme_title_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_title_enable = get_theme_mod( 'mttheme_title_enable', 'on' );
                                if ( $mttheme_title_enable == 'on' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );