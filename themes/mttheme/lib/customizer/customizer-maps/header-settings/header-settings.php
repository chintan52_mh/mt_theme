<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

// header section
Kirki::add_section( 'mttheme_header_section', array(
    'title' => __( 'General Header Settings', 'mttheme' ),
    'panel' => 'mttheme_header_settings_panel',
) );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_header_divider',
        'section'     => 'mttheme_header_section',
        'default'     => '<h3 class="divider-section">' . __( 'Header Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'radio-image',
        'settings'    => 'mttheme_header_style',
        'label'       => __( 'Style', 'mttheme' ),
        'section'     => 'mttheme_header_section',
        'default'     => 'header_type_1',
        'choices'     => [
            'header_type_1'  => get_template_directory_uri() . '/assets/images/header/header_type_1.png',
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_header_menu',
        'label'       => __( 'Main Menu', 'mttheme' ),
        'section'     => 'mttheme_header_section',
        'default'     => '',
        'placeholder' => esc_attr__( 'Select header menu', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_get_nav_menus_array(),
    ] );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_header',
        'label'       => __( 'Right Sidebar', 'mttheme' ),
        'section'     => 'mttheme_header_section',
        'default'     => 'header-sidebar',
        'placeholder' => esc_attr__( 'Select header right sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'radio-buttonset',
        'settings'    => 'mttheme_header_width',
        'label'       => __( 'Width', 'mttheme' ),
        'section'     => 'mttheme_header_section',
        'default'     => 'container-fluid',
        'choices'     => [
                            'container-fluid' => __( 'Full Width', 'mttheme' ),
                            'container'       => __( 'Boxed', 'mttheme' )
                        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_header_sticky_type',
        'label'       => __( 'Sticky Type', 'mttheme' ),
        'section'     => 'mttheme_header_section',
        'default'     => 'up-scroll-sticky',
        'choices'     => [  
                            ''                      => esc_html__( 'Non sticky', 'mttheme' ),
                            'down-scroll-sticky'    => esc_html__( 'Down scroll sticky', 'mttheme' ),
                            'up-scroll-sticky'      => esc_html__( 'Up scroll sticky', 'mttheme' ),
                        ],
    ] );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'switch',
        'settings'    => 'mttheme_header_top_space',
        'label'       => __( 'Top Space', 'mttheme' ),
        'section'     => 'mttheme_header_section',
        'default'     => 'Off',
        'choices'     => [
                            'on'  => esc_html__( 'Yes', 'mttheme' ),
                            'off' => esc_html__( 'No', 'mttheme' ),
                        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_header_color_divider',
        'section'     => 'mttheme_header_section',
        'default'     => '<h3 class="divider-section">' . __( 'Header Color Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_header_bg_color',
        'label'       => __( 'Background', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_header_color',
        'label'       => __( 'Text', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_header_hover_color',
        'label'       => __( 'Text Hover', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_header_icon_color',
        'label'       => __( 'Icon', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_sticky_header_settings', [
        'type'        => 'custom',
        'settings'    => 'mttheme_sticky_header_color_divider',
        'section'     => 'mttheme_header_section',
        'default'     => '<h3 class="divider-section">' . __( 'Sticky Header Color Settings', 'mttheme' ) . '</h3>',
        'active_callback' => function() {
                                $mttheme_header_sticky_type = get_theme_mod( 'mttheme_header_sticky_type', '' );
                                if ( $mttheme_header_sticky_type == 'down-scroll-sticky' || $mttheme_header_sticky_type == 'up-scroll-sticky' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_sticky_header_settings', [
        'type'        => 'color',
        'settings'    => 'mttheme_sticky_header_bg_color',
        'label'       => __( 'Background', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_header_sticky_type = get_theme_mod( 'mttheme_header_sticky_type', '' );
                                if ( $mttheme_header_sticky_type == 'down-scroll-sticky' || $mttheme_header_sticky_type == 'up-scroll-sticky' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_sticky_header_settings', [
        'type'        => 'color',
        'settings'    => 'mttheme_sticky_header_color',
        'label'       => __( 'Text', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_header_sticky_type = get_theme_mod( 'mttheme_header_sticky_type', '' );
                                if ( $mttheme_header_sticky_type == 'down-scroll-sticky' || $mttheme_header_sticky_type == 'up-scroll-sticky' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_sticky_header_settings', [
        'type'        => 'color',
        'settings'    => 'mttheme_sticky_header_hover_color',
        'label'       => __( 'Text Hover', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_header_sticky_type = get_theme_mod( 'mttheme_header_sticky_type', '' );
                                if ( $mttheme_header_sticky_type == 'down-scroll-sticky' || $mttheme_header_sticky_type == 'up-scroll-sticky' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_sticky_header_settings', [
        'type'        => 'color',
        'settings'    => 'mttheme_sticky_header_icon_color',
        'label'       => __( 'Icon', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_header_sticky_type = get_theme_mod( 'mttheme_header_sticky_type', '' );
                                if ( $mttheme_header_sticky_type == 'down-scroll-sticky' || $mttheme_header_sticky_type == 'up-scroll-sticky' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_mobile_header_settings', [
        'type'        => 'custom',
        'settings'    => 'mttheme_mobile_header_color_divider',
        'section'     => 'mttheme_header_section',
        'default'     => '<h3 class="divider-section">' . __( 'Mobile Header Color Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_mobile_menu_bg_color',
        'label'       => __( 'Menu Background', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_mobile_toggle_color',
        'label'       => __( 'Toggle Color', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_sticky_mobile_toggle_color',
        'label'       => __( 'Sticky Toggle Color', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
        'active_callback' => function() {
                                $mttheme_header_sticky_type = get_theme_mod( 'mttheme_header_sticky_type', '' );
                                if ( $mttheme_header_sticky_type == 'down-scroll-sticky' || $mttheme_header_sticky_type == 'up-scroll-sticky' ) {
                                    return true;
                                }
                                return false;
                            },
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_mobile_menu_separator_color',
        'label'       => __( 'Menu Separator', 'mttheme' ),
        'section'     => 'mttheme_header_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );