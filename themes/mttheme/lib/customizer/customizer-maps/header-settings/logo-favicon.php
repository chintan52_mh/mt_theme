<?php
	
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }


Kirki::add_section( 'mttheme_add_logo_favicon_panel', array(
    'title'          => __( 'Logo and Favicon', 'mttheme' ),
    'panel'          => 'mttheme_header_settings_panel',
) );

	Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_general_header_logo_divider',
        'section'     => 'mttheme_add_logo_favicon_panel',
        'default'     => '<h3 class="divider-section">' . __( 'Logo', 'mttheme' ) . '</h3>',
    ] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'image',
		'settings'    => 'mttheme_logo',
		'label'       => __( 'Logo', 'mttheme' ),
		'description' => __( 'Upload the logo image which will be displayed in the website header.', 'mttheme' ),
		'section'     => 'mttheme_add_logo_favicon_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'image',
		'settings'    => 'mttheme_logo_light',
		'label'       => __( 'Sticky logo', 'mttheme' ),
		'description' => __( 'Upload the logo image which will be displayed in the scrolled / sticky header version.', 'mttheme' ),
		'section'     => 'mttheme_add_logo_favicon_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'image',
		'settings'    => 'mttheme_logo_ratina',
		'label'       => __( 'Retina logo', 'mttheme' ),
		'description' => __( 'Upload the double size logo image which will be displayed in the website header of retina devices.', 'mttheme' ),
		'section'     => 'mttheme_add_logo_favicon_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'image',
		'settings'    => 'mttheme_logo_light_ratina',
		'label'       => __( 'Sticky retina logo', 'mttheme' ),
		'description' => __( 'Upload the logo image which will be displayed in the scrolled / sticky header version of retina devices.', 'mttheme' ),
		'section'     => 'mttheme_add_logo_favicon_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_favicon_divider',
        'section'     => 'mttheme_add_logo_favicon_panel',
        'default'     => '<h3 class="divider-section">' . __( 'Site / Favicon icon', 'mttheme' ) . '</h3>',
        'description' => __( 'This icon will be used as a browser favicon and app icon for your website. Icon must be square, and at least 512 pixels wide and tall.', 'mttheme' ),
    ] );