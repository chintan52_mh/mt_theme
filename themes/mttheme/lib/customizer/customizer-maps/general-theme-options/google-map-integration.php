<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

Kirki::add_section( 'mttheme_google_map_section', array(
	'title'          => __( 'Google Map Integration', 'mttheme' ),
	'panel'          => 'mttheme_add_general_panel',
) );
	
	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'custom',
		'settings'    => 'mttheme_google_map_divider',
		'default'     => '<h3 class="divider-section">' . __( 'Google Map Settings', 'mttheme' ) . '</h3>',
		'section'     => 'mttheme_google_map_section',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'text',
		'settings'    => 'mttheme_google_map_api_key',
		'label'       => __( 'Google Map Api', 'mttheme' ),
		'section'     => 'mttheme_google_map_section',
		'description' => sprintf(
						esc_html__( 'You can create own API key  %1$s.', 'mtelements' ),
						'<a target="_blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key">' . esc_html__( 'here', 'mtelements' ) . '</a>'
					)
	] );