<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

Kirki::add_section( 'mttheme_add_404_page_panel', array(
	'title'          => __( '404 Page', 'mttheme' ),
	'panel'          => 'mttheme_add_general_panel',
) );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'custom',
		'settings'    => 'mttheme_page_not_found_divider',
		'default'     => '<h3 class="divider-section">' .  __( '404 Page', 'mttheme' ) . '</h3>',
		'section'     => 'mttheme_add_404_page_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'image',
		'settings'    => 'mttheme_page_not_found_side_image',
		'label'       => __( '404 Image ', 'mttheme' ),
		'section'     => 'mttheme_add_404_page_panel',
	] );	

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'text',
		'settings'    => 'mttheme_page_not_found_main_title',
		'label'       => __( 'Main Title', 'mttheme' ),
		'default' 	  => sprintf( '%s || %s', __( 'Error 404', 'mttheme' ), __( 'Not Found', 'mttheme' ) ),
		'section'     => 'mttheme_add_404_page_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'text',
		'settings'    => 'mttheme_page_not_found_title',
		'label'       => __( 'Title', 'mttheme' ),
		'default' 	  => __( 'Lorem ipsum is simply dummy text of the printing and make a type specimen book.', 'mttheme' ),
		'section'     => 'mttheme_add_404_page_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'text',
		'settings'    => 'mttheme_search_placeholder_text',
		'label'       => __( 'Search Placeholder Text', 'mttheme' ),
		'default' 	  => __( 'Search', 'mttheme' ),
		'section'     => 'mttheme_add_404_page_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'text',
		'settings'    => 'mttheme_page_not_found_back_to_home_text',
		'label'       => __( 'Back to Home Text', 'mttheme' ),
		'default' 	  => sprintf( '%s <a href="' . home_url( '/' ) . '">%s</a> %s', __( 'You can go back to the', 'mttheme' ), __( 'Home page', 'mttheme' ), __( 'or use Search', 'mttheme' ) ),
		'section'     => 'mttheme_add_404_page_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_404_page_divider',
        'section'     => 'mttheme_add_404_page_panel',
        'default'     => '<h3 class="divider-section">' . __( '404 Page Color Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_header_settings', [
        'type'        => 'color',
        'settings'    => 'mttheme_404_background_color',
        'transport'   => 'postMessage',
        'label'       => __( 'Background', 'mttheme' ),
        'section'     => 'mttheme_add_404_page_panel',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

	Kirki::add_field( 'mttheme_header_settings', [
        'type'        => 'color',
        'settings'    => 'mttheme_404_main_title_color',
        'transport'   => 'postMessage',
        'label'       => __( 'Main Title', 'mttheme' ),
        'section'     => 'mttheme_add_404_page_panel',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_header_settings', [
        'type'        => 'color',
        'settings'    => 'mttheme_404_title_color',
        'transport'   => 'postMessage',
        'label'       => __( 'Title', 'mttheme' ),
        'section'     => 'mttheme_add_404_page_panel',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_header_settings', [
        'type'        => 'color',
        'settings'    => 'mttheme_404_back_to_home_color',
        'transport'   => 'postMessage',
        'label'       => __( 'Back to Home Text', 'mttheme' ),
        'section'     => 'mttheme_add_404_page_panel',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );