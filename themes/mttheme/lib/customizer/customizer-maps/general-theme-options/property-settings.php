<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

Kirki::add_section( 'mttheme_add_property_panel', array(
	'title'          => __( 'Property Settings', 'mttheme' ),
	'panel'          => 'mttheme_add_general_panel',
) );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'custom',
		'settings'    => 'mttheme_property_settings_divider',
		'default'     => '<h3 class="divider-section">' .  __( 'Property Currency Options', 'mttheme' ) . '</h3>',
		'section'     => 'mttheme_add_property_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_property_currency',
        'label'       => __( 'Currency', 'mttheme' ),
        'section'     => 'mttheme_add_property_panel',
        'default'     => 'USD',
        'placeholder' => esc_html__( 'Select currency', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_get_currencies(),
    ] );

	Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_property_currency_position',
        'label'       => __( 'Currency Position', 'mttheme' ),
        'section'     => 'mttheme_add_property_panel',
        'default'     => 'left_space',
        'placeholder' => esc_html__( 'Select currency position', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => [
        					'left' 			=> __( 'Left', 'mttheme' ),
        					'right' 		=> __( 'Right', 'mttheme' ),
        					'left_space' 	=> __( 'Left with space', 'mttheme' ),
        					'right_space' 	=> __( 'Right with space', 'mttheme' ),
        				],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'text',
		'settings'    => 'mttheme_property_thousand_separator',
		'label'       => __( 'Thousand Separator', 'mttheme' ),
		'default' 	  => ',',
		'section'     => 'mttheme_add_property_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'text',
		'settings'    => 'mttheme_property_decimal_separator',
		'label'       => __( 'Decimal Separator', 'mttheme' ),
		'default' 	  => '.',
		'section'     => 'mttheme_add_property_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'text',
		'settings'    => 'mttheme_property_number_of_desimals',
		'label'       => __( 'Number of Decimals', 'mttheme' ),
		'default' 	  => '2',
		'section'     => 'mttheme_add_property_panel',
	] );
