<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

Kirki::add_section( 'mttheme_add_sidebar_panel', array(
	'title'          => __( 'Custom Sidebar Areas', 'mttheme' ),
	'panel'          => 'mttheme_add_general_panel',
) );
	
	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'custom',
		'settings'    => 'mttheme_sidebar_divider',
		'default'     => '<h3 class="divider-section">' . __( 'Custom Sidebars', 'mttheme' ) . '</h3>',
		'section'     => 'mttheme_add_sidebar_panel',
	] );

	Kirki::add_field( 'mttheme_config_id', [
		'type'        => 'repeater',
		'label'       => __( 'Custom Register Sidebar Areas', 'mttheme' ),
		'section'     => 'mttheme_add_sidebar_panel',
		'row_label' => [
			'type'  => 'text',
			'value' => __( 'Custom Sidebar', 'mttheme' ),
		],
		'description' => __( 'You can add widgets in these sidebars areas at Appearance > Widgets and these sidebars can be used in header, footer, pages and posts.', 'mttheme' ),
		'button_label' => __('Add New Sidebar', 'mttheme' ),
		'settings'     => 'mttheme_custom_sidebar',
		'fields' => [
			'sidebar_name' => [
				'type'        => 'text',
				'label'       => __( 'Sidebar Name', 'mttheme' ),
				'default'     => '',
			],
		]
	] );