<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }
	
Kirki::add_section( 'mttheme_footer_section', array(
    'title'          => __( 'General Footer Settings', 'mttheme' ),
    'panel'          => 'mttheme_footer_settings_panel',
) );
	
	Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_footer_general_divider',
        'section'     => 'mttheme_footer_section',
        'default'     => '<h3 class="divider-section">' . __( 'Main Footer Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_footer_column_1',
        'label'       => __( 'Footer Sidebar 1', 'mttheme' ),
        'section'     => 'mttheme_footer_section',
        'default'     => 'footer-middle-column-1',
        'placeholder' => esc_html__( 'Select sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_footer_column_2',
        'label'       => __( 'Footer Sidebar 2', 'mttheme' ),
        'section'     => 'mttheme_footer_section',
        'default'     => 'footer-middle-column-2',
        'placeholder' => esc_html__( 'Select sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_footer_column_3',
        'label'       => __( 'Footer Sidebar 3', 'mttheme' ),
        'section'     => 'mttheme_footer_section',
        'default'     => 'footer-middle-column-3',
        'placeholder' => esc_html__( 'Select sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_footer_column_4',
        'label'       => __( 'Footer Sidebar 4', 'mttheme' ),
        'section'     => 'mttheme_footer_section',
        'default'     => 'footer-middle-column-4',
        'placeholder' => esc_html__( 'Select sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_footer_bottom_general_divider',
        'section'     => 'mttheme_footer_section',
        'default'     => '<h3 class="divider-section">' . __( 'Bottom Footer Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_footer_bottom_column_1',
        'label'       => __( 'Footer Bottom Sidebar 1', 'mttheme' ),
        'section'     => 'mttheme_footer_section',
        'default'     => 'footer-bottom-left',
        'placeholder' => esc_html__( 'Select sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_footer_bottom_column_2',
        'label'       => __( 'Footer Bottom Sidebar 2', 'mttheme' ),
        'section'     => 'mttheme_footer_section',
        'default'     => 'footer-bottom-right',
        'placeholder' => esc_html__( 'Select sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_footer_color_divider',
        'section'     => 'mttheme_footer_section',
        'default'     => '<h3 class="divider-section">' . __( 'Main Footer Color Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_footer_bg_color',
        'label'       => __( 'Background', 'mttheme' ),
        'section'     => 'mttheme_footer_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_footer_title_color',
        'label'       => __( 'Title', 'mttheme' ),
        'section'     => 'mttheme_footer_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_footer_title_left_border_color',
        'label'       => __( 'Title Left Border', 'mttheme' ),
        'section'     => 'mttheme_footer_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_footer_color',
        'label'       => __( 'Text', 'mttheme' ),
        'section'     => 'mttheme_footer_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_footer_hover_color',
        'label'       => __( 'Text Hover', 'mttheme' ),
        'section'     => 'mttheme_footer_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_footer_bottom_color_divider',
        'section'     => 'mttheme_footer_section',
        'default'     => '<h3 class="divider-section">' . __( 'Bottom Footer Color Settings', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_footer_bottom_color',
        'label'       => __( 'Text', 'mttheme' ),
        'section'     => 'mttheme_footer_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'color',
        'settings'    => 'mttheme_footer_bottom_hover_color',
        'label'       => __( 'Text Hover', 'mttheme' ),
        'section'     => 'mttheme_footer_section',  
        'choices'     => [
            'alpha' => true,
        ],
    ] );