<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

Kirki::add_section( 'mttheme_home_section', array(
    'title' => __( 'Default Posts / Blog Home', 'mttheme' ),
    'panel' => 'mttheme_layout_content_panel',
) );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_home_divider',
        'section'     => 'mttheme_home_section',
        'default'     => '<h3 class="divider-section">' . __( 'Layout and Container', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_home_right_sidebar',
        'label'       => __( 'Right Sidebar', 'mttheme' ),
        'section'     => 'mttheme_home_section',
        'default'     => 'sidebar-1',
        'placeholder' => esc_attr__( 'Select home right sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'radio-buttonset',
        'settings'    => 'mttheme_home_layout_width',
        'label'       => __( 'Width', 'mttheme' ),
        'section'     => 'mttheme_home_section',
        'default'     => 'container-fluid',
        'choices'     => [
                            'container-fluid' => __( 'Full Width', 'mttheme' ),
                            'container'       => __( 'Boxed', 'mttheme' )
                        ],
    ] );


Kirki::add_section( 'mttheme_single_post_section', array(
    'title' => __( 'Single Post', 'mttheme' ),
    'panel' => 'mttheme_layout_content_panel',
) );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_header_divider',
        'section'     => 'mttheme_single_post_section',
        'default'     => '<h3 class="divider-section">' . __( 'Layout and Container', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'select',
        'settings'    => 'mttheme_single_post_right_sidebar',
        'label'       => __( 'Right Sidebar', 'mttheme' ),
        'section'     => 'mttheme_single_post_section',
        'placeholder' => esc_attr__( 'Select right sidebar', 'mttheme' ),
        'multiple'    => 1,
        'choices'     => mttheme_register_sidebar_customizer_array(),
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'radio-buttonset',
        'settings'    => 'mttheme_single_post_layout_width',
        'label'       => __( 'Width', 'mttheme' ),
        'section'     => 'mttheme_single_post_section',
        'default'     => 'container',
        'choices'     => [
                            'container-fluid' => __( 'Full Width', 'mttheme' ),
                            'container'       => __( 'Boxed', 'mttheme' )
                        ],
    ] );