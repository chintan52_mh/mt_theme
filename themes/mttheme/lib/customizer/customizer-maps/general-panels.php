<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

Kirki::add_config( 'mttheme_config_id', array(
    'capability'    => 'edit_theme_options',
    'option_type'   => 'theme_mod',
) );

Kirki::add_panel( 'mttheme_add_general_panel', array(
    'priority'    => 125,
    'title'       => esc_html__( 'General Theme Options', 'mttheme' ),
) );

Kirki::add_panel( 'mttheme_layout_content_panel', array(
    'priority'    => 130,
    'title'       => esc_html__( 'Layout and Content', 'mttheme' ),
) );

Kirki::add_panel( 'mttheme_header_settings_panel', array(
    'priority'    => 135,
    'title'       => esc_html__( 'Header Settings', 'mttheme' ),
) );

Kirki::add_panel( 'mttheme_title_settings_panel', array(
    'priority'    => 140,
    'title'       => esc_html__( 'Page Title Settings', 'mttheme' ),
) );

Kirki::add_panel( 'mttheme_footer_settings_panel', array(
    'priority'    => 145,
    'title'       => esc_html__( 'Footer Settings', 'mttheme' ),
) );

Kirki::add_panel( 'mttheme_typography_panel', array(
    'priority'    => 150,
    'title'       => esc_html__( 'Typography settings', 'mttheme' ),
) );