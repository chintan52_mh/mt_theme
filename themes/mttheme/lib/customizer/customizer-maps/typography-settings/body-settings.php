<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

// body section
Kirki::add_section( 'mttheme_body_typography_section', array(
    'title' => __( 'Body', 'mttheme' ),
    'panel' => 'mttheme_typography_panel',
) );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_body_typography_divider',
        'section'     => 'mttheme_body_typography_section',
        'default'     => '<h3 class="divider-section">' . __( 'Body Typography', 'mttheme' ) . '</h3>',
    ] );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'typography',
        'settings'    => 'mttheme_body_typography',
        'label'       => '',
        'section'     => 'mttheme_body_typography_section',
        'default'     => [
            'font-family'    => '',
            'variant'        => '',
            'font-size'      => '',
            'line-height'    => '',
            'letter-spacing' => '',
            'color'          => '',
            'text-transform' => '',
            'text-align'     => '',
        ],
        'priority'    => 10,
        'transport'   => 'auto',
        'output'      => [
            [
                'element' => 'body',
            ],
        ],
    ] );