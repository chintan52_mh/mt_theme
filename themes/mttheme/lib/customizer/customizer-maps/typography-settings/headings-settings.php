<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

// heading section
Kirki::add_section( 'mttheme_headings_typography_section', array(
    'title' => __( 'Headings', 'mttheme' ),
    'panel' => 'mttheme_typography_panel',
) );
    
    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_h1_typography_divider',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => '<h3 class="divider-section">' . __( 'H1 Typography', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'typography',
        'settings'    => 'mttheme_h1_typography',
        'label'       => '',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => [
            'font-family'    => '',
            'variant'        => '',
            'font-size'      => '',
            'line-height'    => '',
            'letter-spacing' => '',
            'color'          => '',
            'text-transform' => '',
            'text-align'     => '',
        ],
        'transport'   => 'auto',
        'output'      => [
            [
                'element' => 'h1',
            ],
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_h2_typography_divider',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => '<h3 class="divider-section">' . __( 'H2 Typography', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'typography',
        'settings'    => 'mttheme_h2_typography',
        'label'       => '',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => [
            'font-family'    => '',
            'variant'        => '',
            'font-size'      => '',
            'line-height'    => '',
            'letter-spacing' => '',
            'color'          => '',
            'text-transform' => '',
            'text-align'     => '',
        ],
        'priority'    => 10,
        'transport'   => 'auto',
        'output'      => [
            [
                'element' => 'h2',
            ],
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_h3_typography_divider',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => '<h3 class="divider-section">' . __( 'H3 Typography', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'typography',
        'settings'    => 'mttheme_h3_typography',
        'label'       => '',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => [
            'font-family'    => '',
            'variant'        => '',
            'font-size'      => '',
            'line-height'    => '',
            'letter-spacing' => '',
            'color'          => '',
            'text-transform' => '',
            'text-align'     => '',
        ],
        'priority'    => 10,
        'transport'   => 'auto',
        'output'      => [
            [
                'element' => 'h3',
            ],
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_h4_typography_divider',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => '<h3 class="divider-section">' . __( 'H4 Typography', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'typography',
        'settings'    => 'mttheme_h4_typography',
        'label'       => '',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => [
            'font-family'    => '',
            'variant'        => '',
            'font-size'      => '',
            'line-height'    => '',
            'letter-spacing' => '',
            'color'          => '',
            'text-transform' => '',
            'text-align'     => '',
        ],
        'priority'    => 10,
        'transport'   => 'auto',
        'output'      => [
            [
                'element' => 'h4',
            ],
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_h5_typography_divider',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => '<h3 class="divider-section">' . __( 'H5 Typography', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'typography',
        'settings'    => 'mttheme_h5_typography',
        'label'       => '',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => [
            'font-family'    => '',
            'variant'        => '',
            'font-size'      => '',
            'line-height'    => '',
            'letter-spacing' => '',
            'color'          => '',
            'text-transform' => '',
            'text-align'     => '',
        ],
        'priority'    => 10,
        'transport'   => 'auto',
        'output'      => [
            [
                'element' => 'h5',
            ],
        ],
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'custom',
        'settings'    => 'mttheme_h6_typography_divider',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => '<h3 class="divider-section">' . __( 'H6 Typography', 'mttheme' ) . '</h3>',
    ] );

    Kirki::add_field( 'mttheme_config_id', [
        'type'        => 'typography',
        'settings'    => 'mttheme_h6_typography',
        'label'       => '',
        'section'     => 'mttheme_headings_typography_section',
        'default'     => [
            'font-family'    => '',
            'variant'        => '',
            'font-size'      => '',
            'line-height'    => '',
            'letter-spacing' => '',
            'color'          => '',
            'text-transform' => '',
            'text-align'     => '',
        ],
        'priority'    => 10,
        'transport'   => 'auto',
        'output'      => [
            [
                'element' => 'h6',
            ],
        ],
    ] );