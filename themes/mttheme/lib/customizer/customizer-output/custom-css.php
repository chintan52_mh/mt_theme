<?php
/**
 * Generate css.
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }
	
	/* 404 Page settings */
	$mttheme_404_background_color	= get_theme_mod( 'mttheme_404_background_color', '' );
	$mttheme_404_main_title_color 	= get_theme_mod( 'mttheme_404_main_title_color', '' );
	$mttheme_404_title_color 		= get_theme_mod( 'mttheme_404_title_color', '' );
	$mttheme_404_back_to_home_color = get_theme_mod( 'mttheme_404_back_to_home_color', '' );

	/* Header settings */
	$mttheme_header_bg_color 	= mttheme_option( 'mttheme_header_bg_color', '' );
	$mttheme_header_color 		= get_theme_mod( 'mttheme_header_color', '' );
	$mttheme_header_hover_color = get_theme_mod( 'mttheme_header_hover_color', '' );
	$mttheme_header_icon_color 	= get_theme_mod( 'mttheme_header_icon_color', '' );

	/* Sticky Header settings */
	$mttheme_sticky_header_bg_color		= get_theme_mod( 'mttheme_sticky_header_bg_color', '' );
	$mttheme_sticky_header_color		= get_theme_mod( 'mttheme_sticky_header_color', '' );
	$mttheme_sticky_header_hover_color	= get_theme_mod( 'mttheme_sticky_header_hover_color', '' );
	$mttheme_sticky_header_icon_color	= get_theme_mod( 'mttheme_sticky_header_icon_color', '' );

	/* Mobile Header settings */
	$mttheme_mobile_menu_bg_color			= get_theme_mod( 'mttheme_mobile_menu_bg_color', '' );
	$mttheme_mobile_toggle_color			= get_theme_mod( 'mttheme_mobile_toggle_color', '' );
	$mttheme_sticky_mobile_toggle_color		= get_theme_mod( 'mttheme_sticky_mobile_toggle_color', '' );
	$mttheme_mobile_menu_separator_color	= get_theme_mod( 'mttheme_mobile_menu_separator_color', '' );

	/* Title settings */
	$mttheme_title_bg_color		= get_theme_mod( 'mttheme_title_bg_color', '' );
	$mttheme_title_color 		= get_theme_mod( 'mttheme_title_color', '' );
	$mttheme_left_border_color 	= get_theme_mod( 'mttheme_left_border_color', '' );
	$mttheme_subtitle_color		= get_theme_mod( 'mttheme_subtitle_color', '' );

	/* Main Footer settings */
	$mttheme_footer_bg_color 		= get_theme_mod( 'mttheme_footer_bg_color', '' );
	$mttheme_footer_title_color		= get_theme_mod( 'mttheme_footer_title_color', '' );
	$mttheme_footer_title_left_border_color  = get_theme_mod( 'mttheme_footer_title_left_border_color', '' );
	$mttheme_footer_color 			= get_theme_mod( 'mttheme_footer_color', '' );
	$mttheme_footer_hover_color		= get_theme_mod( 'mttheme_footer_hover_color', '' );

	/* Bottom Footer settings */
	$mttheme_footer_bottom_color		= get_theme_mod( 'mttheme_footer_bottom_color', '' );
	$mttheme_footer_bottom_hover_color	= get_theme_mod( 'mttheme_footer_bottom_hover_color', '' );
?>

<?php if( $mttheme_404_background_color ) : ?>
/* 404 Background Color */
.page-not-found .not-found-inner { background-color: <?php echo sprintf( '%s', $mttheme_404_background_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_404_main_title_color ) : ?>
/* 404 Main Title Color */
.page-not-found .not-found-inner .mttheme-404-content-wrap h2 { color: <?php echo sprintf( '%s', $mttheme_404_main_title_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_404_title_color ) : ?>
/* 404 Title Color */
.page-not-found .not-found-inner .mttheme-404-content-wrap p { color: <?php echo sprintf( '%s', $mttheme_404_title_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_404_back_to_home_color ) : ?>
/* 404 Back to Home Color */
.page-not-found .not-found-inner .mttheme-404-content-wrap .backtohome { color: <?php echo sprintf( '%s', $mttheme_404_back_to_home_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_header_bg_color ) : ?>
/* Header bg color */
header { background-color: <?php echo sprintf( '%s', $mttheme_header_bg_color ); ?>; }
@media screen and (max-width: 1024px) { .header_type_1 { background: <?php echo sprintf( '%s', $mttheme_header_bg_color ); ?>; } }
<?php endif; ?>

<?php if( $mttheme_header_color ) : ?>
/* Header text color */
header .top_navbar .navbar-nav .nav-item .nav-link, .header_type_1 .header-sidebar .textwidget p a, .header_type_1 .header-sidebar .textwidget p { color: <?php echo sprintf( '%s', $mttheme_header_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_header_hover_color ) : ?>
/* Header text hover color */
header .top_navbar .navbar-nav .nav-item.current-menu-item .nav-link, .header_type_1 .navbar .navbar-nav li.current-menu-item a, header .top_navbar .navbar-nav .nav-item .nav-link:hover, header .top_navbar .navbar-nav .nav-item .nav-link:focus { color: <?php echo sprintf( '%s', $mttheme_header_hover_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_header_icon_color ) : ?>
/* Header icon color */
header i.text-warning { color: <?php echo sprintf( '%s', $mttheme_header_icon_color ); ?>!important; }
<?php endif; ?>

<?php if( $mttheme_sticky_header_bg_color ) : ?>
/* Sticky header bg color */
header.sticky { background-color: <?php echo sprintf( '%s', $mttheme_sticky_header_bg_color ); ?> !important; }
<?php endif; ?>

<?php if( $mttheme_sticky_header_color ) : ?>
/* Sticky header text color */
header.sticky .top_navbar .navbar-nav .nav-item .nav-link, .header_type_1.sticky .header-sidebar .textwidget p a, .header_type_1.sticky .header-sidebar .textwidget p { color: <?php echo sprintf( '%s', $mttheme_sticky_header_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_sticky_header_hover_color ) : ?>
/* Sticky header text hover color */
header.sticky .top_navbar .navbar-nav .nav-item .nav-link:hover, header.sticky .top_navbar .navbar-nav .nav-item .nav-link:focus { color: <?php echo sprintf( '%s', $mttheme_sticky_header_hover_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_sticky_header_icon_color ) : ?>
/* Sticky header icon color */
.header_type_1.sticky i.text-warning { color: <?php echo sprintf( '%s', $mttheme_sticky_header_icon_color ); ?>!important; }
<?php endif; ?>

<?php if( $mttheme_mobile_menu_bg_color ) : ?>
/* Mobile menu bg color */
@media screen and (max-width: 1024px) { .header_type_1 .navbar .navbar-nav { background-color: <?php echo sprintf( '%s', $mttheme_mobile_menu_bg_color ); ?>; } }
<?php endif; ?>

<?php if( $mttheme_mobile_toggle_color ) : ?>
/* Mobile toggle color */
.header_type_1 .navbar .navbar-toggler span { background-color: <?php echo sprintf( '%s', $mttheme_mobile_toggle_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_sticky_mobile_toggle_color ) : ?>
/* Mobile sticky toggle color */
.header_type_1.sticky .navbar .navbar-toggler span { background-color: <?php echo sprintf( '%s', $mttheme_sticky_mobile_toggle_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_mobile_menu_separator_color ) : ?>
/* Mobile menu separator color */
@media screen and (max-width: 1024px) { .header_type_1 .navbar .navbar-nav li { border-bottom-color: <?php echo sprintf( '%s', $mttheme_mobile_menu_separator_color ); ?>; } }
<?php endif; ?>

<?php if( $mttheme_title_bg_color ) : ?>
/* Title bg color */
.page-title-wrapper { background-color: <?php echo sprintf( '%s', $mttheme_title_bg_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_title_color ) : ?>
/* Title color */
.page-title-wrapper .heading-style-1 .page-title { color: <?php echo sprintf( '%s', $mttheme_title_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_left_border_color ) : ?>
/* Title left border color  */
.heading-style-1, .page-title-wrapper .heading-style-1 { border-left-color: <?php echo sprintf( '%s', $mttheme_left_border_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_subtitle_color ) : ?>
/* Title subtitle color  */
.page-title-wrapper .heading-style-1 .page-subtitle { color: <?php echo sprintf( '%s', $mttheme_subtitle_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_footer_bg_color ) : ?>
/* Main footer bg color */
.site-footer .mttheme-footer { background-color: <?php echo sprintf( '%s', $mttheme_footer_bg_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_footer_title_color ) : ?>
/* Main footer title color */
.site-footer .mttheme-footer .widget-title { color: <?php echo sprintf( '%s', $mttheme_footer_title_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_footer_title_left_border_color ) : ?>
/* Main footer title left border color */
.site-footer .mttheme-footer .widget-title { border-left-color: <?php echo sprintf( '%s', $mttheme_footer_title_left_border_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_footer_color ) : ?>
/* Main footer text color */
.site-footer .mttheme-footer .menu li a, .site-footer .mttheme-footer .textwidget p a, .site-footer .social-icon.social-icon-style-1 a, .site-footer .mttheme-footer .textwidget p{ color: <?php echo sprintf( '%s', $mttheme_footer_color ); ?>; }

.site-footer .social-icon.social-icon-style-1 a{ border-color: <?php echo sprintf( '%s', $mttheme_footer_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_footer_hover_color ) : ?>
/* Main footer text hover color */
.site-footer .mttheme-footer .menu li a:hover, .site-footer .mttheme-footer .menu li a:focus, .site-footer .mttheme-footer .textwidget p a:hover, .site-footer .mttheme-footer .textwidget p a:focus, .site-footer .social-icon.social-icon-style-1 a:hover, .site-footer .social-icon.social-icon-style-1 a:focus { color: <?php echo sprintf( '%s', $mttheme_footer_hover_color ); ?>; }

.site-footer .social-icon.social-icon-style-1 a:hover, .site-footer .social-icon.social-icon-style-1 a:focus { border-color: <?php echo sprintf( '%s', $mttheme_footer_hover_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_footer_bottom_color ) : ?>
/* Bottom footer color */
.site-footer .mttheme-footer-bottom .footer-bottom-sidebar.copyright-info .textwidget p, .site-footer .mttheme-footer-bottom .footer-bottom-sidebar.copyright-info .textwidget p a, .site-footer .mttheme-footer-bottom .footer-bottom-sidebar.footer-links .menu li a { color: <?php echo sprintf( '%s', $mttheme_footer_bottom_color ); ?>; }
<?php endif; ?>

<?php if( $mttheme_footer_bottom_hover_color ) : ?>
/* Bottom footer hover color */
.site-footer .mttheme-footer-bottom .footer-bottom-sidebar.copyright-info .textwidget p a:hover, .site-footer .mttheme-footer-bottom .footer-bottom-sidebar.copyright-info .textwidget p a:focus, .site-footer .mttheme-footer-bottom .footer-bottom-sidebar.footer-links .menu li a:hover, .site-footer .mttheme-footer-bottom .footer-bottom-sidebar.footer-links .menu li a:focus { color: <?php echo sprintf( '%s', $mttheme_footer_bottom_hover_color ); ?>; }
<?php endif; ?>