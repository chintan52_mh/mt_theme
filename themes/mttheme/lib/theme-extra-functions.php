<?php
/**
 * Theme extra function file
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Theme setup function
 */
if ( ! function_exists( 'mttheme_theme_setup' ) ) {
    function mttheme_theme_setup() {
        /*
         * Text Domain
         */
        load_theme_textdomain( 'mttheme', get_template_directory() . '/languages' );

        /*
         * To add default posts and comments RSS feed links to theme head.
         */
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /**
         * Custom image sizes for posts, pages, gallery, slider.
         */
        add_theme_support( 'post-thumbnails' );

        add_image_size( 'mttheme-medium-image', 450, '', true );
        add_image_size( 'mttheme-extra-medium-image', 850, '', true );
        set_post_thumbnail_size( 1200, 790 );

        // Set Custom Header
        add_theme_support( 'custom-header', apply_filters( 'mttheme_custom_header_args', array(
            'width' => 1920,
            'height'=> 100,
        ) ) );

        // Set Custom Body Background
        add_theme_support( 'custom-background' );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        /*
         * Register menu for Mttheme theme.
         */
        register_nav_menus( array(
            'primary-menu' => __( 'Primary Menu', 'mttheme' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery'
        ) );

        /* Add support for Block Styles. */
        add_theme_support( 'wp-block-styles' );

        /* Add support for full and wide align images. */
        add_theme_support( 'align-wide' );

        /* This theme styles the visual editor with editor-style.css to match the theme style. */
        add_editor_style();

        /* Add support for responsive embedded content. */
        add_theme_support( 'responsive-embeds' );

        /* Add custom editor font sizes. */
        add_theme_support(
            'editor-font-sizes',
            array(
                array(
                    'name'      => __( 'Small', 'mttheme' ),
                    'shortName' => __( 'S', 'mttheme' ),
                    'size'      => 12,
                    'slug'      => 'small',
                ),
                array(
                    'name'      => __( 'Normal', 'mttheme' ),
                    'shortName' => __( 'M', 'mttheme' ),
                    'size'      => 13,
                    'slug'      => 'normal',
                ),
                array(
                    'name'      => __( 'Large', 'mttheme' ),
                    'shortName' => __( 'L', 'mttheme' ),
                    'size'      => 16,
                    'slug'      => 'large',
                ),
                array(
                    'name'      => __( 'Huge', 'mttheme' ),
                    'shortName' => __( 'XL', 'mttheme' ),
                    'size'      => 23,
                    'slug'      => 'huge',
                ),
            )
        );

        /* Editor color palette. */
        add_theme_support(
            'editor-color-palette',
            array(
                array(
                    'name'  => __( 'Primary', 'mttheme' ),
                    'slug'  => 'primary',
                    'color' => '#6f6f6f',
                ),
                array(
                    'name'  => __( 'Secondary', 'mttheme' ),
                    'slug'  => 'secondary',
                    'color' => '#f57250',
                ),
                array(
                    'name'  => __( 'Dark Gray', 'mttheme' ),
                    'slug'  => 'dark-gray',
                    'color' => '#232323',
                ),
                array(
                    'name'  => __( 'Light Gray', 'mttheme' ),
                    'slug'  => 'light-gray',
                    'color' => '#f1f1f1',
                ),
                array(
                    'name'  => __( 'White', 'mttheme' ),
                    'slug'  => 'white',
                    'color' => '#ffffff',
                ),
            )
        );
    }
}
add_action( 'after_setup_theme', 'mttheme_theme_setup' );

/**
 * Theme setup function
 *
 * @return mixed value.
 */
if( ! function_exists( 'mttheme_option' ) ) {
    function mttheme_option( $option, $default_value ) {
        global $post;

        $mttheme_option_value = '';
        if( is_404() ) {
            
            $mttheme_option_value = get_theme_mod( $option, $default_value );

        } else {

            if( ! ( is_category() || is_archive() || is_author() || is_tag() || is_search() || is_home() ) ) {

                // Meta Prefix
                $meta_prefix = '_';
                $single_value = get_post_meta( $post->ID, $meta_prefix.$option.'_single', true );
                if( is_string( $single_value ) && ( strlen( $single_value ) > 0 || is_array( $single_value ) ) && ( $single_value != 'default' ) ) {
                    if( strtolower( $single_value ) == '.' ) {
                        $mttheme_option_value = '';
                    } else {
                        $mttheme_option_value = $single_value;
                    }
                } else {
                    $mttheme_option_value = get_theme_mod( $option, $default_value );
                }
            } else {
                $mttheme_option_value = get_theme_mod( $option, $default_value );
            }
        }
        return $mttheme_option_value;
    }
}

/*
 * Content Width (Set the content width based on the theme's design and stylesheet.)
 */
if ( ! function_exists( 'mttheme_content_width' ) ) {
    function mttheme_content_width() {
        $GLOBALS['content_width'] = apply_filters( 'mttheme_content_width', 1170 );
    }
}
add_action( 'template_redirect', 'mttheme_content_width', 0 );

/**
 * For Image Alt Text
 *
 * @return string
 */
if ( ! function_exists( 'mttheme_option_image_alt' ) ) {
    function mttheme_option_image_alt( $attachment_id ) {

        if ( wp_attachment_is_image( $attachment_id ) == false ) {
            return;
        }

        $mttheme_image_alt = [
                                'alt' => get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ),
                            ];
        
        return $mttheme_image_alt;
    }
}

/**
 * For Image Title Text
 *
 * @return string
 */
if ( ! function_exists( 'mttheme_option_image_title' ) ) {
    function mttheme_option_image_title( $attachment_id ) {

        if ( wp_attachment_is_image( $attachment_id ) == false ) {
            return;
        }
        
        $mttheme_image_title = [
                                'title' =>  esc_attr( get_the_title( $attachment_id ) ),
                            ];

        return $mttheme_image_title;
    }
}

/**
 * Custom Comment Callback
 */
if ( ! function_exists( 'mttheme_comment_callback' ) ) {
    function mttheme_comment_callback( $comment, $args, $depth ) {
        if ( 'div' === $args['style'] ) {
            $tag       = 'div';
            $add_below = 'comment';
        } else {
            $tag       = 'li';
            $add_below = 'div-comment';
        }
        $author_url = get_author_posts_url( get_the_author_meta( 'ID' ) );
        ?>
        <<?php echo esc_attr($tag) ?> <?php comment_class( empty( $args['has_children'] ) ? 'post-comment' : 'parent post-comment' ) ?> id="comment-<?php comment_ID() ?>">
        <?php if ( 'div' != $args['style'] ) { ?>
            <div id="div-comment-<?php comment_ID() ?>" class="comment-author-wrapper">
        <?php } ?>

            <div class="xs-display-block comment-image-box">
                <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
            </div>

            <div class="last-paragraph-no-margin xs-display-block comment-text-box">
                <div class="comment-title-edit-link">
                    <a href="<?php echo esc_url( $author_url ); ?>" ><?php echo get_comment_author() ?></a>
                    <?php edit_comment_link( esc_html__( '(Edit)', 'mttheme' ), '  ', '' ); ?>
                </div>
                <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                <div class="comments-date">
                    <?php
                    /* translators: 1: date, 2: time */
                    printf( esc_html__('%1$s, %2$s', 'mttheme'), get_comment_date(),  get_comment_time() ); ?>
                </div>
                
                <div class="comment-text">
                    <?php comment_text(); ?>
                </div>
            </div>
            
            <?php if ( $comment->comment_approved == '0' ) { ?>
                <em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'mttheme' ); ?></em>
                <br />
            <?php } ?>
        
        <?php if ( 'div' != $args['style'] ) { ?>
        </div>
        <?php } ?>
        <?php
    }
}

/**
 * To Register Custom Sidebar Areas
 */
if ( ! function_exists( 'mttheme_custom_sidebar_areas' ) ) :
    function mttheme_custom_sidebar_areas() {

        $mttheme_custom_sidebar = get_theme_mod( 'mttheme_custom_sidebar', '' );

        if ( is_array( $mttheme_custom_sidebar ) ) {
            foreach ( $mttheme_custom_sidebar as $sidebar ) {

                if ( empty( $sidebar['sidebar_name'] ) ) {
                    continue;
                }

                register_sidebar ( array (
                    'name' => $sidebar['sidebar_name'],
                    'id' => sanitize_title( $sidebar['sidebar_name'] ),
                    'before_widget' => '<div id="%1$s" class="widget custom-widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title'  => '<div class="widget-title">',
                    'after_title'   => '</div>',
                ) );
            }
        }
    }
endif;
add_action( 'widgets_init', 'mttheme_custom_sidebar_areas' );

/**
 * Get Register Sidebar Customizer array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_register_sidebar_customizer_array' ) ) {
    function mttheme_register_sidebar_customizer_array() {
        global $wp_registered_sidebars;
        $sidebar_array = array();
        if ( ! empty( $wp_registered_sidebars ) && is_array( $wp_registered_sidebars ) ) {
            $sidebar_array[''] = esc_html__( 'No Sidebar', 'mttheme' );
            foreach( $wp_registered_sidebars as $sidebar ) {
                $sidebar_array[ $sidebar['id'] ] = $sidebar['name'];
            }
        }
        return $sidebar_array;
    }
}

/**
 * Load Customizer Settings
 */
if ( ! function_exists( 'mttheme_customizer_settings' ) ) :
    function mttheme_customizer_settings() {
        require( MTTHEME_THEME_LIB . '/customizer/theme-customizer.php' );
    }
endif;
add_action( 'init', 'mttheme_customizer_settings', 20 );

/**
 * Theme excerpt
 */
if ( ! function_exists( 'mttheme_get_the_excerpt_theme' ) ) {
    function mttheme_get_the_excerpt_theme( $length ) {

        return mttheme_Excerpt::mttheme_get_by_length($length);
    }
}

/**
 * Post content filter
 */
if ( ! function_exists( 'mttheme_get_the_post_content' ) ) {
    function mttheme_get_the_post_content() {

        return apply_filters( 'the_content', get_the_content() );
    }
}

/**
 * Remove issues with prefetching adding extra views
 */
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

/**
 * Check sidebar active
 *
 * @return bool
 */
if ( ! function_exists( 'mttheme_check_active_sidebar' ) ) {
    function mttheme_check_active_sidebar( $sidebar ) {

        if ( ! empty( $sidebar ) && is_active_sidebar( $sidebar ) ) {
            return true;
        }
        return false;
    }
}

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
if ( ! function_exists( 'mttheme_pingback_header' ) ) {
    function mttheme_pingback_header() {
        if ( is_singular() && pings_open() ) {
            echo '<link rel="pingback" href="' . esc_url( get_bloginfo( 'pingback_url' ) ) . '">';
        }
    }
}
add_action( 'wp_head', 'mttheme_pingback_header', 1 );

/**
 * Get navigation menu array
 *
 * @return array
 */
if ( ! function_exists( 'mttheme_get_nav_menus_array' ) ) {
    function mttheme_get_nav_menus_array() {
        $menu_array = [];
        $menus = wp_get_nav_menus();
        if ( $menus ) {
            foreach ( $menus as $key => $menu ) {
                $menu_array[$menu->slug] = $menu->name;
            }
        }
        return $menu_array;
    }
}

/**
 * Add extra field in profile fields
 */
if ( ! function_exists( 'mttheme_extra_profile_fields' ) ) {
    function mttheme_extra_profile_fields( $user ) { 
        ?>
            <h3>
                <?php esc_html_e( 'Extra User Details', 'mttheme' ); ?>
            </h3>
            <table class="form-table">
                <tr>
                    <th>
                        <label for="designation">
                            <?php esc_html_e( 'Designation', 'mttheme' ); ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" name="designation" id="designation" value="<?php echo esc_attr( get_the_author_meta( 'designation', $user->ID ) ); ?>" class="regular-text" /><br />
                        <span class="description">
                            <?php esc_html_e( 'Enter your designation.', 'mttheme' ); ?>
                        </span>
                    </td>
                </tr>
            </table>
        <?php
    }
}

add_action( 'show_user_profile', 'mttheme_extra_profile_fields', 10 );
add_action( 'edit_user_profile', 'mttheme_extra_profile_fields', 10 );

/**
 * Save extra field in profile fields
 */
if ( ! function_exists( 'mttheme_save_extra_profile_fields' ) ) {
    function mttheme_save_extra_profile_fields( $user_id ) {

        if ( ! current_user_can( 'edit_user', $user_id ) ) {
            return false;
        }

        // Edit the following lines according to your set fields 
        update_user_meta( $user_id, 'designation', $_POST['designation'] );
    }
}

add_action( 'personal_options_update', 'mttheme_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'mttheme_save_extra_profile_fields' );

/**
 * Remove auto p form in contact form 7 
 */
add_filter('wpcf7_autop_or_not', '__return_false');