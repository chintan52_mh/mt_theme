( function( $ ) {

    "use strict";

    var isMobile = false;
    var isiPhoneiPad = false;

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        isMobile = true;
    }

    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        isiPhoneiPad = true;
    }

    const headerHeight = $('header').outerHeight();
    let lastScroll = 0;
    /* Window scroll start code */
    $(window).scroll(function() {
        // Header sticky code
        const currentScroll = window.pageYOffset;
    
        if( currentScroll >= headerHeight ) { 
          $('header.down-scroll-sticky').addClass('sticky'); 
        } else { 
          $('header.down-scroll-sticky').removeClass('sticky');
        }
        
        // Header upscroll
        if( ( currentScroll > lastScroll ) || ( currentScroll <= headerHeight ) ) {
            // down
            $('header.up-scroll-sticky').removeClass('sticky'); 
        } else if (currentScroll < lastScroll) {
            // up
            $('header.up-scroll-sticky').addClass('sticky');
        } else {
            $('header.up-scroll-sticky').removeClass('sticky');
        }
        lastScroll = currentScroll;
    });
    
    // Header top space
    function setHeaderTopSpace() {
        $('.mttheme-main-content-wrap.header-top-space').css( 'margin-top', $('header').outerHeight() );
        if ( $(window).width() <= 1024 ) {
            $('.header_type_1 .navbar .navbar-collapse').css( 'top', $('header').outerHeight() );
        } else {
            $('.header_type_1 .navbar .navbar-collapse').css( 'top', '' );
        }
    }

    /* Window resize event start code */
    $(window).resize(function () {
        setHeaderTopSpace();
    });
    /* Window ready event start code */
    $(document).ready(function () {

        // Header top space
        setHeaderTopSpace();

        // Add class in page when page not setup elementor
        if ( $( '.page-content .elementor').length > 0 ) {
            $('.page-content').removeClass('page-default-space container');
        } else {
            $('.page-content').addClass('page-default-space container');
        }
        // Header mobile menu
        $(document).on('click', '.navbar-toggler', function () {
            $("body").toggleClass("overflow-hidden");        
        });
        
        // Header search popup
        $('.header-search').magnificPopup({
            preloader: false,
            mainClass: 'mfp-fade show-search-popup mttheme-search-popup',
            fixedContentPos: true,
            closeBtnInside: false,
            closeMarkup:'<button title="%title%" type="button" class="mfp-close">×</button>',
        });
        
        // Property listing chnages
        $( '.property-filter-wrap' ).on( 'change', 'select, input[type="radio"]', function() {
            $( this ).closest( 'form' ).submit();
        });

        // Property single thumb popup
        $('.image-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            closeBtnInside: false,
            closeMarkup:'<button title="%title%" type="button" class="mfp-close">×</button>',
        });

        // Property single floorplan
        var floorplan = new Swiper('.floorplan', {
            slidesPerView: 1,
            spaceBetween: 30,
            effect: 'fade',
            pagination: {
                el: '.floorplan-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.floorplan-next',
                prevEl: '.floorplan-prev',
            },
        });

        // Property single loanofferd
        var loanoffered = new Swiper('.loanoffered', {
            spaceBetween: 20,
            breakpoints: {
                576: {
                    slidesPerView: 2,
                },
                768: {
                    slidesPerView: 4,
                },
                1025: {
                    slidesPerView: 3,
                },
                1200: {
                    slidesPerView: 4,
                },
            }
        });

        // Post single related post
        var relatedPostSlider = new Swiper('.related-post-slider', {
            navigation: {
                nextEl: '.related-post-next',
                prevEl: '.related-post-prev',
            },
            breakpoints: {
                600: {
                    slidesPerView: 1.2,
                    spaceBetween: 10,
                },
                768: {
                    slidesPerView: 2.5,
                    spaceBetween: 15,
                },
                1025: {
                    slidesPerView: 2.5,
                    spaceBetween: 20,
                },
                1200: {
                    slidesPerView: 3.5,
                    spaceBetween: 30,
                },
            }
        });


    });
})( jQuery );

