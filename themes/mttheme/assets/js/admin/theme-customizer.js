( function( $ ) {

    "use strict";
    
    $( document ).ready(function() {

        var mttheme_customize = wp.customize,
            $mttheme_scroll_to_top_color = '';

        // Get Hover Values        
        var $mttheme_scroll_to_top_hover_color = '';

        // 404 Page Background Color 
        mttheme_customize( 'mttheme_404_background_color', function( value ) { 
            value.bind( function( to ) {
               $( '.page-not-found .not-found-inner' ).css( 'background-color', to );
            });
        });

        // 404 Page Main Title Color 
        mttheme_customize( 'mttheme_404_main_title_color', function( value ) { 
            value.bind( function( to ) {
               $( '.page-not-found .not-found-inner .mttheme-404-content-wrap h2' ).css( 'color', to );
            });
        });

        // 404 Page Title Color 
        mttheme_customize( 'mttheme_404_title_color', function( value ) { 
            value.bind( function( to ) {
               $( '.page-not-found .not-found-inner .mttheme-404-content-wrap .mttheme-404-subtitle' ).css( 'color', to );
            });
        });

        // 404 Back to Home Color 
        mttheme_customize( 'mttheme_404_back_to_home_color', function( value ) { 
            value.bind( function( to ) {
               $( '.page-not-found .not-found-inner .mttheme-404-content-wrap .backtohome' ).css( 'color', to );
            });
        });
        
        /* Scroll to Top Color */
        mttheme_customize( 'mttheme_scroll_to_top_color', function( value ) { 
            value.bind( function( to ) {
                $mttheme_scroll_to_top_color = to;
                $( '.scroll-top-arrow' ).css( 'color', to );

                if( !$mttheme_scroll_to_top_hover_color ){
                    mttheme_customize( 'mttheme_scroll_to_top_hover_color', function( value ) {
                        $( '.scroll-top-arrow' ).on( 'hover', function () {                        
                            $(this).css( 'color', '' );
                        }, function(){
                            $(this).css( 'color', $mttheme_scroll_to_top_color );
                        });
                    });
                }
            });
        });

        /* Scroll to Top Hover Color */
        mttheme_customize( 'mttheme_scroll_to_top_hover_color', function( value ) { 
            value.bind( function( to ) {
                $mttheme_scroll_to_top_hover_color = to;
                $( '.scroll-top-arrow' ).on( 'hover', function () {                
                    $(this).css( 'color', to );
                }, function(){
                    $(this).css( 'color', $mttheme_scroll_to_top_color );
                });
            });
        });

    });
})( jQuery );