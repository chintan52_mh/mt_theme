( function( $ ) {

    "use strict";
    
    $( document ).ready(function() {
        $( '.mttheme-color-picker' ).wpColorPicker();
    });
        
})( jQuery );