<?php
/**
 * The template file for search form
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>" autocomplete="off">

	<div class="input-group mb-3">
		<span class="icon-search"></span>
		<input type="search" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search For', 'placeholder', 'mttheme' ) ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'mttheme' ) ?>" />
		<div class="input-group-append">
			<button class="btn btn-outline-secondary search-submit" type="submit">
				<img src="<?php echo esc_url( MTTHEME_THEME_IMAGES_URI.'/next.png' ); ?>" alt="prev-image">
			</button>
		</div>
	</div>

</form>
