<?php

/**
 * The template for displaying the footer part
 *
 * @package Mttheme
 */

// Exit if accessed directly.

if ( ! defined( 'ABSPATH' ) ) { exit; }

/* Check footer enable/disable */

$mttheme_footer_middle_column_1_sidebar = get_theme_mod( 'mttheme_footer_column_1', 'footer-middle-column-1' );
$mttheme_footer_middle_column_2_sidebar = get_theme_mod( 'mttheme_footer_column_2', 'footer-middle-column-2' );
$mttheme_footer_middle_column_3_sidebar = get_theme_mod( 'mttheme_footer_column_3', 'footer-middle-column-3' );
$mttheme_footer_middle_column_4_sidebar = get_theme_mod( 'mttheme_footer_column_4', 'footer-middle-column-4' );

$mttheme_footer_bottom_column_1_sidebar = get_theme_mod( 'mttheme_footer_bottom_column_1', 'footer-bottom-left' );
$mttheme_footer_bottom_column_2_sidebar = get_theme_mod( 'mttheme_footer_bottom_column_2', 'footer-bottom-right' );

$mttheme_footer_column_1         = mttheme_check_active_sidebar( $mttheme_footer_middle_column_1_sidebar );
$mttheme_footer_column_2         = mttheme_check_active_sidebar( $mttheme_footer_middle_column_2_sidebar );
$mttheme_footer_column_3         = mttheme_check_active_sidebar( $mttheme_footer_middle_column_3_sidebar );
$mttheme_footer_column_4         = mttheme_check_active_sidebar( $mttheme_footer_middle_column_4_sidebar );

$mttheme_footer_bottom_column_1  = mttheme_check_active_sidebar( $mttheme_footer_bottom_column_1_sidebar );
$mttheme_footer_bottom_column_2  = mttheme_check_active_sidebar( $mttheme_footer_bottom_column_2_sidebar );

?>

<footer id="colophon" class="site-footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
    <div class="container-fluid">
        <div class="mttheme-footer footer-four-column">
            <?php if( $mttheme_footer_column_1 || $mttheme_footer_column_2 || $mttheme_footer_column_3 || $mttheme_footer_column_4 ) { ?>

                    <div class="container">
                        <div class="row">

                            <?php if ( $mttheme_footer_column_1 ) { ?>

                                <div class="footer-sidebar col-lg-3 col-sm-6 mb-lg-0 mb-sm-5 mb-4">

                                    <?php dynamic_sidebar( $mttheme_footer_middle_column_1_sidebar ); ?>

                                </div>

                            <?php } ?>

                            <?php if ( $mttheme_footer_column_2 ) { ?>

                                <div class="footer-sidebar col-lg-3 col-sm-6 mb-lg-0 mb-sm-5 mb-4">

                                    <?php dynamic_sidebar( $mttheme_footer_middle_column_2_sidebar ); ?>

                                </div>

                            <?php } ?>

                            <?php if ( $mttheme_footer_column_3 ) { ?>

                                <div class="footer-sidebar col-lg-3 col-sm-6 mb-sm-0 mb-4">

                                    <?php dynamic_sidebar( $mttheme_footer_middle_column_3_sidebar ); ?>

                                </div>

                            <?php } ?>

                            <?php if ( $mttheme_footer_column_4 ) { ?>

                                <div class="footer-sidebar col-lg-3 col-sm-6">

                                    <?php dynamic_sidebar( $mttheme_footer_middle_column_4_sidebar ); ?>

                                </div>

                            <?php } ?>
                        </div>
                    </div>
            <?php } ?>
        </div>
        <?php if( $mttheme_footer_bottom_column_1 || $mttheme_footer_bottom_column_2 ) { ?>
            <div class="mttheme-footer-bottom">

                <div class="row">

                    <?php if ( $mttheme_footer_bottom_column_1 ) { ?>

                        <div class="footer-bottom-sidebar copyright-info col-md-6">

                            <?php dynamic_sidebar( $mttheme_footer_bottom_column_1_sidebar ); ?>

                        </div>

                    <?php } ?>

                    <?php if ( $mttheme_footer_bottom_column_2 ) { ?>

                        <div class="footer-bottom-sidebar footer-links col-md-6 d-flex justify-content-md-end justify-content-center mt-md-0 mt-4">

                            <?php dynamic_sidebar( $mttheme_footer_bottom_column_2_sidebar ); ?>

                        </div>

                    <?php } ?>

                </div>
            </div>
        <?php } ?>
    </div>
</footer>