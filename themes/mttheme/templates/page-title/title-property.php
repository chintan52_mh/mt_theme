<?php
/**
 * Template part for displaying page title in Property Listing Template
 *
 * @package Mttheme
 */

global $post;

$page_title 	= get_the_title();
$page_subtitle 	= get_post_meta( $post->ID, '_mttheme_page_subtitle_single', true );

$location = ! empty( $_GET['location'] ) ? $_GET['location'] : '';
$bedrooms = ! empty( $_GET['bedrooms'] ) ? $_GET['bedrooms'] : '';
$propery_type = ! empty( $_GET['propery-type'] ) ? $_GET['propery-type'] : '';

$propery_type_lists = mttheme_get_propery_types_array();
$bedrooms_options = mttheme_get_bedrooms_array();

$obj_id = get_queried_object_id();
$current_url = get_permalink( $obj_id );
?>
<div class="page-title-wrapper property-advance-search">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="heading heading-style-1">
					<?php if( ! empty( $page_title ) ) { ?>
						<h1 class="page-title">
							<?php echo sprintf( '%1$s', $page_title ); ?>
						</h1>
					<?php } ?>
					<?php if( ! empty( $page_subtitle ) ) { ?>
						<div class="page-subtitle">
							<?php echo esc_html( $page_subtitle ); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="advance-search">
		<div class="container">
			<div class="property-search-form row mx-sm-n1">
				<div class="col-lg col-sm-6 form-group px-sm-1">
					<label><?php esc_html_e( 'Location', 'mttheme' ); ?></label>
					<input name="location" class="form-control" type="text" placeholder="<?php esc_attr_e( 'Postcode of area', 'mttheme' ); ?>" value="<?php echo esc_attr( $location ); ?>" />
				</div>
				<div class="col-lg col-sm-6 form-group px-sm-1">
					<label><?php esc_html_e( 'Property Type', 'mttheme' ); ?></label>
					<select name="propery-type" class="form-control">
						<option value=""><?php esc_html_e( 'Any', 'mttheme' ); ?></option>
						<?php 
						if ( ! is_wp_error( $propery_type_lists ) && ! empty( $propery_type_lists ) ) {
							foreach( $propery_type_lists as $key => $propery_type_list ) {
								echo  '<option value="' . esc_attr( $propery_type_list->slug ) . '" ' . selected( $propery_type, $propery_type_list->slug, false ) . '>' . esc_html( $propery_type_list->name ) . '</option>';
							}
						}
						?>
					</select>
				</div>
				<div class="col-lg col-sm-6 form-group px-sm-1">
					<label><?php esc_html_e( 'Bedrooms', 'mttheme' ); ?></label>
					<select name="bedrooms" class="form-control">
						<?php 
						if ( ! empty( $bedrooms_options ) ) {
							foreach( $bedrooms_options as $key => $value ) {
								echo  '<option value="' . esc_attr( $key ) . '" ' . selected( $bedrooms, $value, false ) . '>' . esc_html( $value ) . '</option>';
							}
						}
						?>
					</select>
				</div>
				<div class="col-sm-auto form-group align-self-end px-sm-1">
					<button type="submit" class="btn btn-warning btn-3">
						<span>
							<?php esc_html_e( 'Search', 'mttheme' ); ?>
						</span>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
