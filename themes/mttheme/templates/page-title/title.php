<?php
/**
 * Template part for displaying page content in page.php
 *
 * @package Mttheme
 */

global $post;

$page_title    = '';
$page_subtitle = '';

if ( is_search() ) {

	$page_title = sprintf(
		'%1$s %2$s',
		__( 'Search For:', 'mttheme' ),
		'&ldquo;' . get_search_query() . '&rdquo;'
	);

	if ( $wp_query->found_posts ) {
		$page_subtitle = sprintf(
			/* translators: %s: Number of search results. */
			_n(
				'We found %s result for your search.',
				'We found %s results for your search.',
				$wp_query->found_posts,
				'mttheme'
			),
			number_format_i18n( $wp_query->found_posts )
		);
	} else {
		$page_subtitle = __( 'We could not find any results for your search. You can give it another try through the search form below.', 'mttheme' );
	}
} elseif ( ! is_home() && ! is_page() ) {
	$page_title    = get_the_archive_title();
	$page_subtitle = get_the_archive_description();
} elseif ( is_home() ) {
	$page_title = __( 'Blog', 'mttheme' );
} else {
	$page_title 	= get_the_title();
	$page_subtitle 	= get_post_meta( $post->ID, '_mttheme_page_subtitle_single', true );
}

?>
<div class="page-title-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="heading heading-style-1">
					<?php if( ! empty( $page_title ) ) { ?>
						<h1 class="page-title">
							<?php echo sprintf( '%1$s', $page_title ); ?>
						</h1>
					<?php } ?>
					<?php if( ! empty( $page_subtitle ) ) { ?>
						<div class="page-subtitle">
							<?php echo sprintf( '%1$s', $page_subtitle ); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
