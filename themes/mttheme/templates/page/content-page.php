<?php
/**
 * Template part for displaying page content in page.php
 *
 * @package Mttheme
 */

$author_url = get_author_posts_url( get_the_author_meta( 'ID' ) );
?>

<div class="mttheme-rich-snippet d-none">
	<span class="entry-title">
		<?php echo esc_html( get_the_title() ); ?>
	</span>
	<span class="author vcard">
		<a class="url fn n" href="<?php echo esc_url( $author_url ); ?>">
			<?php echo esc_html( get_the_author() ); ?>
		</a>
	</span>
	<span class="published">
		<?php echo esc_html( get_the_date() ); ?>
	</span>
	<time class="updated" datetime="<?php echo esc_attr( get_the_modified_date( 'c' ) ); ?>">
		<?php echo esc_html( get_the_modified_date() ); ?>
	</time>
</div>
<div class="entry-content page-content">
	<?php
		/* Get page content area */
		the_content();
	?>
</div>
<?php
	/* Displays page-links for paginated pages. */
	wp_link_pages( 
		array(
			'before'     => '<div class="page-links"><div class="inner-page-links"><span class="pagination-title">' . esc_html__( 'Pages:', 'mttheme' ).'</span>',
			'after'      => '</div></div>',
			'link_before'=> '<span class="page-number">',
			'link_after' => '</span>',
		)
	);
	/* If comments are open or we have at least one comment, load up the comment template. */
	if ( comments_open() || get_comments_number() ) {
		comments_template();
	}
?>
