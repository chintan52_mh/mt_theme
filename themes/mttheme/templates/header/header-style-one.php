<?php
/**
 * The template for displaying the header style 1
 *
 * @package Mttheme
 */

/* Exit if accessed directly. */
if ( ! defined( 'ABSPATH' ) ) { exit; }

$mttheme_logo             = get_theme_mod( 'mttheme_logo', '' );
$mttheme_logo_light       = get_theme_mod( 'mttheme_logo_light', '' );
$mttheme_logo_ratina      = get_theme_mod( 'mttheme_logo_ratina', '' );
$mttheme_logo_light_ratina= get_theme_mod( 'mttheme_logo_light_ratina', '' );

$mttheme_header_width   = get_theme_mod( 'mttheme_header_width', 'container-fluid' );
$mttheme_header_menu    = get_theme_mod( 'mttheme_header_menu', '' );

$mttheme_header_sidebar = get_theme_mod( 'mttheme_header', 'header-sidebar' );
$mttheme_header = mttheme_check_active_sidebar( $mttheme_header_sidebar );

?>

<div class="<?php echo esc_attr( $mttheme_header_width ); ?> nav-header-container">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
            <nav class="navbar top_navbar navbar-expand-lg ">
                <!-- start logo -->
                <?php if( $mttheme_logo || $mttheme_logo_light ) { ?>
                    <?php if( $mttheme_logo_ratina ) { ?>
                        <?php if( $mttheme_logo ) { ?>
                            <a href="<?php echo esc_url( home_url( '/' ) ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="navbar-brand logo-light">
                                <img class="logo" src="<?php echo esc_url( $mttheme_logo ) ?>" data-rjs="<?php echo esc_url( $mttheme_logo_ratina ) ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                            </a>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if( $mttheme_logo ) { ?>
                            <a href="<?php echo esc_url( home_url( '/' ) ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="logo-light navbar-brand">
                                <img class="logo" src="<?php echo esc_url( $mttheme_logo ) ?>" data-no-retina="" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                            </a>
                        <?php } ?>
                    <?php } ?>
                    <?php if( $mttheme_logo_light_ratina ) { ?>
                        <?php if( $mttheme_logo_light ) { ?>
                            <a href="<?php echo esc_url( home_url( '/' ) ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="logo-dark navbar-brand">
                                <img class="logo" src="<?php echo esc_url( $mttheme_logo_light ) ?>" data-rjs="<?php echo esc_url( $mttheme_logo_light_ratina ) ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                            </a>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if( $mttheme_logo_light ) { ?>
                            <a href="<?php echo esc_url( home_url( '/' ) ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="logo-dark navbar-brand">
                                <img class="logo" src="<?php echo esc_url( $mttheme_logo_light ) ?>" data-no-retina="" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                            </a>
                        <?php } ?>
                    <?php } ?>
                <?php } else { ?>
                    <span class="site-title">
                        <a href="<?php echo esc_url( home_url( '/' ) ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                            <?php echo esc_html( get_bloginfo( 'name' ) ); ?>
                        </a>
                    </span>
                <?php } ?>
                <!-- End logo -->
                
                <!-- Navigation -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <nav class="menu_links" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">

                        <?php   
                        if ( $mttheme_header_menu ) {
                            $mttheme_header_menu_select = array(
                                'container'       => 'ul',
                                'menu_class'      => 'nav navbar-nav',
                                'menu'            => $mttheme_header_menu,
                                'echo'            => true,
                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                'walker'          => new Mttheme_Standard_Menu(),
                            );
                            wp_nav_menu( $mttheme_header_menu_select );
                        }
                        elseif( has_nav_menu( 'primary-menu' ) ) {
                            $mttheme_header_menu_default = array(
                                'theme_location'  => 'primary-menu',
                                'container'       => 'ul',
                                'menu_class'      => 'nav navbar-nav',
                                'menu_id'         => '',
                                'echo'            => true,
                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                'walker'          => new Mttheme_Standard_Menu(),
                            );
                            wp_nav_menu( $mttheme_header_menu_default );
                        } else {
                            $mttheme_header_menu_default = array( 
                                'container'       => 'ul',
                                'menu_class'      => 'nav navbar-nav',
                                'menu_id'         => '',
                                'echo'            => true,
                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            );
                            wp_nav_menu( $mttheme_header_menu_default );
                        }
                        ?>
                    </nav>
                </div>
                <!-- End Navigation -->

                <?php if ( $mttheme_header ) { ?>
                    <div class="header-sidebar d-flex align-items-center justify-content-center">
                        <?php dynamic_sidebar( $mttheme_header_sidebar ); ?>
                    </div>
                <?php } ?>
            </nav>
        </div>
    </div>
</div>