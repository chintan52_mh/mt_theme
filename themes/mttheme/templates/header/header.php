<?php
/**
 * The template for displaying the header
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

$header_type = get_theme_mod( 'mttheme_header_style', 'header_type_1' );
$header_sticky_type = get_theme_mod( 'mttheme_header_sticky_type', 'up-scroll-sticky' );
$header_type .= ! empty( $header_sticky_type ) ? ' '.$header_sticky_type : '';
?>
<!-- header -->
<header id="masthead" class="site-header <?php echo esc_attr( $header_type ); ?>" itemscope="itemscope" itemtype="http://schema.org/WPHeader">

	<div class="main-header-wrapper w-100">

		<?php
		switch ( $header_type ) {
			case 'header_type_2':
			get_template_part( 'templates/header/header-style', 'two' );
			break;
			case 'header_type_3':
			get_template_part( 'templates/header/header-style', 'three' );
			break;
			case 'header_type_4':
			get_template_part( 'templates/header/header-style', 'four' );
			break;
			case 'header_type_1':
			default:
			get_template_part( 'templates/header/header-style', 'one' );
			break;
		}
		
		?>
	</div>
</header>
<!-- End header -->
<?php

if ( is_page() ) {
	$mttheme_search_form_uniqid = uniqid( 'search-header-' );
	?>
	<div class="container-fluid position-relative">
		<div class="search-wrapper">
			<a href="#<?php echo esc_attr( $mttheme_search_form_uniqid ); ?>" class="header-search">
				<i class="icon-search"></i>
				<span><?php _e( 'Search', 'mttheme' );?></span>
			</a>
			<div class="search-form-wrapper mfp-hide" id="<?php echo esc_attr( $mttheme_search_form_uniqid ); ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
								get_search_form();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}

