<?php
    
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

    /* Check if 404 page image set ot not */
    $mttheme_image_url            = get_theme_mod( 'mttheme_page_not_found_image', '' );
    $mttheme_page_not_found_image = ( $mttheme_image_url ) ? ' style="background-image:url('.esc_url( $mttheme_image_url ).');"': '';
    $mttheme_image_bg_class       = ( $mttheme_image_url ) ? ' cover-background': '';

    $mttheme_page_not_found_side_image = get_theme_mod( 'mttheme_page_not_found_side_image', '' );

    /* Get 404 page main title */
    $mttheme_page_not_found_main_title  = get_theme_mod( 'mttheme_page_not_found_main_title', sprintf( '%s || %s', __( 'Error 404', 'mttheme' ), __( 'Not Found', 'mttheme' ) ) );
    
    /* Get 404 page subtitle */
    $mttheme_page_not_found_title    = get_theme_mod( 'mttheme_page_not_found_title', __( 'Lorem ipsum is simply dummy text of the printing and make a type specimen book.', 'mttheme' ) );
    
    /* Back to home text */
    $mttheme_page_not_found_back_to_home_text = get_theme_mod( 'mttheme_page_not_found_back_to_home_text', sprintf( '%s <a href="' . home_url( '/' ) . '">%s</a> %s', __( 'You can go back to the', 'mttheme' ), __( 'Homepage', 'mttheme' ), __( 'or use Search', 'mttheme' ) ) );
    
    /* Check if search placeholder text */
    $mttheme_search_placeholder_text    = get_theme_mod( 'mttheme_search_placeholder_text', __( 'Search', 'mttheme' ) );

?>  
    <div class="main-background mttheme-main-content-wrap header-top-space" <?php echo sprintf( '%s', $mttheme_page_not_found_image ); ?>>
        <div class="page-not-found">
            <div class="container-fluid">
                <div class="row m-0">
                    <div class="not-found-inner w-100">
                        <div class="container pattern-wave pattern-gray related_post_slider_box">
                            <div class="row p-1 p-lg-5 align-items-center">    
                                <div class="col-lg-5 col-md-5">
                                    <div class="mttheme-404-content-wrap">
                                        <?php if( $mttheme_page_not_found_main_title ) { ?>
                                            <?php $mttheme_page_not_found_main_title = str_replace( '||', '<br />', $mttheme_page_not_found_main_title ); ?>
                                            <h2 class="mttheme-404-main-title">
                                                <?php echo sprintf( '%s', $mttheme_page_not_found_main_title ) ?>
                                            </h2>
                                        <?php } ?>
                                        <?php if( $mttheme_page_not_found_title ) { ?>
                                            <?php $mttheme_page_not_found_title = str_replace( '||', '<br />', $mttheme_page_not_found_title ); ?>
                                            <p class="mttheme-404-subtitle">
                                                <?php echo esc_html( $mttheme_page_not_found_title ) ?>
                                            </p>
                                        <?php } ?>
                                        <form action="<?php echo esc_url( home_url( '/' ) ) ?>" method="get" class="search-form ">
                                            <div class="input-group-404 input-group">
                                                <input type="text" id="search" class="search-input" placeholder="<?php echo esc_attr( $mttheme_search_placeholder_text ) ?>" value="<?php echo get_search_query(); ?>" name="s">
                                                <div class="input-group-btn">
                                                    <button type="submit" class="btn">
                                                        <i class="icon-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <p class="backtohome">
                                        <?php echo sprintf( '%s', $mttheme_page_not_found_back_to_home_text ); ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 mt-5 mt-md-0">
                                    <?php if( $mttheme_page_not_found_side_image ) { ?>
                                        <div class="text-center not-found-img">
                                            <img src="<?php echo esc_url( $mttheme_page_not_found_side_image ); ?>" alt="404-image">
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>