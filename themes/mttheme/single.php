<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package Mttheme
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header();

/* Start of the loop. */
while ( have_posts() ) : the_post();

	$author_displayname = get_the_author_meta( 'display_name' );
	$author_email = get_the_author_meta( 'email' );
	$author_id = get_the_author_meta( 'ID' );
	$author_designation = get_the_author_meta( 'designation', $author_id );
	$release_date = get_the_date();
	$categories = get_the_category();

	/* Filter for change layout style for ex. ?sidebar=right_sidebar */
	$author_url = get_author_posts_url( get_the_author_meta( 'ID' ) );

	$mttheme_single_post_right = get_theme_mod( 'mttheme_single_post_right_sidebar', '' );
	$mttheme_single_post_right_sidebar = mttheme_check_active_sidebar( $mttheme_single_post_right );
	$mttheme_single_post_layout_width   = get_theme_mod( 'mttheme_single_post_layout_width', 'container' );
	$column_class = ( $mttheme_single_post_right_sidebar ) ? 'col-lg-9 col-md-8' : 'col-md-12';

?>
<div id="post-<?php echo esc_attr( get_the_ID() ); ?>" <?php post_class( 'mttheme-main-content-wrap single-post-main-section' ); ?>>
	<div class="mttheme-rich-snippet d-none">
		<span class="entry-title">
			<?php echo esc_html( get_the_title() ); ?>
		</span>
		<span class="author vcard">
			<a class="url fn n" href="<?php echo esc_url( $author_url ); ?>">
				<?php echo esc_html( get_the_author() ); ?>
			</a>
		</span>
		<span class="published">
			<?php echo esc_html( get_the_date() ); ?>
		</span>
		<time class="updated" datetime="<?php echo esc_attr( get_the_modified_date( 'c' ) ); ?>">
			<?php echo esc_html( get_the_modified_date() ); ?>
		</time>
	</div>
	<div class="single-post-image blog-detail-img">
		<div class="container-fluid">
			<?php if ( has_post_thumbnail() ) { ?>
				<div class="image-wrapper">
					<?php
						echo get_the_post_thumbnail();
					?>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="<?php echo esc_attr( $mttheme_single_post_layout_width ); ?>">
		<div class="row m-0">
			<div class="blog-detail-inner <?php echo esc_attr( $column_class ); ?>">
				<div class="auther-are d-flex flex-wrap w-100 align-items-center justify-content-between mb-4">
					<div class="left">
						<div class="blog-author">
							<div class="author-image-left">
								<div class="author-image">
									<?php echo get_avatar( $author_email, 50 ); ?>
								</div>
							</div>
							<div class="author-content">
								<div class="author-name">
									<?php echo esc_html( $author_displayname ); ?>
								</div>

								<?php if ( ! empty( $author_designation ) ) { ?>
									<div class="author-designation">
										<?php echo esc_html( $author_designation ); ?>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="right d-flex align-items-center flex-sm-wrap">
						<?php if ( ! is_wp_error( $categories ) && ! empty( $categories ) ) { ?>
							<div class="blog-categories">
								<?php
									foreach( $categories as $category ) {
										$category_link = get_category_link( $category->term_id );
										?>
											<a href="<?php echo esc_url( $category_link ); ?>" >
												<?php echo esc_html( $category->name ); ?>
											</a>
										<?php
									}
								?>
							</div>
						<?php }	?>
						<div class="release-date">
							<?php echo esc_html( $release_date ); ?>
						</div>
					</div>
				</div>
				<div class="entry-content">
					<div class="post-title">
						<?php the_title( '<h3>', '</h3>' ); ?>
					</div>
					<div class="post-content">
						<?php the_content(); ?>
						<?php the_posts_pagination(); ?>
					</div>
				</div>
			</div>
			<?php if ( $mttheme_single_post_right_sidebar ) { ?>
				<div class="col-lg-3 col-md-4 sidebar" itemtype="http://schema.org/WPSideBar" itemscope="itemscope" role="complementary">
					<?php dynamic_sidebar( $mttheme_single_post_right ); ?>
				</div>
			<?php } ?>
		</div>
		<?php
			/* Displays page-links for paginated posts. */
            wp_link_pages( 
                array(
                    'before'      => '<div class="page-links"><div class="inner-page-links"><span class="pagination-title">' . esc_html__( 'Posts:', 'mttheme' ) . '</span>',
                    'after'       => '</div></div>',
                    'link_before' => '<span class="page-number">',
                    'link_after'  => '</span>',
                )
            );
			/* If comments are open or we have at least one comment, load up the comment template. */
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		?>
	</div>
	<?php
        $args = array(
            'category__in'          => wp_get_post_categories( get_the_ID() ),
            'ignore_sticky_posts'   => 1,
            'post_type'             => 'post',
            'post_status'           => 'publish',
            'posts_per_page'        => '10',
            'post__not_in'          => array( get_the_ID() ),
        );

        $related_post = new WP_Query( $args );
    ?>
    <div class="related_post_slider_main">
	    <div class="container-fluid pattern-wave pattern-gray related_post_slider_box">
			<div class="post-title container">
	    		<h3 class="heading-title">
	    			<?php esc_html_e( 'Related Blogs', 'mttheme' ); ?>
	    		</h3>
	    	</div>
		</div>
		
		<div class="container-fluid p-0 for-control">
			<div class="related-slider-controler">
				<div class="swiper-button-prev related-post-prev"></div>
				<div class="swiper-button-next related-post-next"></div>
			</div>
			<div class="swiper-container related-post-slider">
				<div class="swiper-wrapper">
					<?php
			            while( $related_post->have_posts() ) :
							$related_post->the_post();

							$related_author_displayname = get_the_author_meta( 'display_name' );
							$related_author_email = get_the_author_meta( 'email' );
							$related_author_id = get_the_author_meta( 'ID' );
							$related_author_designation = get_the_author_meta( 'designation', $related_author_id );
							$related_release_date = get_the_date();
							$related_categories = get_the_category();
							?>
								<div class="swiper-slide">
									<div <?php post_class( 'blog' ); ?>>
										<?php if ( has_post_thumbnail() ) { ?>
											<a href="<?php echo esc_url( get_permalink() ); ?>" class="blog-category">
												<div class="blog-image">
													<?php
														echo get_the_post_thumbnail( get_the_ID(), 'mttheme-medium-image' );
													?>
												</div>
											</a>
										<?php } ?>
											<div class="blog-detail">
													<?php if ( ! is_wp_error( $related_categories ) && ! empty( $related_categories ) ) { ?>
														<div class="blog-categories">
															<?php
																foreach( $related_categories as $related_category ) {
																	$category_link = get_category_link( $related_category->term_id );
																	?>
																		<a href="<?php echo esc_url( $category_link ); ?>" >
																			<?php echo esc_html( $related_category->name ); ?>
																		</a>
																	<?php
																}
															?>
														</div>
													<?php }	?>
												<div class="blog-title">
													<a href="<?php echo esc_url( get_permalink() ); ?>" >
														<?php echo get_the_title(); ?>
													</a>
												</div>
												<div class="blog-author">
													<div class="left">
														<div class="author-image">
															<?php echo get_avatar( $related_author_email, 50 ); ?>
														</div>
														<div class="author-content">
															<div class="author-name">
																<?php echo esc_html( $related_author_displayname ); ?>
															</div>
															<?php if ( ! empty( $related_author_designation ) ) { ?>
																<div class="author-designation">
																	<?php echo esc_html( $related_author_designation ); ?>
																</div>
															<?php } ?>
														</div>
													</div>
													<div class="right">
														<div class="release-date">
															<?php echo esc_html( $related_release_date ); ?>
														</div>
													</div>
												</div>
												<div class="read-more-btn">
													<a class="btn btn-link" href="<?php echo esc_url( get_permalink() ); ?>" >
														<?php esc_html_e( 'Read More', 'mtelements' ); ?>
													</a>
												</div>
											</div>
									</div>
								</div>
								
							<?php
						endwhile;
						wp_reset_postdata();
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
endwhile; // End of the loop.
get_footer();